<?php
/*
 -------------------------------------------------------------------------
 TAM plugin for GLPI
 Copyright (C) 2021-2023 by the TICgal Team.
 https://www.tic.gal
 -------------------------------------------------------------------------
 LICENSE
 This file is part of the TAM plugin.
 TAM plugin is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.
 TAM plugin is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with TAM. If not, see <http://www.gnu.org/licenses/>.
 --------------------------------------------------------------------------
 @package   TAM
 @author    the TICgal team
 @copyright Copyright (c) 2021-2023 TICgal team
 @license   AGPL License 3.0 or (at your option) any later version
            http://www.gnu.org/licenses/agpl-3.0-standalone.html
 @link      https://www.tic.gal
 @since     2021
 ----------------------------------------------------------------------
*/

/**
 * Install all necessary elements for the plugin
 *
 * @return boolean True if success
 */
function plugin_tam_install()
{
    $migration = new Migration(PLUGIN_TAM_VERSION);

    // Parse inc directory
    foreach (glob(dirname(__FILE__) . '/inc/*') as $filepath) {
        // Load *.class.php files and get the class name
        if (preg_match("/inc.(.+)\.class.php/", $filepath, $matches)) {
            $classname = 'PluginTam' . ucfirst($matches[1]);
            include_once($filepath);
            // If the install method exists, load it
            if (method_exists($classname, 'install')) {
                $classname::install($migration);
            }
        }
    }

    // Execute the whole migration
    $migration->executeMigration();

    return true;
}
/**
 * Uninstall previously installed elements of the plugin
 *
 * @return boolean True if success
 */
function plugin_tam_uninstall()
{
    $migration = new Migration(PLUGIN_TAM_VERSION);

    // Parse inc directory
    foreach (glob(dirname(__FILE__) . '/inc/*') as $filepath) {
        // Load *.class.php files and get the class name
        if (preg_match("/inc.(.+)\.class.php/", $filepath, $matches)) {
            $classname = 'PluginTam' . ucfirst($matches[1]);
            include_once($filepath);
            // If the install method exists, load it
            if (method_exists($classname, 'uninstall')) {
                $classname::uninstall($migration);
            }
        }
    }

    // Execute the whole migration
    $migration->executeMigration();

    return true;
}

function plugin_tam_getDropdown()
{
    $plugin = new Plugin();

    if ($plugin->isActivated("tam")) {
        return [
            'PluginTamLeaveType' => PluginTamLeaveType::getTypeName(2),
            'PluginTamShiftTemplate' => PluginTamShiftTemplate::getTypeName(2),
            'PluginTamReason' => PluginTamReason::getTypeName(2)
        ];
    } else {
        return [];
    }
}

function plugin_tam_checkRequired(CommonDBTM $item)
{
    switch ($item::getType()) {
        case PluginTamLeave::getType():
            if ($item->input['users_id'] <= 0) {
                $message = sprintf(__('Mandatory fields are not filled. Please correct: %s'), __('User'));
                Session::addMessageAfterRedirect($message, false, ERROR);
                return $item->input = false;
            } elseif ($item->input['plugin_tam_leavetypes_id'] <= 0) {
                $message = sprintf(__('Mandatory fields are not filled. Please correct: %s'), __('Type'));
                Session::addMessageAfterRedirect($message, false, ERROR);
                return $item->input = false;
            } elseif (empty(trim($item->input['date_start']))) {
                $message = sprintf(__('Mandatory fields are not filled. Please correct: %s'), __('Start date'));
                Session::addMessageAfterRedirect($message, false, ERROR);
                return $item->input = false;
            } elseif (empty(trim($item->input['date_end']))) {
                $message = sprintf(__('Mandatory fields are not filled. Please correct: %s'), __('End date'));
                Session::addMessageAfterRedirect($message, false, ERROR);
                return $item->input = false;
            } elseif ($item->input['date_end'] < $item->input['date_start']) {
                $message = __('Invalid date. End date must be greater than start date.', 'tam');
                Session::addMessageAfterRedirect($message, false, ERROR);
                return $item->input = false;
            } else {
                PluginTamLeave::addName($item);
            }
            break;
    }
}

function plugin_tam_post_item_form(array $params)
{
    if (isset($params['item']) && $params['item'] instanceof CommonDBTM) {
        switch ($params['item']::getType()) {
            case User::getType():
                //error_log(print_r($params,true), 3, GLPI_LOG_DIR."/test.log");
                $user = $params['item'];
                PluginTamUserCalendar::showField($user);
                break;
        }
    }
}

function plugin_tam_getAddSearchOptions($itemtype)
{
    $sopt = [];

    switch ($itemtype) {
        case 'User':
            if (Session::haveRight('user', READ)) {
                $sopt[1000] = [
                    'table' => Calendar::getTable(),
                    'field' => 'name',
                    'name' => __('Calendar'),
                    'massiveaction' => true,
                    'datatype' => 'itemlink',
                    'joinparams' => [
                        'beforejoin' => [
                            'table' => PluginTamUserCalendar::getTable(),
                            'joinparams' => [
                                'jointype' => 'child'
                            ]
                        ]
                    ]
                ];
            }
            break;
    }

    return $sopt;
}

function plugin_tam_AssignToTicket($types)
{
    $types['PluginTamLeave'] = PluginTamLeave::getTypeName();
    return $types;
}

function plugin_tam_redefine_menus($menus)
{
    global $CFG_GLPI;

    //error_log(print_r($menus,true),3,GLPI_LOG_DIR."/aaamenus.log");
    if (isset($menus['helpdesk'])) {
        if (isset($menus['helpdesk']['content']['planning'])) {
            if (PluginTamShift::canView()) {
                $icon = "<i class='fa fas fa-calendar-check pointer' title='" . __('Shift', 'tam') . "'><span class='sr-only'>" . __('Shift', 'tam') . "</span></i>";
                $menus['helpdesk']['content']['planning']['links'][$icon] = PluginTamShift::getSearchURL(false);
                $menus['helpdesk']['content']['planning']['options']['plugintamshift'] = [
                    'title' => PluginTamShift::getTypeName(Session::getPluralNumber()),
                    'page'  => PluginTamShift::getSearchURL(false),
                    'links' => [
                        'add'    => PluginTamShift::getFormURL(false),
                        'search' => PluginTamShift::getSearchURL(false),
                    ] + Planning::getAdditionalMenuLinks()
                ];
                if (isset($menus['helpdesk']['content']['planning']['options']['external'])) {
                    $icon = "<i class='fa fas fa-calendar-check pointer' title='" . __('Shift', 'tam') . "'><span class='sr-only'>" . __('Shift', 'tam') . "</span></i>";
                    $menus['helpdesk']['content']['planning']['options']['external']['links'][$icon] = "/" . Plugin::getWebDir('tam', false) . '/front/shift.php';
                }
            }
        }
    }

    $admin_icon = "<i class='far fa-calendar-alt' title='" . __('Admin', 'tam') . "'><span class='sr-only'>" . __('Shift', 'tam') . "</span></i>";
    $log_icon = "<i class='fas fa-book' title='" . __('Log', 'tam') . "'><span class='sr-only'>" . __('Shift', 'tam') . "</span></i>";
    $leave_icon = "<i class='far fa-calendar-times' title='" . __('Leave', 'tam') . "'><span class='sr-only'>" . __('Shift', 'tam') . "</span></i>";
    $forgot_icon = "<i class='fas fa-stopwatch' title='" . __('Forgot', 'tam') . "'><span class='sr-only'>" . __('Shift', 'tam') . "</span></i>";

    $menus['management']['content']['plugintammenu']['options']['admin'] = [
        'title' => __("Admin Panel", "tam"),
        'page'  => '/' . Plugin::getWebDir('tam', false) . "/front/admin.form.php",
        'icon'  => 'far fa-calendar-alt',
        'links' => [
            "$log_icon"     => "/plugins/tam/front/log.php",
            "$leave_icon"   => "/plugins/tam/front/leave.php",
            "$forgot_icon"  => "/plugins/tam/front/forgot.php",
        ]
    ];

    $menus['management']['content']['plugintammenu']['options']['log']  = [
        'title' => __("Logs", "tam"),
        'page'  => '/' . Plugin::getWebDir('tam', false) . "/front/log.php",
        'icon'  => 'fas fa-book',
        'links' => [
            "$admin_icon"   => "/plugins/tam/front/admin.form.php",
            "$leave_icon"   => "/plugins/tam/front/leave.php",
            "$forgot_icon"  => "/plugins/tam/front/forgot.php",
        ]
    ];

    $menus['management']['content']['plugintammenu']['options']['leave'] = [
        'title' => __("Leavings", "tam"),
        'page'  => '/' . Plugin::getWebDir('tam', false) . "/front/leave.php",
        'icon'  => 'far fa-calendar-times',
        'links' => [
            "add" => '/plugins/tam/front/leave.form.php',
            "$admin_icon"   => "/plugins/tam/front/admin.form.php",
            "$log_icon"     => "/plugins/tam/front/log.php",
            "$forgot_icon"  => "/plugins/tam/front/forgot.php",
        ]
    ];

    $menus['management']['content']['plugintammenu']['options']['forgot'] = [
        'title' => __("Forgotten", "tam"),
        'page'  => '/' . Plugin::getWebDir('tam', false) . "/front/forgot.php",
        'icon'  => 'fas fa-stopwatch',
        'links' => [
            "$admin_icon"   => "/plugins/tam/front/admin.form.php",
            "$log_icon"     => "/plugins/tam/front/log.php",
            "$leave_icon"   => "/plugins/tam/front/leave.php",
        ]
    ];

    return $menus;
}

function plugin_tam_display_login()
{
    $config = new PluginTamConfig();
    $config->getFromDB(1);
    if ($config->fields['tam_login'] == 0) {
        return;
    }

    $redirect = '/plugins/tam/front/login.php';
    $text = __("TAM", "tam");
    $icon = '';
    $button = "<a href='".$redirect."' class='btn btn-secondary mt-2 w-100'><i class='fa fa-user-clock ms-2'></i>$text</a>";

    $script = <<<JAVASCRIPT
		window.addEventListener("load", (event) => {
			$(".form-footer").append("{$button}");
		});
JAVASCRIPT;

    echo HTML::scriptBlock($script);
}
