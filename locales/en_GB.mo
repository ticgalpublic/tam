��    s      �  �   L      �	     �	     �	     �	     �	     �	  
   �	  
   

     
     $
     2
     ;
     H
     X
     i
     u
     z
     �
     �
     �
     �
  $   �
     �
  
   �
  
                       3     M  
   `     k     s     y  	   �     �     �     �     �     �     �     �  7   �  7        Q     W     d     y     �     �     �     �     �     �     �     �     �     �     �     �          ,     /  
   ?     J     W     h     n     |     �     �     �     �     �  
   �     �     �     �               6     C     J     _     c     p     y  	   �     �  2   �     �     �     �          ,  
   9     D     P     \     n     }     �     �     �      �  #   �               #     5     =     I     V  	   c  1   m  5  �     �     �     �     �       
     
        )     8     F     O     \     l     }     �     �     �     �     �     �  $   �       
   
  
              (     -     G     a  
   t          �     �  	   �     �     �     �     �     �     �     �  7   �  7   -     e     k     x     �     �     �     �     �     �     �     �     �     �                    '     @     C  
   S     ^     k     |     �     �     �     �     �     �     �  
   �     �     �          $     3     J     W     ^     s     w     �     �  	   �     �  2   �     �               2     @  
   M     X     d     p     �     �     �     �     �      �  #   �          (     7     I     Q     ]     j  	   w  1   �     Q   k   d   O   b   D                 6       T      -   p   5   0       B         	           l       %          <   +         E           A   V                 @          I   #   8                 7   :   .   9           Z   &          S          N   H       X           (   '   f       1   "   j   ]       o          e               C       P   g   n   Y   q      h   M           W   L   $      _   2   R   =   K       ?              >         [   *          3   ^      a          ,   U   s   !             m   r   F   ;       4      `       \       G          /   J   c   
   )       i        Admin Admin Panel After hours Assigned Workplace Back to Home Break Time Break time By the company By the worker Calendar Calendar day Change calendar Changed calendar Close times Code Company Compensation Coordinates end Coordinates start Create a task for user Create a ticket to correct the timer Date end Date range Date start Details Diff Effective working days %s Enable quick login to TAM End date corrected Ending TAM Fix TAM Fixed Forgot Forgotten Forgotten TAM Forgotten Timers %s Forgotten timer Holiday Hours worked IP end IP start Indicate the time of departure / completion of the task Invalid date. End date must be greater than start date. Leave Leave Reason Leave without notice Leaves Leaves type by user Leaving type Leavings Log Login with TAM Logs Mark as default Mark day as leave Max holidays Motive NIF New end of working day Number of leaves by user Ok On working time Origin end Origin start Overworked hours Print Printing date Reason Reason for leaving Remote working Settings Shift Shift template Show timer Show unstacked leaves Standard Working Diff Standard Working time Standard timer Standard working hours Starting TAM Status Stop forgotten timer TAM TAM Calendar TAM Logs TAM Management TAM Users Task template The selected date is different from the start date There is an active timer on Ticket template Timer has not been initialized Timer running Total Breaks Total Diff Total Hours Total Shift Total Shift Hours Total Standard Total Standard Diff Total Working day Total holidays Total leaves User %s forgot to stop the timer User %s was on leave without notice View all timers Work from home Worked less hours Working Working day Working days Working time Workplace You will be logged out once you press the button. Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2023-05-19 10:38+0200
Last-Translator: Automatically generated
Language-Team: none
Language: en_GB
MIME-Version: 1.0
Content-Type: text/plain; charset=ASCII
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 Admin Admin Panel After hours Assigned Workplace Back to Home Break Time Break time By the company By the worker Calendar Calendar day Change calendar Changed calendar Close times Code Company Compensation Coordinates end Coordinates start Create a task for user Create a ticket to correct the timer Date end Date range Date start Details Diff Effective working days %s Enable quick login to TAM End date corrected Ending TAM Fix TAM Fixed Forgot Forgotten Forgotten TAM Forgotten Timers %s Forgotten timer Holiday Hours worked IP end IP start Indicate the time of departure / completion of the task Invalid date. End date must be greater than start date. Leave Leave Reason Leave without notice Leaves Leaves type by user Leaving type Leavings Log Login with TAM Logs Mark as default Mark day as leave Max holidays Motive NIF New end of working day Number of leaves by user Ok On working time Origin end Origin start Overworked hours Print Printing date Reason Reason for leaving Remote working Settings Shift Shift template Show timer Show unstacked leaves Standard Working Diff Standard Working time Standard timer Standard working hours Starting TAM Status Stop forgotten timer TAM TAM Calendar TAM Logs TAM Management TAM Users Task template The selected date is different from the start date There is an active timer on Ticket template Timer has not been initialized Timer running Total Breaks Total Diff Total Hours Total Shift Total Shift Hours Total Standard Total Standard Diff Total Working day Total holidays Total leaves User %s forgot to stop the timer User %s was on leave without notice View all timers Work from home Worked less hours Working Working day Working days Working time Workplace You will be logged out once you press the button. 