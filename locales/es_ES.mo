��    s      �  �   L      �	     �	     �	     �	     �	     �	  
   �	  
   

     
     $
     2
     ;
     H
     X
     i
     u
     z
     �
     �
     �
     �
  $   �
     �
  
   �
  
                       3     M  
   `     k     s     y  	   �     �     �     �     �     �     �     �  7   �  7        Q     W     d     y     �     �     �     �     �     �     �     �     �     �     �     �          ,     /  
   ?     J     W     h     n     |     �     �     �     �     �  
   �     �     �     �               6     C     J     _     c     p     y  	   �     �  2   �     �     �     �          ,  
   9     D     P     \     n     }     �     �     �      �  #   �               #     5     =     I     V  	   c  1   m  �   �     �     �     �     �     �     �     �     �       
         +     8     K     _     o     w          �     �     �  ,   �               .     >  
   G     R  +   p      �     �     �  	   �     �     �     �          $  
   :     E     V     _  5   l  R   �     �     �          &     .     J     W     _     h     �     �     �     �     �     �     �     �                ;     H     Y     t     }     �     �     �     �     �     �     �     �          -     I     `     }     �     �     �     �     �     �     �     �  7        <     W  %   k     �     �     �     �     �     �     �          '     =     P  -   `  -   �     �     �     �  
                  .     @  6   Q     Q   k   d   O   b   D                 6       T      -   p   5   0       B         	           l       %          <   +         E           A   V                 @          I   #   8                 7   :   .   9           Z   &          S          N   H       X           (   '   f       1   "   j   ]       o          e               C       P   g   n   Y   q      h   M           W   L   $      _   2   R   =   K       ?              >         [   *          3   ^      a          ,   U   s   !             m   r   F   ;       4      `       \       G          /   J   c   
   )       i        Admin Admin Panel After hours Assigned Workplace Back to Home Break Time Break time By the company By the worker Calendar Calendar day Change calendar Changed calendar Close times Code Company Compensation Coordinates end Coordinates start Create a task for user Create a ticket to correct the timer Date end Date range Date start Details Diff Effective working days %s Enable quick login to TAM End date corrected Ending TAM Fix TAM Fixed Forgot Forgotten Forgotten TAM Forgotten Timers %s Forgotten timer Holiday Hours worked IP end IP start Indicate the time of departure / completion of the task Invalid date. End date must be greater than start date. Leave Leave Reason Leave without notice Leaves Leaves type by user Leaving type Leavings Log Login with TAM Logs Mark as default Mark day as leave Max holidays Motive NIF New end of working day Number of leaves by user Ok On working time Origin end Origin start Overworked hours Print Printing date Reason Reason for leaving Remote working Settings Shift Shift template Show timer Show unstacked leaves Standard Working Diff Standard Working time Standard timer Standard working hours Starting TAM Status Stop forgotten timer TAM TAM Calendar TAM Logs TAM Management TAM Users Task template The selected date is different from the start date There is an active timer on Ticket template Timer has not been initialized Timer running Total Breaks Total Diff Total Hours Total Shift Total Shift Hours Total Standard Total Standard Diff Total Working day Total holidays Total leaves User %s forgot to stop the timer User %s was on leave without notice View all timers Work from home Worked less hours Working Working day Working days Working time Workplace You will be logged out once you press the button. Project-Id-Version: TAM
Report-Msgid-Bugs-To: 
Language: es_ES
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Localazy (https://localazy.com)
Plural-Forms: nplurals=2; plural=(n==1) ? 0 : 1;
 Administrador Admin Panel Fuera de horario Lugar de trabajo asignado De vuelta a casa Descanso Descanso Por la empresa Por el trabajador Calendario Día natural Cambiar calendario Calendario cambiado Horas de cierre Código Empresa Compensación Fin de coordenadas Inicio de coordenadas Crear una tarea para el usuario Crea un ticket para corregir el temporizador Fecha de finalización Intervalo de fechas Fecha de inicio Detalles Diferencia Días laborables efectivos %s Activar el inicio rápido de sesión en TAM Fecha de finalización corregida Finalización de TAM Arreglar TAM Arreglado Olvidado Olvidado TAM olvidado Temporizadores olvidados %s Temporizador olvidado Vacaciones Horas trabajadas IP final IP de inicio Indicar la hora de salida / finalización de la tarea Fecha invalida. La fecha de finalización debe ser posterior a la fecha de inicio. Salir Motivo de la salida Salir sin previo aviso Salidas Tipo de salidas por usuario Leaving type Salidas Registro Iniciar sesión con TAM Logs Marcar como predeterminado Marcar el día como salida Máximo de vacaciones Motivo NIF Nuevo fin de jornada laboral Número de salidas por usuario Aceptar Sobre el tiempo de trabajo Origen final Inicio de origen Horas de trabajo excesivas Imprimir Fecha de impresión Motivo Motivo de la salida Trabajo remoto Ajustes Turno Plantilla de turnos Mostrar temporizador Mostrar salidas sin apilar Diferencia de trabajo estándar Tiempo de trabajo estándar Temporizador estándar Horario de trabajo estándar Iniciando TAM Estado Detener temporizador olvidado TAM Calendario TAM Registros TAM Gestión de TAM Usuarios de TAM Plantilla de tarea La fecha seleccionada es diferente a la fecha de inicio Hay un temporizador activo Plantilla de ticket El temporizador no se ha inicializado Temporizador en marcha Descansos totales Diferencia total Horas totales Turno total Horas de turno totales Total estándar Diferencia estándar total Jornada laboral total Vacaciones totales Salidas totales El usuario %s olvidó detener el temporizador El usuario %s estuvo de baja sin previo aviso Ver todos los temporizadores Trabajar desde casa Trabajó menos horas Trabajando Día de trabajo Días laborables Tiempo de trabajo Lugar de trabajo Se cerrará la sesión una vez que presione el botón. 