��    s      �  �   L      �	     �	     �	     �	     �	     �	  
   �	  
   

     
     $
     2
     ;
     H
     X
     i
     u
     z
     �
     �
     �
     �
  $   �
     �
  
   �
  
                       3     M  
   `     k     s     y  	   �     �     �     �     �     �     �     �  7   �  7        Q     W     d     y     �     �     �     �     �     �     �     �     �     �     �     �          ,     /  
   ?     J     W     h     n     |     �     �     �     �     �  
   �     �     �     �               6     C     J     _     c     p     y  	   �     �  2   �     �     �     �          ,  
   9     D     P     \     n     }     �     �     �      �  #   �               #     5     =     I     V  	   c  1   m  �   �     �     �     �     �     �     �     �     �       
        '     7     J     ^     q     y     �     �     �      �  +   �     
          &     5  
   >     I  )   g     �     �     �  	   �  	   �  	   �     �     �       
   .     9     K  	   R  2   \  N   �     �     �     �               0     =     E     N     e     j     �     �     �     �     �     �  	   �                -     =     Z     c     v     }     �     �     �     �     �     �      �          9     K     f     t     {     �     �     �     �     �     �  2   �          :  #   K     o     �     �     �     �     �     �     �     
     $     5  *   D  -   o     �     �     �     �     �               $  '   6     Q   k   d   O   b   D                 6       T      -   p   5   0       B         	           l       %          <   +         E           A   V                 @          I   #   8                 7   :   .   9           Z   &          S          N   H       X           (   '   f       1   "   j   ]       o          e               C       P   g   n   Y   q      h   M           W   L   $      _   2   R   =   K       ?              >         [   *          3   ^      a          ,   U   s   !             m   r   F   ;       4      `       \       G          /   J   c   
   )       i        Admin Admin Panel After hours Assigned Workplace Back to Home Break Time Break time By the company By the worker Calendar Calendar day Change calendar Changed calendar Close times Code Company Compensation Coordinates end Coordinates start Create a task for user Create a ticket to correct the timer Date end Date range Date start Details Diff Effective working days %s Enable quick login to TAM End date corrected Ending TAM Fix TAM Fixed Forgot Forgotten Forgotten TAM Forgotten Timers %s Forgotten timer Holiday Hours worked IP end IP start Indicate the time of departure / completion of the task Invalid date. End date must be greater than start date. Leave Leave Reason Leave without notice Leaves Leaves type by user Leaving type Leavings Log Login with TAM Logs Mark as default Mark day as leave Max holidays Motive NIF New end of working day Number of leaves by user Ok On working time Origin end Origin start Overworked hours Print Printing date Reason Reason for leaving Remote working Settings Shift Shift template Show timer Show unstacked leaves Standard Working Diff Standard Working time Standard timer Standard working hours Starting TAM Status Stop forgotten timer TAM TAM Calendar TAM Logs TAM Management TAM Users Task template The selected date is different from the start date There is an active timer on Ticket template Timer has not been initialized Timer running Total Breaks Total Diff Total Hours Total Shift Total Shift Hours Total Standard Total Standard Diff Total Working day Total holidays Total leaves User %s forgot to stop the timer User %s was on leave without notice View all timers Work from home Worked less hours Working Working day Working days Working time Workplace You will be logged out once you press the button. Project-Id-Version: TAM
Report-Msgid-Bugs-To: 
Language: gl_ES
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Localazy (https://localazy.com)
Plural-Forms: nplurals=2; plural=(n==1) ? 0 : 1;
 Administrador Admin Panel Despois de horas Lugar de traballo asignado Volver á casa Descanso Descanso Pola empresa Polo traballador Calendario Día calendario Cambiar calendario Calendario cambiado Horarios próximos Código Empresa Compensación As coordenadas rematan Comezan as coordenadas Crear unha tarefa para o usuario Crea un ticket para corrixir o temporizador Data fin Intervalo de datas Data de inicio Detalles Diferencia Días laborables efectivos %s Activa o inicio de sesión rápido en TAM Data de finalización corrixida Finalizando TAM Correxir TAM Corrixido Esquecido Esquecido TAM esquecido Temporizadores esquecidos %s Temporizador esquecido Vacacións Horas traballadas IP Fin IP Inicio Indicar a hora de saída / finalización da tarefa Data non válida. A data de finalización debe ser superior á data de inicio. Saír Motivo da saída Saír sen previo aviso Saídas Tipo de saídas por usuario Leaving type Saídas Rexistro Inicia sesión con TAM Logs Marcar como predeterminado Marca o día como saída O máximo de vacacións Motivo NIF Novo fin de xornada laboral Número de saídas por usuario De acordo Sobre o tempo de traballo Fin da orixe Inicio da orixe Horas con exceso de traballo Imprimir Data de impresión Motivo Motivo para saír Traballo a distancia Configuración Quenda Modelo de quendas Mostrar temporizador Mostrar saídas sen apilar Diferencia de traballo estándar Tempo de traballo estándar Horario estándar Horario normal de traballo Iniciando TAM Estado Detén o temporizador esquecido TAM Calendario TAM Rexistro TAM Xestión de TAM Usuarios de TAM Modelo de tarefa A data seleccionada é diferente da data de inicio Hai un temporizador activo Modelo de ticket O temporizador non foi inicializado Temporizador funcionando Descansos totais Diferencia total Total de horas Quenda total Total de horas de quenda Estándar total Diferencia estándar total Xornada total de traballo Total vacacións Saídas totais O usuario %s esqueceu deter o temporizador O usuario %s estaba de baixa sen previo aviso Ver todos os temporizadores Traballar desde casa Traballou menos horas Traballando Día de traballo Días laborables Tempo de traballo Lugar de traballo Pecharase sesión cando prema o botón. 