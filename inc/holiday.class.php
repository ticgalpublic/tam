<?php
/*
 -------------------------------------------------------------------------
 TAM plugin for GLPI
 Copyright (C) 2021-2023 by the TICgal Team.
 https://www.tic.gal
 -------------------------------------------------------------------------
 LICENSE
 This file is part of the TAM plugin.
 TAM plugin is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.
 TAM plugin is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with TAM. If not, see <http://www.gnu.org/licenses/>.
 --------------------------------------------------------------------------
 @package   TAM
 @author    the TICgal team
 @copyright Copyright (c) 2021-2023 TICgal team
 @license   AGPL License 3.0 or (at your option) any later version
            http://www.gnu.org/licenses/agpl-3.0-standalone.html
 @link      https://www.tic.gal
 @since     2021
 ----------------------------------------------------------------------
*/

class PluginTamHoliday extends CommonDBTM
{
    public $dohistory = false;
    public static $rightname = 'planning';

    public $auto_message_on_action    = false;

    public static function getTypeName($nb = 0)
    {
        return __('Holiday', 'tam');
    }

    public static function getHolidays($ID, $entities_id)
    {
        global $DB;

        $holidays = [];
        if (!$calendars_id = PluginTamUserCalendar::getCalendar($ID)) {
            $calendars_id = Entity::getUsedConfig('calendars_strategy', $entities_id, 'calendars_id', 0);
        }
        if ($calendars_id > 0) {
            $query = [
                'SELECT' => 'glpi_holidays.*',
                'FROM' => 'glpi_calendars_holidays',
                'INNER JOIN' => [
                    'glpi_holidays' => [
                        'FKEY' => [
                            'glpi_calendars_holidays' => 'holidays_id',
                            'glpi_holidays' => 'id'
                        ]
                    ]
                ],
                'WHERE' => [
                    'calendars_id' => $calendars_id
                ]
            ];
            foreach ($DB->request($query) as $id => $row) {
                $holidays[] = [
                    'begin'        => $row['begin_date'],
                    'end'          => $row['end_date'],
                    'is_perpetual' => $row['is_perpetual'],
                    'name' => $row['name'],
                    'comment' => $row['comment'],
                    'id' => $row['id'],
                ];
            }
        }
        return $holidays;
    }

    public static function populatePlanning($options = [])
    {
        global $DB, $CFG_GLPI;

        $default_options = [
            'color'               => '',
            'event_type_color'    => '',
            'check_planned'       => false,
            'display_done_events' => true,
        ];
        $options = array_merge($default_options, $options);
        $interv   = [];

        if (!isset($options['begin']) || ($options['begin'] == 'NULL') || !isset($options['end']) || ($options['end'] == 'NULL')) {
            return $interv;
        }
        if (!$options['display_done_events']) {
            return $interv;
        }

        $holidays = self::getHolidays(Session::getLoginUserID(), $_SESSION["glpiactive_entity"]);

        if (!empty($holidays)) {
            foreach ($holidays as $k => $holiday) {
                $link = Holiday::getFormURLWithID($holiday['id']);
                if ($holiday['is_perpetual']) {
                    for ($i = 0; $i <= 10; $i++) {
                        $year = (date("Y") - 5) + $i;
                        $hb = date('m-d', strtotime($holiday['begin']));
                        $he = date('m-d', strtotime($holiday['end']));
                        $begin = $year . '-' . $hb . ' 00:00:00';
                        $end = $year . '-' . $he . ' 23:59:59';

                        if (($options['begin'] < $begin && $begin < $options['end']) || ($options['begin'] < $end && $end < $options['end'])) {
                            $key = $begin . "$$" . "PluginTamHoliday" . $k;
                            $interv[$key]['color']            = $options['event_type_color'];
                            $interv[$key]['event_type_color'] = $options['event_type_color'];
                            $interv[$key]["itemtype"]         = Holiday::getType();
                            $interv[$key]["id"]               = $k;
                            $interv[$key]["users_id"]         = 0;
                            $interv[$key]["begin"]            = $begin;
                            $interv[$key]["end"]              = $end;
                            $interv[$key]["name"]             = $holiday['name'];
                            $interv[$key]["content"]          = $holiday['comment'];
                            $interv[$key]["editable"] = false;
                            $interv[$key]["url"]     = $link;
                        }
                    }
                } else {
                    $begin = $holiday['begin'] . ' 00:00:00';
                    $end = $holiday['end'] . ' 23:59:59';
                    if (($options['begin'] < $begin && $begin < $options['end']) || ($options['begin'] < $end && $end < $options['end'])) {
                        $key = $begin . "$$" . "PluginTamHoliday" . $k;
                        $interv[$key]['color']            = $options['event_type_color'];
                        $interv[$key]['event_type_color'] = $options['event_type_color'];
                        $interv[$key]["itemtype"]         = Holiday::getType();
                        $interv[$key]["id"]               = $k;
                        $interv[$key]["users_id"]         = 0;
                        $interv[$key]["begin"]            = $begin;
                        $interv[$key]["end"]              = $end;
                        $interv[$key]["name"]             = $holiday['name'];
                        $interv[$key]["content"]          = $holiday['comment'];
                        $interv[$key]["editable"] = false;
                        $interv[$key]["url"]     = $link;
                    }
                }
            }
        }

        return $interv;
    }

    public static function displayPlanningItem(array $val, $who, $type = "", $complete = 0)
    {
        $html = "";
        $rand     = mt_rand();
        if ($val["name"]) {
            $html .=  "<strong>" . $val["name"] . "</strong><br>";
        }
        if ($val["end"]) {
            $html .= "<strong>" . __('End date') . "</strong> : " . Html::convdatetime($val["end"]) . "<br>";
        }
        if ($complete) {
            $html .= "<div class='event-description'>" . $val["content"] . "</div>";
        } else {
            $html .= Html::showToolTip($val["content"], [
                'applyto' => "cri_" . $val["id"] . $rand,
                'display' => false
            ]);
        }
        return $html;
    }
}
