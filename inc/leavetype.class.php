<?php
/*
 -------------------------------------------------------------------------
 TAM plugin for GLPI
 Copyright (C) 2021-2023 by the TICgal Team.
 https://www.tic.gal
 -------------------------------------------------------------------------
 LICENSE
 This file is part of the TAM plugin.
 TAM plugin is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.
 TAM plugin is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with TAM. If not, see <http://www.gnu.org/licenses/>.
 --------------------------------------------------------------------------
 @package   TAM
 @author    the TICgal team
 @copyright Copyright (c) 2021-2023 TICgal team
 @license   AGPL License 3.0 or (at your option) any later version
            http://www.gnu.org/licenses/agpl-3.0-standalone.html
 @link      https://www.tic.gal
 @since     2021
 ----------------------------------------------------------------------
*/

if (!defined('GLPI_ROOT')) {
    die("Sorry. You can't access directly to this file");
}
class PluginTamLeaveType extends CommonTreeDropdown
{
    public static $rightname = 'plugin_tam_leave';

    public static function getTypeName($nb = 0)
    {
        return __('Leaving type', 'tam');
    }

    public function getAdditionalFields()
    {
        return [
            [
                'name'  => $this->getForeignKeyField(),
                'label' => __('As child of'),
                'type'  => 'parent',
                'list'  => false
            ], [
                'name'   => 'public',
                'label'  => __('Public'),
                'type'   => 'bool',
                'list'   => true
            ], [
                'name'   => 'natural',
                'label'  => __('Calendar day', 'tam'),
                'type'   => 'bool',
                'list'   => true
            ], [
                'name'   => 'compensation',
                'label'  => __('Compensation', 'tam'),
                'type'   => 'bool',
                'list'   => true
            ], [
                'name'   => 'holiday',
                'label'  => __('Holiday', 'tam'),
                'type'   => 'bool',
                'list'   => true
            ]
        ];
    }

    public function rawSearchOptions()
    {
        $tab = parent::rawSearchOptions();

        $tab[] = [
            'id'                => '411',
            'table'              => $this->getTable(),
            'field'              => 'public',
            'name'               => __('Public'),
            'datatype'           => 'bool',
            'massiveaction'      => false
        ];

        $tab[] = [
            'id'                => '412',
            'table'              => $this->getTable(),
            'field'              => 'natural',
            'name'               => __('Calendar day', 'tam'),
            'datatype'           => 'bool',
            'massiveaction'      => false
        ];

        $tab[] = [
            'id'                => '413',
            'table'              => $this->getTable(),
            'field'              => 'compensation',
            'name'               => __('Compensation', 'tam'),
            'datatype'           => 'bool',
            'massiveaction'      => false
        ];

        $tab[] = [
            'id'                => '414',
            'table'              => $this->getTable(),
            'field'              => 'holiday',
            'name'               => __('Holiday', 'tam'),
            'datatype'           => 'bool',
            'massiveaction'      => false
        ];

        return $tab;
    }

    public static function rawSearchOptionsToAdd()
    {
        $tab = [];

        $table = self::getTable();

        $tab[] = [
            'id'                => '410',
            'table'              => $table,
            'field'              => 'completename',
            'name'               => __('Leaving type', 'tam'),
            'datatype'           => 'itemlink',
            'massiveaction'      => false
        ];

        $tab[] = [
            'id'                => '411',
            'table'              => $table,
            'field'              => 'public',
            'name'               => __('Public'),
            'datatype'           => 'bool',
            'massiveaction'      => false
        ];

        $tab[] = [
            'id'                => '412',
            'table'              => $table,
            'field'              => 'natural',
            'name'               => __('Calendar day', 'tam'),
            'datatype'           => 'bool',
            'massiveaction'      => false
        ];

        $tab[] = [
            'id'                => '413',
            'table'              => $table,
            'field'              => 'compensation',
            'name'               => __('Compensation', 'tam'),
            'datatype'           => 'bool',
            'massiveaction'      => false
        ];

        $tab[] = [
            'id'                => '414',
            'table'              => $table,
            'field'              => 'holiday',
            'name'               => __('Holiday', 'tam'),
            'datatype'           => 'bool',
            'massiveaction'      => false
        ];

        return $tab;
    }

    public static function install(Migration $migration)
    {
        global $DB;

        $default_charset = DBConnection::getDefaultCharset();
        $default_collation = DBConnection::getDefaultCollation();
        $default_key_sign = DBConnection::getDefaultPrimaryKeySignOption();

        $table = self::getTable();

        if (!$DB->tableExists($table)) {
            $query = "CREATE TABLE IF NOT EXISTS $table(
				`id` INT {$default_key_sign} NOT NULL auto_increment,
				`entities_id` INT {$default_key_sign} NOT NULL DEFAULT '0',
				`is_recursive` TINYINT NOT NULL DEFAULT '0',
				`name` varchar(255) DEFAULT NULL,
				`comment` text,
				`plugin_tam_leavetypes_id` int {$default_key_sign} NOT NULL DEFAULT '0',
				`completename` text,
				`public` TINYINT NOT NULL DEFAULT '0',
				`natural` TINYINT NOT NULL DEFAULT '0',
				`compensation` TINYINT NOT NULL DEFAULT '0',
				`holiday` TINYINT NOT NULL DEFAULT '0',
				`level` int {$default_key_sign} NOT NULL DEFAULT '0',
				`ancestors_cache` longtext,
				`sons_cache` longtext,
				`is_deleted` TINYINT NOT NULL DEFAULT '0',
				PRIMARY KEY (`id`)
			) ENGINE=InnoDB DEFAULT CHARSET={$default_charset} COLLATE={$default_collation} ROW_FORMAT=DYNAMIC;";
            $DB->query($query) or die($DB->error());
        }

        $leavetype = new self();
        if (countElementsInTable(self::getTable(), ['id' => 1]) == 0) {
            $leavetype->add([
                'id' => 1,
                'name' => __('Leave without notice', 'tam'),
                'entities_id' => 0,
                'is_recursive' => 1,
                'plugin_tam_leavetypes_id' => 0,
                'completename' => __('Leave without notice', 'tam'),
            ]);
        }
    }
}
