<?php
/*
 -------------------------------------------------------------------------
 TAM plugin for GLPI
 Copyright (C) 2021-2023 by the TICgal Team.
 https://www.tic.gal
 -------------------------------------------------------------------------
 LICENSE
 This file is part of the TAM plugin.
 TAM plugin is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.
 TAM plugin is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with TAM. If not, see <http://www.gnu.org/licenses/>.
 --------------------------------------------------------------------------
 @package   TAM
 @author    the TICgal team
 @copyright Copyright (c) 2021-2023 TICgal team
 @license   AGPL License 3.0 or (at your option) any later version
            http://www.gnu.org/licenses/agpl-3.0-standalone.html
 @link      https://www.tic.gal
 @since     2021
 ----------------------------------------------------------------------
*/

use Glpi\Plugin\Hooks;
use Glpi\Application\View\TemplateRenderer;
use Glpi\Toolbox\Sanitizer;

class PluginTamManagement extends CommonGLPI
{
    public static function getTypeName($nb = 0)
    {
        return 'TAM';
    }

    public static function getIcon()
    {
        return "fa fa-stopwatch";
    }

    public function getTabNameForItem(CommonGLPI $item, $withtemplate = 0)
    {
        $profiles_id = $_SESSION['glpiactiveprofile']['id'];
        $effective_rights = ProfileRight::getProfileRights($profiles_id, ['plugin_tam_active_timer']);
        if (get_class($item) == 'Preference' && $effective_rights['plugin_tam_active_timer']) {
            return __("TAM Management", "tam") . '<i class="ms-2 '.self::getIcon().'"></i>';
        }
        return '';
    }

    public static function displayTabContentForItem(CommonGLPI $item, $tabnum = 1, $withtemplate = 0)
    {
        if (get_class($item) == 'Preference') {
            $pref = new self();
            $pref->showForm(Session::getLoginUserID());
        }
        return true;
    }

    public function showForm($ID, $options = [])
    {
        $lastTams = PluginTamManagement::getLastTams();
        $forgottenTams = PluginTamManagement::getForgottenTams();

        $template = '@tam/tam_management.html.twig';
        $options = [
            'lastTams' => $lastTams,
            'forgottenTams' => $forgottenTams,
            'rand' => mt_rand(),
        ];
        TemplateRenderer::getInstance()->display($template, $options);
    }


    public static function getLastTams($limit = 10)
    {
        global $DB;

        $current_user_id = $_SESSION['glpiID'];
        $t_table = PluginTamTam::getTable();
        $td_table = PluginTamDay::getTable();

        $sql = [
            'SELECT' => [
                "$t_table.date_start",
                "$t_table.date_end",
                "$t_table.working_time",
                "$t_table.status",
                "$t_table.reason",
                "$t_table.on_working_time",
            ],
            'FROM' => $t_table,
            'LEFT JOIN' => [
                $td_table => [
                    'ON' => [
                        $t_table => 'plugin_tam_days_id',
                        $td_table => 'id'
                    ]
                ]
            ],
            'WHERE' => [
                "$td_table.users_id" => $current_user_id
            ],
            'LIMIT' => $limit,
            'ORDER' => ["$t_table.id DESC"]
        ];

        $iterator = $DB->request($sql);

        return $iterator;
    }

    public static function getForgottenTams($limit = 10)
    {
        global $DB;

        $current_user_id = $_SESSION['glpiID'];
        $t_table = PluginTamTam::getTable();
        $td_table = PluginTamDay::getTable();

        $sql = [
            'SELECT' => [
                "$t_table.id",
                "$t_table.date_start",
                "$t_table.date_end",
                "$t_table.working_time",
                "$t_table.status",
            ],
            'FROM' => $t_table,
            'LEFT JOIN' => [
                $td_table => [
                    'ON' => [
                        $t_table => 'plugin_tam_days_id',
                        $td_table => 'id'
                    ]
                ]
            ],
            'WHERE' => [
                "$td_table.users_id" => $current_user_id,
                "$t_table.status" => PluginTamStatus::FORGOTTEN
            ],
            'LIMIT' => $limit,
            'ORDER' => ["$t_table.id DESC"]
        ];

        $iterator = $DB->request($sql);

        return $iterator;
    }

    public static function checkForgottenTam($id, $workingtime = 28800)
    {
        global $DB;

        // workingtime = 60sec * 60min * 8hours = 28800sec
        // if passes 8 hours probably means it was forgotten

        $tam = new PluginTamTam();
        $tam->getFromDB($id);

        $t_table = PluginTamTam::getTable();
        $sql = [
            'FROM' => $t_table,
            'WHERE' => [
                "$t_table.plugin_tam_days_id" => $tam->fields['plugin_tam_days_id'],
                "$t_table.date_start" => $tam->fields['date_start'],
                "$t_table.working_time" => ['>=',$workingtime],
                "$t_table.reason" => null,
            ],
        ];

        $iterator = $DB->request($sql);

        return ['count' => count($iterator)];
    }

    public static function createTicket($tam_id, $user_id, $task = false, $tickettemplate = 0)
    {
        $result = false;

        $ticket_recurrent = new TicketRecurrent();
        $concrete_class = TicketRecurrent::getConcreteClass();
        $template_class = TicketRecurrent::getTemplateClass();
        $fields_class = TicketRecurrent::getPredefinedFieldsClass();

        $template_id = $tickettemplate;
        $template = new $template_class();

        if ($template->getFromDB($template_id)) {
            $input = $concrete_class::getDefaultValues($template->fields['entities_id']);

            $fields = new $fields_class();
            $predefined = $fields->getPredefinedFields($template->fields['id'], true);

            $tam = new PluginTamTam();
            $tam->getFromDB($tam_id);

            $user = new User();
            $user->getFromDB($user_id);

            foreach ($predefined as $field => $value) {
                $vars = [];
                $copy = '';
                if (is_string($value)) {
                    $copy = $value;
                }

                while ($copy && $string = self::getStringBetween($copy, '[', ']')) {
                    $replace = '';
                    $bracket = '[' . $string . ']';
                    $sl = strtolower($string);
                    $parts = explode('.', $sl);

                    switch($parts[0]) {
                        case 'title':
                            $replace = __('Forgotten TAM', 'tam');
                            break;
                        case 'link':
                            $link = '<a title="'. __('TAM Management', 'tam') .'" 
                            href="/plugins/tam/front/forgot.php?id='.$tam_id.'" 
                            target="_blank" 
                            rel="noopener">'. __('TAM Management', 'tam') .'</a>';
                            $replace = Sanitizer::encodeHtmlSpecialChars($link);
                            break;
                        case 'info':
                            $table = '<table class="table table-responsive border"><tbody><tr class="text-center">';
                            $table .= '<td>'.__('ID').'</td>';
                            $table .= '<td>'.__('Date start', 'tam').'</td>';
                            $table .= '<td>'.__('IP start', 'tam').'</td>';
                            $table .= '<td>'.__('Origin start', 'tam').'</td>';
                            $table .= '<td>'.__('Status').'</td>';
                            $table .= '</tr><tr class="text-center">';
                            $table .= '<td>'.$tam->fields['id'].'</td>';
                            $table .= '<td>'.$tam->fields['date_start'].'</td>';
                            $table .= '<td>'.$tam->fields['ip_start'].'</td>';
                            $table .= '<td>'.$tam->fields['origin_start'].'</td>';
                            $table .= '<td>'.$tam->fields['status'].'</td>';
                            $table .= '</tr></tbody></table>';

                            $replace = Sanitizer::encodeHtmlSpecialChars($table);
                            break;
                        case 'user':
                            if (isset($parts[1]) && isset($user->fields[$parts[1]])) {
                                $replace = $user->fields[$parts[1]];
                            }
                            break;
                        case 'tam':
                            if (isset($parts[1]) && isset($tam->fields[$parts[1]])) {
                                $replace = $tam->fields[$parts[1]];
                            }
                            break;
                    }

                    if ($replace != '') {
                        $vars["$string"] = $replace;
                    }

                    $copy = str_replace("$bracket", '', $copy);
                }

                $formated_value = $value;
                foreach ($vars as $string => $replace) {
                    $bracket = '[' . $string . ']';
                    $formated_value = str_replace("$bracket", $replace, $formated_value);
                }

                $predefined[$field] = $formated_value;
            }

            $input = $predefined;

            // Set entity
            $input['entities_id'] = $template->fields['entities_id'];
            $input['_auto_import'] = true;

            // * Reassign
            $input['_users_id_requester'] = $user_id;
            $input['_users_id_assign'] = $user_id;

            $item = new $concrete_class();
            $s = 'TAM Ticket';
            $input  = Toolbox::addslashes_deep($input);

            if ($items_id = $item->add($input)) {
                $msg = sprintf(
                    __('%s %d successfully created'),
                    $s,
                    $items_id
                );
                $result = true;

                // * Create task for user
                if ($task) {
                    $content = '<p>'.__('Indicate the time of departure / completion of the task', 'tam').': </p>';
                    $ticket_task = new TicketTask();
                    $ticket_task->add([
                        'tickets_id' => $items_id,
                        'content' => Sanitizer::encodeHtmlSpecialChars($content),
                        'users_id' => $user_id,
                        'users_id_tech' => $user_id,
                        'state' => 1,
                        'is_private' => 1
                    ]);
                }
            } else {
                $msg = sprintf(
                    __('%s creation failed (check mandatory fields)'),
                    $s
                );
            }
        } else {
            $s = 'TAM Ticket';
            $msg = sprintf(
                __('%s creation failed (no template)'),
                $s
            );
        }

        $log_id = isset($template->fields['id']) ? $template->fields['id'] : 0;
        Log::history(
            $log_id,
            $template_class,
            [0, '', addslashes($msg)],
            '',
            Log::HISTORY_LOG_SIMPLE_MESSAGE
        );

        return $result;
    }

    public static function getStringBetween($string, $start, $end)
    {
        $string = ' ' . $string;
        $ini = strpos($string, $start);
        if ($ini == 0) {
            return '';
        }
        $ini += strlen($start);
        $len = strpos($string, $end, $ini) - $ini;

        return substr($string, $ini, $len);
    }

    public static function configBehaviour($entity_id)
    {
        $tam_entity = new PluginTamEntity();
        $tam_entity->getFromDBByCrit(['entities_id' => $entity_id]);

        $template = '@tam/tam_behaviour.html.twig';
        $options = [
            'action' => PluginTamEntity::getFormURL(),
            'item' => $tam_entity,
            'entity' => $entity_id
        ];
        TemplateRenderer::getInstance()->display($template, $options);
    }

    public static function fixForgottenTam($tam_id, $new_date_end)
    {
        global $DB;

        $tam = new PluginTamTam();
        $tam->getFromDB($tam_id);
        $actiontime = strtotime($new_date_end) - strtotime($tam->fields['date_start']);
        $ip = getenv("HTTP_X_FORWARDED_FOR") ? Sanitizer::encodeHtmlSpecialChars(getenv("HTTP_X_FORWARDED_FOR")) : getenv("REMOTE_ADDR");

        $table = PluginTamTam::getTable();
        $DB->insert(
            $table,
            [
                "$table.plugin_tam_days_id" => $tam->fields['plugin_tam_days_id'],
                "$table.date_start" => $tam->fields['date_start'],
                "$table.ip_start" => $tam->fields['ip_start'],
                "$table.origin_start" => $tam->fields['origin_start'],
                "$table.date_end" => $new_date_end,
                "$table.ip_end" => $ip,
                "$table.origin_end" => PluginTamTam::WEB,
                "$table.working_time" => $actiontime,
                "$table.status" => PluginTamStatus::FIXED,
                "$table.reason" => __('End date corrected', 'tam')
            ]
        );

        $timer_id = $DB->insertId();
        PluginTamBlockedlog::createLog($timer_id, Session::getLoginUserID());
    }
}
