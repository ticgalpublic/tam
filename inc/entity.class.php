<?php
/*
 -------------------------------------------------------------------------
 TAM plugin for GLPI
 Copyright (C) 2021-2023 by the TICgal Team.
 https://www.tic.gal
 -------------------------------------------------------------------------
 LICENSE
 This file is part of the TAM plugin.
 TAM plugin is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.
 TAM plugin is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with TAM. If not, see <http://www.gnu.org/licenses/>.
 --------------------------------------------------------------------------
 @package   TAM
 @author    the TICgal team
 @copyright Copyright (c) 2021-2023 TICgal team
 @license   AGPL License 3.0 or (at your option) any later version
            http://www.gnu.org/licenses/agpl-3.0-standalone.html
 @link      https://www.tic.gal
 @since     2021
 ----------------------------------------------------------------------
*/
if (!defined('GLPI_ROOT')) {
    die("Sorry. You can't access directly to this file");
}

class PluginTamEntity extends CommonDBTM
{
    public static $rightname = 'entity';

    public static function getTypeName($nb = 0)
    {
        return __('TAM', 'tam');
    }

    public function getTabNameForItem(CommonGLPI $item, $withtemplate = 0)
    {
        switch ($item::getType()) {
            case Entity::getType():
                return self::getTypeName();
                break;
        }
        return '';
    }

    public static function displayTabContentForItem(CommonGLPI $item, $tabnum = 1, $withtemplate = 0)
    {
        switch ($item::getType()) {
            case Entity::getType():
                self::displayTabForEntity($item);
                break;
        }
    }

    public function getFromDBByEntity($entity_id)
    {
        global $DB;

        $req = $DB->request(['FROM' => self::getTable(), 'WHERE' => ['entities_id' => $entity_id]]);
        if (count($req)) {
            $this->fields = $req->current($req);
            return true;
        } else {
            $DB->insert(self::getTable(), ['entities_id' => $entity_id]);
            $this->getFromDB($DB->insertId());
            return false;
        }
    }

    public static function displayTabForEntity(Entity $entity)
    {
        global $DB, $CFG_GLPI;

        $ID = $entity->getField('id');
        if (!$entity->can($ID, READ)) {
            return false;
        }

        $config_entity = new self();
        $config_entity->getFromDBByEntity($ID);

        $canedit = $entity->canEdit($ID);
        if ($canedit) {
            echo "<form name='form' method='post' action='" . self::getFormUrl() . "'>";
            echo "<input type='hidden' name='entities_id' value='$ID'>";
            echo "<input type='hidden' name='id' value='" . $config_entity->fields['id'] . "'>";
        }
        echo "<table class='tab_cadre_fixe'>";
        echo '<tr><th colspan="2">'. __('Settings', 'tam') .'</th></tr>';
        echo "<tr class='tab_bg_1'>";
        echo "<td>" . __('Show unstacked leaves', 'tam') . "</td>";
        echo "<td>";
        Dropdown::showYesNo('unstacked', $config_entity->fields['unstacked'], -1, ['use_checkbox' => true]);
        echo "</td>";
        echo "</tr>";

        echo "<tr class='tab_bg_1'>";
        echo "<td>" . __('Max holidays', 'tam') . "</td>";
        echo "<td>";
        Dropdown::showNumber('max_holiday', ['value' => $config_entity->fields['max_holiday']]);
        echo "</td>";
        echo "</tr>";

        echo "<tr class='tab_bg_1'>";
        echo "<td>" . __('Assigned Workplace', 'tam') . "</td>";
        echo "<td>";
        Location::dropdown(['value' => $config_entity->fields['locations_id']]);
        echo "</td>";
        echo "</tr>";
        if ($canedit) {
            echo "<tr class='tab_bg_1'><td class='tab_bg_2 center' colspan='4'>";
            echo "<input type='submit' name='update' value=\"" . _sx('button', 'Save') . "\" class='submit'>";
            echo "</td></tr>";
            echo "</table>";
            Html::closeForm();
        } else {
            echo "</table>";
        }

        PluginTamEntityCalendar::showCalendarForm($entity);

        PluginTamManagement::configBehaviour($ID);
    }

    public static function ungroupDays($entities_id)
    {
        global $DB;

        $req = $DB->request(['FROM' => self::getTable(), 'WHERE' => ['entities_id' => $entities_id]]);
        if ($row = $req->current()) {
            return $row['unstacked'];
        } else {
            return false;
        }
    }

    public static function maxHolidays($entities_id)
    {
        global $DB;

        $req = $DB->request(['FROM' => self::getTable(), 'WHERE' => ['entities_id' => $entities_id]]);
        if ($row = $req->current()) {
            return $row['max_holiday'];
        } else {
            return 0;
        }
    }

    public static function assignedWorkplace($entities_id)
    {
        global $DB;

        $req = $DB->request(['FROM' => self::getTable(), 'WHERE' => ['entities_id' => $entities_id]]);
        if ($row = $req->current()) {
            return $row['locations_id'];
        } else {
            return 0;
        }
    }

    public static function updateTam($entity_id, $unstacked, $max_holiday)
    {
        global $DB;

        $DB->update(self::getTable(), [
            'unstacked' => $unstacked,
            'max_holiday' => $max_holiday
        ], ['entities_id' => $entity_id]);
    }

    public static function install(Migration $migration)
    {
        global $DB;

        $default_charset = DBConnection::getDefaultCharset();
        $default_collation = DBConnection::getDefaultCollation();
        $default_key_sign = DBConnection::getDefaultPrimaryKeySignOption();

        $table = self::getTable();

        if (!$DB->tableExists($table)) {
            $migration->displayMessage("Installing $table");
            $query = "CREATE TABLE IF NOT EXISTS $table (
				`id` INT {$default_key_sign} NOT NULL auto_increment,
				`entities_id` INT {$default_key_sign} NOT NULL DEFAULT '0',
				`unstacked` TINYINT NOT NULL DEFAULT '0',
				`max_holiday` INT {$default_key_sign} NOT NULL DEFAULT '0',
				`locations_id` INT {$default_key_sign} NOT NULL DEFAULT '0',
				`create_ticket` BOOLEAN NOT NULL DEFAULT '0',
				`tickettemplates_id` INT {$default_key_sign} NOT NULL DEFAULT '0',
				`create_task` BOOLEAN NOT NULL DEFAULT '0',
				`tasktemplates_id` INT {$default_key_sign} NOT NULL DEFAULT '0',
				PRIMARY KEY (`id`)
			) ENGINE=InnoDB  DEFAULT CHARSET={$default_charset} COLLATE={$default_collation} ROW_FORMAT=DYNAMIC;";
            $DB->query($query) or die($DB->error());
        } else {
            $migration->addField($table, 'max_holiday', 'integer');
            $migration->addField($table, 'locations_id', 'integer');
            // * 1.5.0
            $migration->addField($table, 'create_ticket', 'boolean');
            $migration->addField($table, 'tickettemplates_id', 'integer');
            $migration->addField($table, 'create_task', 'boolean');
            $migration->addField($table, 'tasktemplates_id', 'integer');

            $migration->migrationOneTable($table);
        }
    }

    public static function unistall(Migration $migration)
    {
        $table = self::getTable();
        $migration->displayMessage("Uninstalling $table");
        $migration->dropTable($table);
    }
}
