<?php
/*
 -------------------------------------------------------------------------
 TAM plugin for GLPI
 Copyright (C) 2021-2023 by the TICgal Team.
 https://www.tic.gal
 -------------------------------------------------------------------------
 LICENSE
 This file is part of the TAM plugin.
 TAM plugin is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.
 TAM plugin is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with TAM. If not, see <http://www.gnu.org/licenses/>.
 --------------------------------------------------------------------------
 @package   TAM
 @author    the TICgal team
 @copyright Copyright (c) 2021-2023 TICgal team
 @license   AGPL License 3.0 or (at your option) any later version
            http://www.gnu.org/licenses/agpl-3.0-standalone.html
 @link      https://www.tic.gal
 @since     2021
 ----------------------------------------------------------------------
*/

class PluginTamLog extends CommonDBTM
{
    public static $rightname = 'plugin_tam_all_timers';

    public static function getTypeName($nb = 0)
    {
        return __("TAM", "tam");
    }

    public function canSee()
    {
        return Session::haveRight('plugin_tam_all_timers', 1);
    }

    public static function show($options = [])
    {
        global $DB;

        if (isset($_GET["start"])) {
            $start = intval($_GET["start"]);
        } else {
            $start = 0;
        }

        $sql_filters = self::convertFiltersValuesToSqlCriteria(isset($_GET['filters']) ? $_GET['filters'] : []);

        // Total Number of events
        $total_number    = countElementsInTable("glpi_plugin_tam_tams");
        $filtered_number = countElementsInTable("glpi_plugin_tam_tams", $sql_filters);

        // No Events in database
        if ($total_number < 1) {
            echo "<div class='center'>";
            echo "<table class='tab_cadre_fixe'>";
            echo "<tr><th>" . __('No historical') . "</th></tr>";
            echo "</table>";
            echo "</div><br>";
            return;
        }

        // Display the pager
        echo '<div>';
        $additional_params = isset($_GET['filters']) ? http_build_query(['filters' => $_GET['filters']]) : '';
        Html::printPager($start, $total_number, '', $additional_params);
        echo '</div>';


        // Output events
        echo "<div class='card p-3 center overflow-auto'>";
        echo "<table class='table card-table table-hover align-middle'>";

        $header = "<tr>";
        $header .= "<th>" . __('ID') . "</th>";
        $header .= "<th>" . __('User') . "</th>";
        $header .= "<th>" . __('Date start', 'tam') . "</th>";
        $header .= "<th>" . __('IP start', 'tam') . "</th>";
        $header .= "<th>" . __('Origin start', 'tam') . "</th>";
        $header .= "<th>" . __('Coordinates start', 'tam') . "</th>";
        $header .= "<th>" . __('Date end', 'tam') . "</th>";
        $header .= "<th>" . __('IP end', 'tam') . "</th>";
        $header .= "<th>" . __('Origin end', 'tam') . "</th>";
        $header .= "<th>" . __('Coordinates end', 'tam') . "</th>";
        $header .= "<th>" . __('Leave Reason', 'tam') . "</th>";
        $header .= "<th>" . __('Status', 'tam') . "</th>";

        $header .= "</tr>";

        echo "<thead>";
        echo $header;
        if (isset($_GET['filters'])) {
            echo "<tr class='log_history_filter_row'>";
            echo "<th>";
            echo "<input type='hidden' name='filters[active]' value='1' />";
            echo "</th>";
            echo "<th>";
            Dropdown::showFromArray(
                "filters[users_names]",
                PluginTamLog::getDistinctUsers(),
                [
                    'multiple'            => true,
                    'display_emptychoice' => true,
                    'values'              => isset($_GET['filters']['users_names']) ? $_GET['filters']['users_names'] : [],
                    'width'               => "100%",
                ]
            );
            echo "</th>";
            $dateValue = isset($_GET['filters']['date_start']) ? Html::cleanInputText($_GET['filters']['date_start']) : null;
            echo "<th><input type='date' name='filters[date_start]' value='$dateValue' /></th>";
            echo "<th>";
            Dropdown::showFromArray(
                "filters[ip_start]",
                PluginTamLog::getDistinctFieldValues('ip_start'),
                [
                    'multiple'            => true,
                    'display_emptychoice' => true,
                    'values'              => isset($_GET['filters']['ip_start']) ? $_GET['filters']['ip_start'] : [],
                    'width'               => "100%",
                ]
            );
            echo "</th>";
            echo "<th>";
            Dropdown::showFromArray(
                "filters[origin_start]",
                PluginTamLog::getDistinctFieldValues('origin_start'),
                [
                    'multiple'            => true,
                    'display_emptychoice' => true,
                    'values'              => isset($_GET['filters']['origin_start']) ? $_GET['filters']['origin_start'] : [],
                    'width'               => "100%",
                ]
            );
            echo "</th>";
            $dateValue = isset($_GET['filters']['date_end']) ? Html::cleanInputText($_GET['filters']['date_end']) : null;
            echo "<th><input type='date' name='filters[date_end]' value='$date_end' /></th>";
            echo "<th>";
            Dropdown::showFromArray(
                "filters[ip_end]",
                PluginTamLog::getDistinctFieldValues('ip_end'),
                [
                    'multiple'            => true,
                    'display_emptychoice' => true,
                    'values'              => isset($_GET['filters']['ip_end']) ? $_GET['filters']['ip_end'] : [],
                    'width'               => "100%",
                ]
            );
            echo "</th>";
            echo "<th>";
            Dropdown::showFromArray(
                "filters[origin_end]",
                PluginTamLog::getDistinctFieldValues('origin_end'),
                [
                    'multiple'            => true,
                    'display_emptychoice' => true,
                    'values'              => isset($_GET['filters']['origin_end']) ? $_GET['filters']['origin_end'] : [],
                    'width'               => "100%",
                ]
            );
            echo "</th>";
            echo "</tr>";
        } else {
            /*echo "<tr>";
            echo "<th colspan='8'>";
            echo "<a href='#' class='show_log_filters'>" . __('Show filters') . " <span class='fa fa-filter pointer'></span></a>";
            echo "</th>";
            echo "</tr>";*/
        }
        echo "</thead>";

        echo "<tfoot>$header</tfoot>";

        echo "<tbody>";
        if ($filtered_number > 0) {
            $origin = PluginTamTam::getOrigin();

            foreach (PluginTamLog::getHistoryData($start, $_SESSION['glpilist_limit'], $sql_filters) as $data) {
                // show line
                echo "<tr class='tab_bg_2'>";
                echo "<td>" . $data['id'] . "</td>";
                echo "<td>" . getUserName($data['user_name']) . "</td>";
                echo "<td class='tab_date'>" . $data['date_start'] . "</td>";
                echo "<td>" . $data['ip_start'] . "</td>";
                echo "<td>";
                Dropdown::showFromArray('origin_start', $origin, ['value' => $data['origin_start'], 'readonly' => true]);
                echo "</td>";
                if ($data['coordinates_start'] != ",") {
                    if (is_null($name = PluginTamLog::getNearLocation($data['coordinates_start']))) {
                        echo "<td><a href='https://www.google.com/maps/place/" . $data['coordinates_start'] . "' target='_blank'>" . $data['coordinates_start'] . "</a></td>";
                    } else {
                        echo "<td><a href='https://www.google.com/maps/place/" . $data['coordinates_start'] . "' target='_blank'>" . $name . "</a></td>";
                    }
                } else {
                    echo "<td></td>";
                }
                echo "<td class='tab_date'>" . $data['date_end'] . "</td>";
                echo "<td>" . $data['ip_end'] . "</td>";
                echo "<td>";
                Dropdown::showFromArray('origin_end', $origin, ['value' => $data['origin_end'], 'readonly' => true]);
                echo "</td>";
                if ($data['coordinates_end'] != ",") {
                    if (is_null($name = PluginTamLog::getNearLocation($data['coordinates_end']))) {
                        echo "<td><a href='https://www.google.com/maps/place/" . $data['coordinates_end'] . "' target='_blank'>" . $data['coordinates_end'] . "</a></td>";
                    } else {
                        echo "<td><a href='https://www.google.com/maps/place/" . $data['coordinates_end'] . "' target='_blank'>" . $name . "</a></td>";
                    }
                } else {
                    echo "<td></td>";
                }
                echo "<td>" . $data['reason'] . "</td>";
                echo "<td>" . $data['status'] . "</td>";
                echo "</tr>";
            }
        } else {
            echo "<tr>";
            echo "<th colspan='8'>" . __('No historical matching your filters') . "</th>";
            echo "</tr>";
        }
        echo "</tbody>";

        echo "</table>";
        echo "</div>";

        Html::printPager($start, $total_number, '', $additional_params);
    }

    public static function getNearLocation($coordinates)
    {
        global $DB;

        $coor = explode(",", $coordinates);

        $query = [
            'FROM' => new QueryExpression("(SELECT *, (((acos(sin(( $coor[0] * pi() / 180))*sin(( `latitude` * pi() / 180)) + cos(( $coor[0] * pi() /180 ))*cos(( `latitude` * pi() / 180)) * cos((( $coor[1] - `longitude`) * pi()/180)))) * 180/pi()) * 60 * 1.1515 * 1.609344)as distance FROM `glpi_locations`) myTable"),
            'WHERE' => [
                'distance' => ['<=', 0.05]
            ],
            'ORDER' => 'distance ASC',
            'LIMIT' => 1
        ];
        $req = $DB->request($query);
        if ($row = $req->current()) {
            return $row['name'];
        }
        return null;
    }

    public static function getLocationNear($coordinates)
    {
        global $DB;

        $coor = explode(",", $coordinates);

        $query = [
            'FROM' => new QueryExpression("(SELECT *, (((acos(sin(( $coor[0] * pi() / 180))*sin(( `latitude` * pi() / 180)) + cos(( $coor[0] * pi() /180 ))*cos(( `latitude` * pi() / 180)) * cos((( $coor[1] - `longitude`) * pi()/180)))) * 180/pi()) * 60 * 1.1515 * 1.609344)as distance FROM `glpi_locations`) myTable"),
            'WHERE' => [
                'distance' => ['<=', 0.05]
            ],
            'ORDER' => 'distance ASC',
            'LIMIT' => 1
        ];
        $req = $DB->request($query);
        if ($row = $req->current()) {
            return $row['id'];
        }
        return 0;
    }

    public static function convertFiltersValuesToSqlCriteria(array $filters)
    {
        $sql_filters = [];

        if (isset($filters['affected_fields']) && !empty($filters['affected_fields'])) {
            $affected_field_crit = [];
            foreach ($filters['affected_fields'] as $index => $affected_field) {
                $affected_field_crit[$index] = [];
                foreach (explode(";", $affected_field) as $var) {
                    if (1 === preg_match('/^(?P<key>.+):(?P<operator>.*):(?P<values>.+)$/', $var, $matches)) {
                        $key = $matches['key'];
                        $operator = $matches['operator'];
                        // Each field can have multiple values for a given filter
                        $values = explode(',', $matches['values']);

                        // linked_action and id_search_option are stored as integers
                        if (in_array($key, ['linked_action', 'id_search_option'])) {
                            $values = array_map('intval', $values);
                        }

                        if (!empty($operator)) {
                            $affected_field_crit[$index][$operator][$key] = $values;
                        } else {
                            $affected_field_crit[$index][$key] = $values;
                        }
                    }
                }
            }
            $sql_filters[] = [
                'OR' => $affected_field_crit
            ];
        }

        if (isset($filters['date']) && !empty($filters['date'])) {
            $sql_filters['date_mod'] = ['LIKE', "%{$filters['date']}%"];
        }

        if (isset($filters['linked_actions']) && !empty($filters['linked_actions'])) {
            $linked_action_crit = [];
            foreach ($filters['linked_actions'] as $linked_action) {
                if ($linked_action === 'other') {
                    $linked_action_crit[] = ['linked_action' => self::HISTORY_LOG_SIMPLE_MESSAGE];
                    $linked_action_crit[] = ['linked_action' => ['>=', self::HISTORY_PLUGIN]];
                } else {
                    $linked_action_crit[] = ['linked_action' => $linked_action];
                }
            }
            $sql_filters[] = ['OR' => $linked_action_crit];
        }

        if (isset($filters['users_names']) && !empty($filters['users_names'])) {
            $sql_filters['user_name'] = $filters['users_names'];
        }

        return $sql_filters;
    }

    public static function getDistinctUsers()
    {
        global $DB;

        $iterator = $DB->request([
            'SELECT' => [
                'users_id', 'name'
            ],
            'DISTINCT' => true,
            'FROM' => 'glpi_plugin_tam_tams',
            'INNER JOIN' => [
                'glpi_users' => [
                    'FKEY' => [
                        'glpi_plugin_tam_tams' => 'users_id',
                        'glpi_users' => 'id'
                    ]
                ]
            ],
            'ORDER'  => 'id DESC'
        ]);

        $values = [];
        foreach ($iterator as $data) {
            if (empty($data['users_id'])) {
                continue;
            }
            $values[$data['users_id']] = $data['name'];
        }

        asort($values, SORT_NATURAL | SORT_FLAG_CASE);

        return $values;
    }

    public static function getDistinctFieldValues($field)
    {
        global $DB;

        $iterator = $DB->request([
            'SELECT'  => $field,
            'FROM'    => 'glpi_plugin_tam_tams',
            'GROUPBY' => $field,
        ]);

        $values = [];
        foreach ($iterator as $data) {
            $values[] = $data[$field];
        }

        return $values;
    }

    public static function getHistoryData($start = 0, $limit = 0, array $sqlfilters = [])
    {
        global $DB;

        $plugin = new Plugin();
        $query = [
            'SELECT' => [
                'glpi_plugin_tam_tams.*',
                'users_id',
            ],
            'FROM'   => 'glpi_plugin_tam_tams',
            'INNER JOIN' => [
                'glpi_plugin_tam_days' => [
                    'FKEY' => [
                        'glpi_plugin_tam_tams' => 'plugin_tam_days_id',
                        'glpi_plugin_tam_days' => 'id'
                    ]
                ]
            ],
            'WHERE'  => $sqlfilters,
            'ORDER'  => 'glpi_plugin_tam_tams.id DESC'
        ];

        if ($limit) {
            $query['START'] = (int)$start;
            $query['LIMIT'] = (int)$limit;
        }

        $iterator = $DB->request($query);

        $values = [];
        foreach ($iterator as $data) {
            $tmp = [];
            $tmp['id'] = $data['id'];
            $tmp['user_name'] = $data['users_id'];
            $tmp['date_start'] = $data['date_start'];
            $tmp['ip_start'] = $data['ip_start'];
            $tmp['origin_start'] = $data['origin_start'];
            $tmp['date_end'] = $data['date_end'];
            $tmp['ip_end'] = $data['ip_end'];
            $tmp['origin_end'] = $data['origin_end'];
            $tmp['coordinates_start'] = ",";
            $tmp['coordinates_end'] = ",";
            $tmp['reason'] = $data['reason'];
            $tmp['status'] = PluginTamStatus::getStatusName($data['status']);
            if ($plugin->isActivated('gappextended')) {
                $geolocation = new PluginGappextendedGeolocation();
                if ($geolocation->getFromDBByCrit(['completed' => PluginGappextendedGeolocation::START, 'itemtype' => PluginTamTam::getType(), 'items_id' => $data['id']])) {
                    $tmp['coordinates_start'] = $geolocation->fields['latitude'] . "," . $geolocation->fields['longitude'];
                }
                if ($geolocation->getFromDBByCrit(['completed' => PluginGappextendedGeolocation::END, 'itemtype' => PluginTamTam::getType(), 'items_id' => $data['id']])) {
                    $tmp['coordinates_end'] = $geolocation->fields['latitude'] . "," . $geolocation->fields['longitude'];
                }
            }
            $values[] = $tmp;
        }
        return $values;
    }
}
