<?php
/*
 -------------------------------------------------------------------------
 TAM plugin for GLPI
 Copyright (C) 2021-2023 by the TICgal Team.
 https://www.tic.gal
 -------------------------------------------------------------------------
 LICENSE
 This file is part of the TAM plugin.
 TAM plugin is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.
 TAM plugin is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with TAM. If not, see <http://www.gnu.org/licenses/>.
 --------------------------------------------------------------------------
 @package   TAM
 @author    the TICgal team
 @copyright Copyright (c) 2021-2023 TICgal team
 @license   AGPL License 3.0 or (at your option) any later version
            http://www.gnu.org/licenses/agpl-3.0-standalone.html
 @link      https://www.tic.gal
 @since     2021
 ----------------------------------------------------------------------
*/
class PluginTamCalendar extends CommonDBTM
{
    public function getTabNameForItem(CommonGLPI $item, $withtemplate = 0)
    {
        if (get_class($item) == 'Calendar') {
            return __("TAM", "tam");
        }
        return '';
    }

    public static function displayTabContentForItem(CommonGLPI $item, $tabnum = 1, $withtemplate = 0)
    {
        if (get_class($item) == 'Calendar') {
            self::displayTabForCalendar($item);
        }
        return true;
    }

    public function getFromDBByCalendar($calendars_id)
    {
        global $DB;

        $req = $DB->request(['FROM' => self::getTable(), 'WHERE' => ['calendars_id' => $calendars_id]]);
        if (count($req)) {
            $this->fields = $req->current($req);
            return true;
        } else {
            $DB->insert(self::getTable(), ['calendars_id' => $calendars_id, 'break' => 0, 'standard_hour' => 0]);
            $this->fields['break'] = 0;
            $this->fields['standard_hour'] = 0;
            return false;
        }
    }

    public static function displayTabForCalendar(Calendar $calendar)
    {
        global $DB, $CFG_GLPI;

        $ID = $calendar->getField('id');

        $break = new self();
        $break->getFromDBByCalendar($ID);

        $rand = mt_rand();
        echo "<form name='break_form$rand' id='break_form$rand' method='post' action='";
        echo self::getFormUrl() . "'>";
        echo "<table class='tab_cadre_fixe'>";
        echo "<tr class='tab_bg_1'>";
        echo "<td>" . __('Break Time', 'tam') . "</td><td>";
        Dropdown::showTimeStamp('break', ['rand' => $rand, 'value' => $break->fields['break'], 'max' => HOUR_TIMESTAMP]);
        echo "</td></tr>";
        echo "<tr class='tab_bg_1'>";
        echo "<td>" . __('Standard working hours', 'tam') . "</td><td>";
        Dropdown::showTimeStamp('standard_hour', ['rand' => $rand, 'value' => $break->fields['standard_hour'], 'max' => DAY_TIMESTAMP]);
        echo "</td></tr>";
        echo "<tr><td class='tab_bg_2 right'>";
        echo "<input type='submit' name='update' value='" . _sx('button', 'Update') . "' class='submit'>";
        echo "</td></tr>";
        echo "</table>";
        echo "<input type='hidden' name='calendars_id' value='$ID'>";
        Html::closeForm();
    }

    public static function updateCalendar($input)
    {
        global $DB;

        $DB->update(self::getTable(), ['break' => $input['break'], 'standard_hour' => $input['standard_hour']], ['calendars_id' => $input['calendars_id']]);
    }

    public static function install(Migration $migration)
    {
        global $DB;

        $default_charset = DBConnection::getDefaultCharset();
        $default_collation = DBConnection::getDefaultCollation();
        $default_key_sign = DBConnection::getDefaultPrimaryKeySignOption();

        $table = self::getTable();

        if (!$DB->tableExists($table)) {
            $migration->displayMessage("Installing $table");
            $query = "CREATE TABLE IF NOT EXISTS $table (
				`id` INT {$default_key_sign} NOT NULL auto_increment,
				`calendars_id` INT {$default_key_sign} NOT NULL,
				`break` INT {$default_key_sign} NOT NULL,
				`standard_hour` INT {$default_key_sign} NOT NULL,
				PRIMARY KEY (`id`)
			) ENGINE=InnoDB  DEFAULT CHARSET={$default_charset} COLLATE={$default_collation} ROW_FORMAT=DYNAMIC;";
            $DB->query($query) or die($DB->error());
        }
    }
}
