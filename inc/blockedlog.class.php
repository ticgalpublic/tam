<?php
/*
 -------------------------------------------------------------------------
 TAM plugin for GLPI
 Copyright (C) 2021-2023 by the TICgal Team.
 https://www.tic.gal
 -------------------------------------------------------------------------
 LICENSE
 This file is part of the TAM plugin.
 TAM plugin is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.
 TAM plugin is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with TAM. If not, see <http://www.gnu.org/licenses/>.
 --------------------------------------------------------------------------
 @package   TAM
 @author    the TICgal team
 @copyright Copyright (c) 2021-2023 TICgal team
 @license   AGPL License 3.0 or (at your option) any later version
            http://www.gnu.org/licenses/agpl-3.0-standalone.html
 @link      https://www.tic.gal
 @since     2021
 ----------------------------------------------------------------------
*/
use Glpi\Toolbox\Sanitizer;

class PluginTamBlockedlog extends CommonDBTM
{
    public static function generateRandomPassword()
    {
        $lowercase = "qwertyuiopasdfghjklzxcvbnm";
        $uppercase = "ASDFGHJKLZXCVBNMQWERTYUIOP";
        $numbers = "1234567890";
        $length = 24;

        $password = '';
        $nbofchar = round($length / 3);
        $nbofcharlast = ($length - 2 * $nbofchar);

        $max = strlen($lowercase) - 1;
        for ($x = 0; $x < $nbofchar; $x++) {
            $tmp = mt_rand(0, $max);
            $password .= $lowercase[$tmp];
        }
        $max = strlen($uppercase) - 1;
        for ($x = 0; $x < $nbofchar; $x++) {
            $tmp = mt_rand(0, $max);
            $password .= $uppercase[$tmp];
        }
        $max = strlen($numbers) - 1;
        for ($x = 0; $x < $nbofcharlast; $x++) {
            $tmp = mt_rand(0, $max);
            $password .= $numbers[$tmp];
        }

        $password = str_shuffle($password);

        return $password;
    }

    public static function hashEncrypt($data)
    {
        return hash('sha256', $data);
    }

    public static function createLog($tam_id, $user_id)
    {
        global $DB;

        $table = PluginTamTam::getTable();
        $sql = [
            'FROM' => $table,
            'WHERE' => [
                "$table.id" => $tam_id
            ]
        ];

        $iterator = $DB->request($sql);
        if ($row = $iterator->current()) {
            $day_id = $row['plugin_tam_days_id'];
            $fingerprint = self::createFingerprint($row);
            $signature = self::createSignature($fingerprint, $row);
            $record = Sanitizer::sanitize(print_r($row, true));
            $validation = 1;
            $date = date('Y-m-d');

            $logtable = self::getTable();
            $res = $DB->insert(
                $logtable,
                [
                    "$logtable.tams_id" => $tam_id,
                    "$logtable.users_id" => $user_id,
                    "$logtable.days_id" => $day_id,
                    "$logtable.fingerprint" => $fingerprint,
                    "$logtable.signature" => $signature,
                    "$logtable.record" => $record,
                    "$logtable.validation" => $validation,
                    "$logtable.date_creation" => $date,
                    "$logtable.date_mod" => $date
                ]
            );

            return true;
        }

        return false;
    }

    public static function createFingerprint($data)
    {
        $fingerprint = self::hashEncrypt(print_r($data, true) . self::generateRandomPassword());

        return $fingerprint;
    }

    public static function createSignature($fingerprint, $data)
    {
        $signature = self::hashEncrypt($fingerprint . print_r($data, true));

        return $signature;
    }

    public static function checkSignature($blockedlog)
    {
        global $DB;

        $table = PluginTamTam::getTable();
        $sql = [
            'FROM' => $table,
            'WHERE' => [
                "$table.id" => $blockedlog['tams_id']
            ]
        ];

        $iterator = $DB->request($sql);
        if ($row = $iterator->current()) {
            $signature = self::createSignature($blockedlog['fingerprint'], $row);
            if ($signature === $blockedlog['signature']) {
                return true;
            }
        }

        return false;
    }

    public static function updateValidation($blockedlog_id, $ignore_validation = false)
    {
        global $DB;

        $validation = false;
        $table = self::getTable();

        if (!$ignore_validation) {
            $sql = [
                'FROM' => $table,
                'WHERE' => [
                    "$table.id" => $blockedlog_id
                ]
            ];

            $iterator = $DB->request($sql);
            if ($row = $iterator->current()) {
                $validation = self::checkSignature($row);
            }
        }

        $DB->update(
            $table,
            [
                "$table.validation" => $validation
            ],
            [
                'WHERE' => [
                    "$table.id" => $blockedlog_id
                ]
            ]
        );
    }

    public static function install(Migration $migration)
    {
        global $DB;

        $default_charset = DBConnection::getDefaultCharset();
        $default_collation = DBConnection::getDefaultCollation();
        $default_key_sign = DBConnection::getDefaultPrimaryKeySignOption();

        $table = self::getTable();
        $date = date('Y-m-d');

        if (!$DB->tableExists($table)) {
            $migration->displayMessage("Installing $table");
            $query = "CREATE TABLE IF NOT EXISTS $table (
				`id` INT {$default_key_sign} NOT NULL auto_increment,
				`tams_id` INT {$default_key_sign} NOT NULL,
				`users_id` INT {$default_key_sign} NOT NULL,
				`days_id` INT {$default_key_sign} NOT NULL,
				`fingerprint` VARCHAR(255) COLLATE {$default_collation} NOT NULL,
				`signature` VARCHAR(255) COLLATE {$default_collation} NOT NULL,
				`record` TEXT COLLATE {$default_collation} NOT NULL,
                `validation` BOOLEAN NOT NULL DEFAULT 1,
				`date_creation` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
				`date_mod` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
				PRIMARY KEY (`id`)
			) ENGINE=InnoDB  DEFAULT CHARSET={$default_charset} COLLATE={$default_collation} ROW_FORMAT=DYNAMIC;";
            $DB->query($query) or die($DB->error());
        }
    }
}
