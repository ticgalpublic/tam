<?php
/*
 -------------------------------------------------------------------------
 TAM plugin for GLPI
 Copyright (C) 2021-2023 by the TICgal Team.
 https://www.tic.gal
 -------------------------------------------------------------------------
 LICENSE
 This file is part of the TAM plugin.
 TAM plugin is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.
 TAM plugin is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with TAM. If not, see <http://www.gnu.org/licenses/>.
 --------------------------------------------------------------------------
 @package   TAM
 @author    the TICgal team
 @copyright Copyright (c) 2021-2023 TICgal team
 @license   AGPL License 3.0 or (at your option) any later version
            http://www.gnu.org/licenses/agpl-3.0-standalone.html
 @link      https://www.tic.gal
 @since     2021
 ----------------------------------------------------------------------
*/

if (!defined('GLPI_ROOT')) {
    die("Sorry. You can't access directly to this file");
}

class PluginTamEntityCalendar extends CommonDBTM
{
    public static function canPurge()
    {
        return true;
    }

    public static function getTypeName($nb = 0)
    {
        return __('TAM Calendar', 'tam');
    }

    public static function cronInfo($name)
    {
        switch ($name) {
            case 'changeCalendar':
                return ['description' => __('Change calendar', 'tam')];
        }
        return [];
    }

    public static function cronchangeCalendar($task)
    {
        global $DB, $CFG_GLPI;

        $query = [
            'SELECT' => [
                'id',
                'name'
            ],
            'FROM' => Entity::getTable(),

        ];

        foreach ($DB->request($query) as $id => $row) {
            $sql = [
                'SELECT' => [
                    'calendars_id'
                ],
                'FROM' => self::getTable(),
                'WHERE' => [
                    'entities_id' => $row['id'],
                    'date' => date("m/d")
                ]
            ];
            $req = $DB->request($sql);
            if ($cal = $req->current()) {
                $entity = new Entity();
                $entity->getFromDB($row['id']);
                $entity->update(['id' => $row['id'], 'calendars_id' => $cal['calendars_id']]);
                $task->addVolume(1);
                $task->log("<a href='" . Entity::getFormURLWithID($row["id"]) . "'>" . $row['name'] . " - " . __("Changed calendar", "tam") . "</a>");
            }
        }
        return true;
    }

    public static function showCalendarForm(Entity $entity)
    {
        global $DB;

        $ID = $entity->getField('id');
        $canedit = $entity->canEdit($ID);
        if ($canedit) {
            echo "<form name='form' method='post' action='" . self::getFormUrl() . "'>";
            echo "<input type='hidden' name='entities_id' value='$ID'>";
            echo "<table class='tab_cadre_fixe'>";
            echo '<tr><th colspan="2">'. __('Calendar', 'tam') .'</th></tr>';
            echo "<tr class='tab_bg_1'>";
            echo "<td>" . __('Begin date') . "</td>";
            echo "<td>";
            echo "<div class='d-flex flex-nowrap flatpickr' id='showdate'>";
            echo "<input type='text' name='date_start' data-input>";
            echo "<div class='d-flex align-items-middle px-2'>";
            echo "<a class='input-button align-middle p-2 flex-grow-1' data-toggle><i class='far fa-calendar-alt fa-lg pointer'></i></a>";
            echo "<a class='p-2 flex-grow-1' data-clear  title='" . __s('Clear') . "'><i class='fa fa-times-circle pointer'></i></a>";
            echo "</div>";
            echo "</div>";
            echo "</td>";

            $script = <<<JAVASCRIPT
		$(function(){
			$('#showdate').flatpickr({
				altInput:true,
				altFormat: 'd F',
				enableTime: false,
				dateFormat: 'm/d',
				wrap:true
			});
		});

JAVASCRIPT;
            echo Html::scriptBlock($script);
            echo "</tr>";
            echo "<tr>";
            echo "<td>" . __('Calendar') . "</td>";
            echo "<td>";
            Calendar::dropdown();
            echo "</td>";
            echo "</tr>";
            echo "<tr><td class='tab_bg_2 center' colspan='2'>";
            echo "<input type='submit' name='add_calendar' value=\"" . _sx('button', 'Add') . "\" class='submit'>";
            echo "</td></tr>";
            echo "</table>";
            Html::closeForm();
        }

        $query = [
            'FROM' => self::getTable(),
            'WHERE' => [
                'entities_id' => $ID,
            ]
        ];
        if ($req = $DB->request($query)) {
            echo "<div class='card'>";
            if (count($req)) {
                if ($canedit) {
                    Html::openMassiveActionsForm('mass' . __CLASS__);
                    $massiveactionparams = ['container' => 'mass' . __CLASS__, 'specific_actions' => ['purge' => _x('button', 'Delete permanently')]];
                    Html::showMassiveActions($massiveactionparams);
                }
                echo "<table class='table card-table table-hover'>";
                echo "<tr>";
                if ($canedit) {
                    echo "<th width='10'>";
                    echo Html::getCheckAllAsCheckbox('mass' . __CLASS__);
                    echo "</th>";
                }
                echo "<th>" . __('Begin date') . "</th>";
                echo "<th>" . __('Calendar') . "</th>";
                echo "</tr>";
                foreach ($req as $id => $row) {
                    echo "<tr class='tab_bg_2'>";
                    if ($canedit) {
                        echo "<td width='10'>";
                        Html::showMassiveActionCheckBox(__CLASS__, $row["id"]);
                        echo "</td>";
                    }
                    echo "<td>" . date("Y-m-d", strtotime($row['date'])) . "</td>";
                    echo "<td>" . $row['calendars_id'] . "</td>";
                    echo "</tr>\n";
                }
                echo "</table>";
                if ($canedit) {
                    $massiveactionparams['ontop'] = false;
                    Html::showMassiveActions($massiveactionparams);
                    Html::closeForm();
                }
            } else {
                echo "<table class='tab_cadre_fixe'>";
                echo "<tr><th>" . __('No item found') . "</th></tr>";
                echo "</table>\n";
            }
            echo "</div>";
        }
    }

    public static function addCalendar($params)
    {
        global $DB;

        $DB->insert(self::getTable(), ['entities_id' => $params['entities_id'], 'calendars_id' => $params['calendars_id'], 'date' => $params['date_start']]);
    }

    public static function install(Migration $migration)
    {
        global $DB;

        $default_charset = DBConnection::getDefaultCharset();
        $default_collation = DBConnection::getDefaultCollation();
        $default_key_sign = DBConnection::getDefaultPrimaryKeySignOption();

        $table = self::getTable();

        if (!$DB->tableExists($table)) {
            $migration->displayMessage("Installing $table");
            $query = "CREATE TABLE IF NOT EXISTS $table (
				`id` INT {$default_key_sign} NOT NULL auto_increment,
				`entities_id` INT {$default_key_sign} NOT NULL,
				`calendars_id` INT {$default_key_sign} NOT NULL,
				`date` VARCHAR(255) NOT NULL,
				PRIMARY KEY (`id`)
			) ENGINE=InnoDB  DEFAULT CHARSET={$default_charset} COLLATE={$default_collation} ROW_FORMAT=DYNAMIC;";
            $DB->query($query) or die($DB->error());
        }
    }
}
