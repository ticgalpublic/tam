<?php
/*
 -------------------------------------------------------------------------
 TAM plugin for GLPI
 Copyright (C) 2021-2023 by the TICgal Team.
 https://www.tic.gal
 -------------------------------------------------------------------------
 LICENSE
 This file is part of the TAM plugin.
 TAM plugin is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.
 TAM plugin is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with TAM. If not, see <http://www.gnu.org/licenses/>.
 --------------------------------------------------------------------------
 @package   TAM
 @author    the TICgal team
 @copyright Copyright (c) 2021-2023 TICgal team
 @license   AGPL License 3.0 or (at your option) any later version
            http://www.gnu.org/licenses/agpl-3.0-standalone.html
 @link      https://www.tic.gal
 @since     2021
 ----------------------------------------------------------------------
*/

class PluginTamLeave extends CommonDBTM
{
    public $dohistory = true;
    public static $rightname = 'plugin_tam_leave';

    public static function getTypeName($nb = 0)
    {
        return $nb > 1 ? __('Leavings', 'tam') : __('Leave', 'tam');
    }

    public static function cronInfo($name)
    {
        switch ($name) {
            case 'markLeave':
                return ['description' => __('Mark day as leave', 'tam')];
        }
        return [];
    }

    public static function cronmarkLeave($task)
    {
        global $DB, $CFG_GLPI;

        $date = date("Y-m-d", strtotime(date("Y-m-d") . ' -1 day'));
        $query = [
            'SELECT' => 'glpi_profiles_users.users_id AS id',
            'DISTINCT' => true,
            'FROM' => 'glpi_profiles_users',
            'INNER JOIN' => [
                'glpi_profilerights' => [
                    'FKEY' => [
                        'glpi_profiles_users' => 'profiles_id',
                        'glpi_profilerights' => 'profiles_id'
                    ]
                ],
                'glpi_users' => [
                    'FKEY' => [
                        'glpi_users' => 'id',
                        'glpi_profiles_users' => 'users_id'
                    ]
                ]
            ],
            'WHERE' => [
                'glpi_profilerights.name' => 'plugin_tam_active_timer',
                'glpi_profilerights.rights' => 1,
                'glpi_users.is_active' => 1,
                [
                    'OR' => [
                        'glpi_users.end_date' => null,
                        'NOT' => [
                            'glpi_users.end_date' => ['<', $date]
                        ]
                    ]
                ]
            ]
        ];
        $users = [];
        foreach ($DB->request($query) as $id => $row) {
            $user = new User();
            $user->getFromDB($row['id']);

            if (!$calendars_id = PluginTamUserCalendar::getCalendar($row['id'])) {
                $calendars_id = Entity::getUsedConfig('calendars_id', $user->fields["entities_id"], 'calendars_id', 0);
            }
            if (PluginTamPdf::isWorkingDay($date, $calendars_id)) {
                $users[] = $row['id'];
            }
        }

        if (count($users) > 0) {
            $query = [
                'SELECT' => [
                    'users_id'
                ],
                'FROM' => 'glpi_plugin_tam_days',
                'WHERE' => [
                    'date' => $date
                ]
            ];
            foreach ($DB->request($query) as $id => $row) {
                if (($key = array_search($row['users_id'], $users)) !== false) {
                    unset($users[$key]);
                }
            }

            if (count($users) > 0) {
                $query = [
                    'SELECT' => [
                        'users_id'
                    ],
                    'FROM' => 'glpi_plugin_tam_leaves',
                    'WHERE' => [
                        'date_start' => ['<=', $date],
                        'date_end' => ['>=', $date],
                        'is_deleted' => 0
                    ]
                ];
                foreach ($DB->request($query) as $id => $row) {
                    if (($key = array_search($row['users_id'], $users)) !== false) {
                        unset($users[$key]);
                    }
                }
                if (count($users) > 0) {
                    foreach ($users as $key => $value) {
                        $leave  = new self();
                        $user = new User();
                        $user->getFromDB($value);
                        $toadd = [
                            'users_id' => $value,
                            'date_start' => $date,
                            'date_end' => $date,
                            'entities_id' => $user->fields["entities_id"],
                            'plugin_tam_leavetypes_id' => 1
                        ];
                        $leave->add($toadd);
                        $task->addVolume(1);
                        $task->log("<a href='" . User::getFormURLWithID($value, false) . "'>" . sprintf(__("User %s was on leave without notice", "tam"), getUserName($value)) . "</a>");
                    }
                }
            }
        }

        return true;
    }

    public static function canView()
    {
        return Session::haveRight('planning', 1);
    }

    public static function canCreate()
    {
        return Session::haveRight('plugin_tam_leave', 1);
    }

    public function defineTabs($options = [])
    {
        $ong = [];
        $this->addDefaultFormTab($ong);
        $this->addStandardTab('Ticket', $ong, $options);
        $this->addStandardTab('Log', $ong, $options);

        return $ong;
    }

    public function rawSearchOptions()
    {
        $tab = parent::rawSearchOptions();

        $tab[] = [
            'id' => '2',
            'table' => $this->getTable(),
            'field' => 'id',
            'name' => __('ID'),
            'datatype' => 'number'
        ];

        $tab[] = [
            'id' => '3',
            'table' => 'glpi_users',
            'field' => 'name',
            'name' => __('User'),
            'datatype' => 'itemlink'
        ];

        $tab[] = [
            'id' => '19',
            'table' => $this->getTable(),
            'field' => 'date_mod',
            'name' => __('Last update'),
            'datatype' => 'datetime',
            'massiveaction' => false
        ];

        $tab[] = [
            'id' => '400',
            'table' => $this->getTable(),
            'field' => 'date_start',
            'name' => __('Start date'),
            'datatype' => 'datetime'
        ];

        $tab[] = [
            'id' => '401',
            'table' => $this->getTable(),
            'field' => 'date_end',
            'name' => __('End date'),
            'datatype' => 'datetime'
        ];

        $tab[] = [
            'id' => '402',
            'table' => $this->getTable(),
            'field' => 'comment',
            'name' => __('Comment'),
            'datatype' => 'text'
        ];

        $tab = array_merge($tab, PluginTamLeaveType::rawSearchOptionsToAdd());

        return $tab;
    }

    public function showForm($ID, $options = [])
    {
        global $CFG_GLPI, $DB;

        $this->initForm($ID, $options);
        $this->showFormHeader($options);
        $rand = mt_rand();

        $canedit = $this->canUpdate();

        if (array_key_exists('begin', $options)) {
            $this->fields['date_start'] = date("Y-m-d", strtotime($options['begin']));
        }

        if (array_key_exists('end', $options)) {
            $this->fields['date_end'] = date("Y-m-d", strtotime($options['end']));
        }

        echo "<tr class='tab_bg_1'>";
        echo "<td>" . __('User') . "</td>";
        echo "<td colspan=2>";
        if ($canedit) {
            User::dropdown([
                'name' => 'users_id',
                'value' => $this->fields['users_id'],
                'right' => 'all',
            ]);
        } else {
            echo getUserName($this->fields['users_id']);
        }
        echo "</td>";
        echo "</tr>";

        echo "<tr class='tab_bg_1'>";
        echo "<td>" . __('Start date') . "</td>";
        echo "<td>";
        if ($canedit) {
            Html::showDateField('date_start', [
                'value' => $this->fields['date_start'],
                'maybeempty' => false,
            ]);
        } else {
            echo Html::convDate($this->fields["date_start"]);
        }
        echo "</td>";
        echo "<td>" . __('End date') . "</td>";
        echo "<td>";
        if ($canedit) {
            Html::showDateField('date_end', [
                'value' => $this->fields['date_end'],
                'maybeempty' => false,
            ]);
        } else {
            echo Html::convDate($this->fields["date_end"]);
        }
        echo "</td>";
        echo "</tr>";

        echo "<tr class='tab_bg_1'>";
        echo "<td>" . __('Type') . "</td>";
        echo "<td>";
        if ($canedit) {
            Dropdown::show('PluginTamLeaveType', [
                'name' => 'plugin_tam_leavetypes_id',
                'value' => $this->fields['plugin_tam_leavetypes_id'],
            ]);
        } else {
            echo Dropdown::getDropdownName('glpi_plugin_tam_leavetypes', $this->fields['plugin_tam_leavetypes_id']);
        }
        echo "</td>";
        echo "<td>" . __('Calendar') . "</td>";
        echo "<td>";
        if ($canedit) {
            Dropdown::show('Calendar', [
                'name' => 'calendars_id',
                'value' => ($this->fields['calendars_id']) ? $this->fields['calendars_id'] : 1,
                'display_emptychoice' => false
            ]);
        } else {
            echo Dropdown::getDropdownName('Calendar', $this->fields['calendars_id']);
        }
        echo "</td>";
        echo "</tr>";

        echo "<tr class='tab_bg_1'>";
        echo "<td>" . __("Comments") . ":  </td>";
        echo "<td colspan='3' align='center'>";
        if ($canedit) {
            echo "<textarea cols='40' rows='3' name='comment'>" . $this->fields["comment"] . "</textarea>";
        } else {
            echo $this->fields["comment"];
        }
        echo "</td></tr>";

        if ($canedit) {
            $this->showFormButtons($options);
        } else {
            echo "</table></div>";
            Html::closeForm();
        }
    }

    public static function addName(CommonDBTM $item)
    {
        if ($item::getType() === self::getType()) {
            if (!is_array($item->input) || !count($item->input)) {
                return;
            }
            $name = getUserName($item->input['users_id']) . "-" . Dropdown::getDropdownName('glpi_plugin_tam_leavetypes', $item->input['plugin_tam_leavetypes_id']) . "-" . $item->input['date_start'] . "-" . $item->input['date_end'];
            $item->input['name'] = $name;
        }
    }

    public static function checkLeave($id)
    {
        global $DB;

        $query = [
            'FROM' => self::getTable(),
            'WHERE' => [
                'users_id' => $id,
                'date_start' => ['<=', date("Y-m-d")],
                'date_end' => ['>=', date("Y-m-d")],
                'is_deleted' => 0
            ]
        ];

        $req = $DB->request($query);
        if ($row = $req->current()) {
            return true;
        } else {
            return false;
        }
    }

    public static function populatePlanning($options = [])
    {
        global $DB, $CFG_GLPI;

        $default_options = [
            'color' => '',
            'event_type_color' => '',
            'check_planned' => false,
            'display_done_events' => false,
        ];

        $options = array_merge($default_options, $options);

        $interv  = [];
        $leave = new self();

        if (!isset($options['begin']) || ($options['begin'] == 'NULL') || !isset($options['end']) || ($options['end'] == 'NULL')) {
            return $interv;
        }
        if (!$options['display_done_events']) {
            return $interv;
        }

        $who       = $options['who'];
        $whogroup  = $options['whogroup'];
        $begin     = $options['begin'];
        $end       = $options['end'];
        // Get items to print
        $ASSIGN = "";

        $query = [
            'SELECT' => [
                self::getTable() . '.id',
                'users_id',
                self::getTable() . '.name',
                'date_start',
                'date_end',
                self::getTable() . '.comment',
                'completename',
                'public',
                self::getTable() . '.plugin_tam_leavetypes_id'
            ],
            'FROM' => self::getTable(),
            'INNER JOIN' => [
                'glpi_plugin_tam_leavetypes' => [
                    'FKEY' => [
                        self::getTable() => 'plugin_tam_leavetypes_id',
                        'glpi_plugin_tam_leavetypes' => 'id'
                    ]
                ]
            ],
            'WHERE' => [
                'date_start' => ['<=', $end],
                'date_end' => ['>=', $begin],
                self::getTable() . '.is_deleted' => 0,
            ],
            'ORDER' => [
                'date_start ASC'
            ]
        ];

        if (!Session::haveRight('plugin_tam_leave', 1)) {
            $query['WHERE'][] = ["public" => true];
        }

        if ($whogroup === "mine") {
            if (isset($_SESSION["glpigroups"])) {
                $whogroup = $_SESSION['glpigroups'];
            } elseif ($who > 0) {
                $whogroup = array_column(Group_User::getUserGroups($who), 'id');
            }
        }
        if ($who > 0) {
            $query['WHERE'][] = ["users_id" => $who];
        }
        if ($whogroup > 0) {
            $query['WHERE'][] = ["groups_id" => $whogroup];
        }

        foreach ($DB->request($query) as $id => $row) {
            $key = $row["date_start"] . "$$" . "PluginTamLeave" . $row["id"];
            $interv[$key]['color']            = $options['color'];
            $interv[$key]['event_type_color'] = $options['event_type_color'];
            $interv[$key]["itemtype"]         = 'PluginTamLeave';
            $interv[$key]["id"]               = $row["id"];
            $interv[$key]["users_id"]         = $row["users_id"];

            $interv[$key]["begin"] = date("Y-m-d 00:00:00", strtotime($row['date_start']));
            $interv[$key]["end"] = date("Y-m-d 23:59:59", strtotime($row['date_end']));

            $interv[$key]["name"]    = Html::resume_text($row["name"], $CFG_GLPI["cut"]);
            $interv[$key]["type"]    = $row["completename"];
            $interv[$key]["content"] = Html::resume_text($row["comment"], $CFG_GLPI["cut"]);
            $link = PluginTamLeave::getFormURLWithID($row['id']);
            $interv[$key]["url"]     = $link;
            $interv[$key]["ajaxurl"] = $CFG_GLPI["root_doc"] . "/ajax/planning.php?action=edit_event_form&itemtype=PluginTamLeave&id=" . $row['id'] . "&url=" . $interv[$key]["url"];

            $leave->getFromDB($row["id"]);
            $interv[$key]["editable"] = false;
        }

        return $interv;
    }

    public static function displayPlanningItem(array $val, $who, $type = "", $complete = 0)
    {
        $rand = mt_rand();
        $dbu  = new DbUtils();
        $html = "";

        if ($complete) {
            if ($val["end"]) {
                $html .= "<strong>" . __('End date') . "</strong> : " . Html::convdatetime($val["end"]) . "<br>";
            }
            if ($val["users_id"] && $who != 0) {
                $html .= "<strong>" . __('User') . "</strong> : " . $dbu->getUserName($val["users_id"]) . "<br>";
            }
            if ($val["type"]) {
                $html .= "<strong>" . PluginTamLeaveType::getTypeName(1) . "</strong> : " . $val["type"] . "<br>";
            }
            if ($val["content"]) {
                $html .= "<strong>" . __('Description') . "</strong> : " . $val["content"];
            }
        } else {
            $html .= "<div class='event-description'>" . $val["content"] . "</div>";
            $html .= Html::showToolTip($val["content"], ['applyto' => "activity_" . $val["id"] . $rand, 'display' => false]);
        }
        return $html;
    }

    public function post_addItem()
    {
        global $DB;

        parent::post_addItem();

        $query = [
            'FROM' => PluginTamShift::getTable(),
            'WHERE' => [
                'users_id' => $this->fields['users_id'],
                'begin' => ['<=', $this->fields['date_end']],
                'NOT' => ['rrule' => '']
            ]
        ];
        foreach ($DB->request($query) as $id => $row) {
            $rrule = json_decode($row['rrule'], true);
            if (!isset($rrule['exceptions'])) {
                $rrule['exceptions'] = [];
            }

            $start = date("Y-m-d", strtotime($this->fields['date_start']));
            $finish = date("Y-m-d", strtotime($this->fields['date_end']));
            if ($start == $finish) {
                if (!in_array($start, $rrule['exceptions'])) {
                    $rrule['exceptions'][] = $start;
                }
            } else {
                for ($i = $start; $i <= $finish; $i = date('Y-m-d', strtotime($i . ' + 1 days'))) {
                    if (!in_array($i, $rrule['exceptions'])) {
                        $rrule['exceptions'][] = $i;
                    }
                }
            }
            $input = ['rrule' => json_encode($rrule)];
            $DB->update(PluginTamShift::getTable(), $input, ['id' => $row['id']]);
        }
    }

    public function post_updateItem($history = 1)
    {
        global $DB;

        parent::post_updateItem($history);

        if (isset($this->oldvalues['date_end']) || isset($this->oldvalues['date_start'])) {
            $query = [
                'FROM' => PluginTamShift::getTable(),
                'WHERE' => [
                    'users_id' => $this->fields['users_id'],
                    'begin' => ['<=', $this->fields['date_end']],
                    'NOT' => ['rrule' => '']
                ]
            ];
            foreach ($DB->request($query) as $id => $row) {
                $rrule = json_decode($row['rrule'], true);
                $start = date("Y-m-d", strtotime($this->fields['date_start']));
                $finish = date("Y-m-d", strtotime($this->fields['date_end']));
                if ($start == $finish) {
                    if (!in_array($start, $rrule['exceptions'])) {
                        $rrule['exceptions'][] = $start;
                    }
                } else {
                    for ($i = $start; $i <= $finish; $i = date('Y-m-d', strtotime($i . ' + 1 days'))) {
                        if (!in_array($i, $rrule['exceptions'])) {
                            $rrule['exceptions'][] = $i;
                        }
                    }
                }
                if (isset($this->oldvalues['date_start'])) {
                    $old_start = date("Y-m-d", strtotime($this->oldvalues['date_start']));
                    if ($old_start < $start) {
                        $end = $start;
                        for ($i = $old_start; $i < $end; $i = date('Y-m-d', strtotime($i . ' + 1 days'))) {
                            if (in_array($i, $rrule['exceptions'])) {
                                $key = array_search($i, $rrule['exceptions']);
                                unset($rrule['exceptions'][$key]);
                            }
                        }
                    }
                }
                if (isset($this->oldvalues['date_end'])) {
                    $old_end = date("Y-m-d", strtotime($this->oldvalues['date_end']));
                    if ($finish < $old_end) {
                        for ($i = date('Y-m-d', strtotime($finish . ' + 1 days')); $i < $old_end; $i = date('Y-m-d', strtotime($i . ' + 1 days'))) {
                            if (in_array($i, $rrule['exceptions'])) {
                                $key = array_search($i, $rrule['exceptions']);
                                unset($rrule['exceptions'][$key]);
                            }
                        }
                    }
                }
                $input = ['rrule' => json_encode($rrule)];
                $DB->update(PluginTamShift::getTable(), $input, ['id' => $row['id']]);
            }
        }
    }

    public static function install(Migration $migration)
    {
        global $DB;

        $default_charset = DBConnection::getDefaultCharset();
        $default_collation = DBConnection::getDefaultCollation();
        $default_key_sign = DBConnection::getDefaultPrimaryKeySignOption();

        $table = self::getTable();

        if (!$DB->tableExists($table)) {
            $query = "CREATE TABLE IF NOT EXISTS $table(
				`id` INT {$default_key_sign} NOT NULL auto_increment,
				`name` varchar(255) DEFAULT NULL,
				`users_id` INT {$default_key_sign} NOT NULL,
				`plugin_tam_leavetypes_id` INT {$default_key_sign} NOT NULL,
				`date_start` DATE DEFAULT NULL,
				`date_end` DATE DEFAULT NULL,
				`comment` text,
				`calendars_id` INT {$default_key_sign} NOT NULL,
				`date_mod` TIMESTAMP NULL DEFAULT NULL,
				`date_creation` TIMESTAMP NULL DEFAULT NULL,
				`entities_id` INT {$default_key_sign} NOT NULL DEFAULT '0',
				`is_recursive` TINYINT NOT NULL DEFAULT '0',
				`is_deleted` TINYINT NOT NULL DEFAULT '0',
				`is_helpdesk_visible` tinyint NOT NULL default '1',
				PRIMARY KEY (`id`)
			) ENGINE=InnoDB DEFAULT CHARSET={$default_charset} COLLATE={$default_collation} ROW_FORMAT=DYNAMIC;";
            $DB->query($query) or die($DB->error());
        }
    }
}
