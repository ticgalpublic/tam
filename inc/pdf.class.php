<?php
/*
 -------------------------------------------------------------------------
 TAM plugin for GLPI
 Copyright (C) 2021-2023 by the TICgal Team.
 https://www.tic.gal
 -------------------------------------------------------------------------
 LICENSE
 This file is part of the TAM plugin.
 TAM plugin is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.
 TAM plugin is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with TAM. If not, see <http://www.gnu.org/licenses/>.
 --------------------------------------------------------------------------
 @package   TAM
 @author    the TICgal team
 @copyright Copyright (c) 2021-2023 TICgal team
 @license   AGPL License 3.0 or (at your option) any later version
            http://www.gnu.org/licenses/agpl-3.0-standalone.html
 @link      https://www.tic.gal
 @since     2021
 ----------------------------------------------------------------------
*/
class PluginTamPdf extends CommonGLPI
{
    public function showForm($options = [])
    {
        global $CFG_GLPI;

        $title = getUserName($options['id']) . " " . date("d/m/Y", strtotime($options['start'])) . " - " . date("d/m/Y", strtotime($options['end']));

        $events = PluginTamTam::constructPDFEvents($options);

        $user = new User();
        $user->getFromDB($options['id']);
        $entity = new Entity();
        $entity->getFromDB($user->fields["entities_id"]);

        if (!$calendars_id = PluginTamUserCalendar::getCalendar($options['id'])) {
            $calendars_id = Entity::getUsedConfig('calendars_strategy', $user->fields["entities_id"], 'calendars_id', 0);
        }

        $completename = $user->getFriendlyName();

        $unstacked = PluginTamEntity::ungroupDays($user->fields["entities_id"]);
        $maxholiday = PluginTamEntity::maxHolidays($user->fields["entities_id"]);

        $html = <<<EOF
<style>
	header{
		text-align: center;
	}
	table{
		width: 90%;
		margin: 0 auto;

	}
	tr th{
		color:white;
		background-color:#808080;
		text-shadow: 1px 1px 0 #444;

	}
	table#time{
		border-collapse: collapse;
		font-size: x-small;
	}
	table#time th{
		text-align: center;
		vertical-align: middle;
	}
	table#time td{
		text-align: right;
		vertical-align: middle;
		padding: 2px;
	}
	table#time td:nth-child(1){
		white-space:nowrap;
	}
	table#time tr:nth-child(even) {
		background-color: #f2f2f2;
	}
	tr.leave{
		background-color: rgba(0, 255, 255,0.25) !important;
		border:1px solid silver;
	}
	tr.close_time{
		background-color: rgba(127, 255, 212,0.25) !important;
		border:1px solid silver;
	}
	@page { 
		size: landscape;
		margin:0;
	}
	*{
		-webkit-print-color-adjust: exact !important;
		color-adjust: exact !important;
		font-family: sans-serif;
	}
	table#sum{
		margin-top: 10px;
		border-collapse: collapse;
		font-size: x-small;
	}
	table#sum th{
		text-align: center;
		vertical-align: middle;
	}
	table#sum td{
		text-align: center;
		vertical-align: middle;
		padding: 2px;
	}
	table#total{
		page-break-inside: avoid;
	}
	table#total td{
		vertical-align: top;
	}
	table#total table{
		padding-top:5px;
		font-size: x-small;
		border-collapse: collapse;
	}
	table#total .total{
		font-size: x-small;
		text-align: center;
		vertical-align: top;
		font-weight: bold;
	}
	table#total .total div{
		color: white;
		background-color: #808080;
		text-shadow: 1px 1px 0 #444;
		padding: 0.1%;
	}
	table#leave{
		text-align: end;
	}
	table#close_time{
		text-align:end;
	}
	table#header{
		padding-top:10px;
		padding-bottom:10px;
		font-size: x-small;
	}
	span{
		font-weight: bold;
		font-size: 12px;
		vertical-align: super;
	}
	p{
		text-align: start;
		display: block;
		margin-block-start: 0px;
		margin-block-end: 0px;
		margin-inline-start: 20px;
		margin-inline-end: 0px;
	}
	table#signature{
		width: 50%;
		padding-top:10px;
		font-size: x-small;
	}
	table#signature th,table#signature td{
		text-align: left;
	}
	table#signature td{
		/*font-size: 12px;*/
	}
	div.anual{
		width: 90%;
		margin: 0 auto;
		background-color: #d9d9d9;
		margin-top: 10px;
	}
	div.anual table{
		width:100%;
		margin: 0 auto !important;
	}
	div.anual span{
		position:relative;
		left:50%;
		text-shadow: 1px 1px 0 white;
	}
	div.anual table.anual th{
		text-align: center;
		vertical-align: top;
	}
	/* 1.5.0 Blockedlog */
	tr.blockedlog{
		background-color: #ED1010;
	}
</style>
EOF;

        $html .= "<table id='header'>";
        $html .= "<tbody>";
        $html .= "<tr>";
        $html .= "<td colspan=2><span>" . __('Company', 'tam') . "</span><p>" . $entity->fields['comment'] . "</p></td>";
        $html .= "<td><span>" . __('Printing date', 'tam') . "</span><p>" . date("d/m/Y H:i") . "</p></td>";
        $html .= "</tr>";
        $html .= "<tr>";
        $html .= "<td><span>" . __('Complete name') . "</span><p>" . $completename . "</p></td>";
        $html .= "<td><span>" . __('NIF', 'tam') . "</span><p>" . $user->fields['registration_number'] . "</p></td>";
        $html .= "<td><span>" . __('Date range', 'tam') . "</span><p>" . date("d/m/Y", strtotime($options['start'])) . " - " . date("d/m/Y", strtotime($options['end'])) . "</p></td>";
        $html .= "</tr>";
        $html .= "</tbody>";
        $html .= "</table>";

        $html .= "<table id='time'><thead><tr><th>" . __('Day') . "</th><th>" . __('Details', 'tam') . "</th><th>" . __('Hours worked', 'tam') . "</th><th>" . __('Break time', 'tam') . "</th><th>" . __('Working day', 'tam') . "</th><th>" . __('Diff', 'tam') . "</th><th>" . __('Standard Working time', 'tam') . "</th><th>" . __('Standard Working Diff', 'tam') . "</th><th>" . __('Comments') . "</th></tr></thead><tbody>";

        $total_workforgotten = 0;
        $total_realworkfor = 0;
        $total_breaks = 0;
        $total_standard = 0;
        $total_leves = 0;
        $leaves = [];
        $close_time = [];
        $total_close_time = 0;
        $num_days = 0;
        $num_forgotten = 0;
        $total_shift = 0;
        foreach ($events as $key => $value) {
            if ($value['leave']) {
                if (!array_key_exists($value['type'], $leaves)) {
                    $leaves[$value['type']]['name'] = $value['name'];
                    $leaves[$value['type']]['total'] = 0;
                }

                if (!$leavecalendar = $value['calendar']) {
                    $leavecalendar = Entity::getUsedConfig('calendars_strategy', $user->fields["entities_id"], 'calendars_id', 0);
                }
                $start = date("Y-m-d", strtotime($value['start']));
                $end = date("Y-m-d", strtotime($value['end']));
                if ($start < date("Y-m-d", strtotime($options['start']))) {
                    $start = date("Y-m-d", strtotime($options['start']));
                }
                if ($end > date("Y-m-d", strtotime($options['end']))) {
                    $end = date("Y-m-d", strtotime($options['end']));
                }

                if ($value['natural']) {
                    if ($start == $end) {
                        $html .= "<tr class='leave'><td>" . $start . "</td>";
                        $html .= "<td>-------</td>";
                        $html .= "<td>-------</td>";
                        $html .= "<td>-------</td>";
                        if ($value['compensation']) {
                            $time = self::timeofDay($start, $leavecalendar);
                            if (self::isWorkingDay($start, $leavecalendar)) {
                                $timestand = self::standardTime($leavecalendar);
                            } else {
                                $timestand = 0;
                            }
                            $total_realworkfor += $time;
                            $total_standard += $timestand;

                            $html .= "<td>" . HTML::timestampToString($time, false, false) . "</td>";
                            $html .= "<td style='color:green;'>" . HTML::timestampToString($time, false, false) . "</td>";
                            $html .= "<td>" . HTML::timestampToString($timestand, false, false) . "</td>";
                            $html .= "<td style='color:green;'>" . HTML::timestampToString($timestand, false, false) . "</td>";
                        } else {
                            $html .= "<td>-------</td>";
                            $html .= "<td>-------</td>";
                            $html .= "<td>-------</td>";
                            $html .= "<td>-------</td>";
                        }
                        $html .= "<td>" . $value['name'] . "</td></tr>";
                        $total_leves += 1;
                        $leaves[$value['type']]['total'] += 1;
                    } else {
                        if ($unstacked) {
                            for ($i = $start; $i <= $end; $i = date('Y-m-d', strtotime($i . ' + 1 days'))) {
                                $html .= "<tr class='leave'><td>" . $i . "</td>";
                                $html .= "<td>-------</td>";
                                $html .= "<td>-------</td>";
                                $html .= "<td>-------</td>";
                                if ($value['compensation']) {
                                    $time = self::timeofDay($i, $leavecalendar);
                                    if (self::isWorkingDay($i, $leavecalendar)) {
                                        $timestand = self::standardTime($leavecalendar);
                                    } else {
                                        $timestand = 0;
                                    }
                                    $total_realworkfor += $time;
                                    $total_standard += $timestand;

                                    $html .= "<td>" . HTML::timestampToString($time, false, false) . "</td>";
                                    $html .= "<td style='color:green;'>" . HTML::timestampToString($time, false, false) . "</td>";
                                    $html .= "<td>" . HTML::timestampToString($timestand, false, false) . "</td>";
                                    $html .= "<td style='color:green;'>" . HTML::timestampToString($timestand, false, false) . "</td>";
                                } else {
                                    $html .= "<td>-------</td>";
                                    $html .= "<td>-------</td>";
                                    $html .= "<td>-------</td>";
                                    $html .= "<td>-------</td>";
                                }
                                $html .= "<td>" . $value['name'] . "</td></tr>";
                            }
                        } else {
                            $html .= "<tr class='leave'><td>" . $start . " <--> " . $end . "</td>";
                            $html .= "<td>-------</td>";
                            $html .= "<td>-------</td>";
                            $html .= "<td>-------</td>";
                            if ($value['compensation']) {
                                $time = 0;
                                $timestand = 0;
                                for ($i = $start; $i <= $end; $i = date('Y-m-d', strtotime($i . ' + 1 days'))) {
                                    $time += self::timeofDay($i, $leavecalendar);
                                    if (self::isWorkingDay($i, $leavecalendar)) {
                                        $timestand += self::standardTime($leavecalendar);
                                    }
                                }
                                $total_realworkfor += $time;
                                $total_standard += $timestand;
                                $html .= "<td>" . HTML::timestampToString($time, false, false) . "</td>";
                                $html .= "<td style='color:green;'>" . HTML::timestampToString($time, false, false) . "</td>";
                                $html .= "<td>" . HTML::timestampToString($timestand, false, false) . "</td>";
                                $html .= "<td style='color:green;'>" . HTML::timestampToString($timestand, false, false) . "</td>";
                            } else {
                                $html .= "<td>-------</td>";
                                $html .= "<td>-------</td>";
                                $html .= "<td>-------</td>";
                                $html .= "<td>-------</td>";
                            }
                            $html .= "<td>" . $value['name'] . "</td></tr>";
                        }

                        $interval = date_diff(date_create($start), date_create($end));
                        $total_leves += ($interval->format('%a') + 1);
                        $leaves[$value['type']]['total'] += ($interval->format('%a') + 1);
                    }
                } else {
                    if ($start == $end) {
                        if (self::isWorkingDay($start, $leavecalendar)) {
                            $html .= "<tr class='leave'><td>" . $start . "</td>";
                            $html .= "<td>-------</td>";
                            $html .= "<td>-------</td>";
                            $html .= "<td>-------</td>";
                            if ($value['compensation']) {
                                $time = self::timeofDay($start, $leavecalendar);
                                $timestand = self::standardTime($leavecalendar);
                                $total_realworkfor += $time;
                                $total_standard += $timestand;

                                $html .= "<td>" . HTML::timestampToString($time, false, false) . "</td>";
                                $html .= "<td style='color:green;'>" . HTML::timestampToString($time, false, false) . "</td>";
                                $html .= "<td>" . HTML::timestampToString($timestand, false, false) . "</td>";
                                $html .= "<td style='color:green;'>" . HTML::timestampToString($timestand, false, false) . "</td>";
                            } else {
                                $html .= "<td>-------</td>";
                                $html .= "<td>-------</td>";
                                $html .= "<td>-------</td>";
                                $html .= "<td>-------</td>";
                            }
                            $html .= "<td>" . $value['name'] . "</td></tr>";
                            $total_leves += 1;
                            $leaves[$value['type']]['total'] += 1;
                        }
                    } elseif ($unstacked) {
                        for ($i = $start; $i <= $end; $i = date('Y-m-d', strtotime($i . ' + 1 days'))) {
                            if (self::isWorkingDay($i, $leavecalendar)) {
                                $leaves[$value['type']]['total'] += 1;
                                $total_leves += 1;
                                $html .= "<tr class='leave'><td>" . $i . "</td>";
                                $html .= "<td>-------</td>";
                                $html .= "<td>-------</td>";
                                $html .= "<td>-------</td>";
                                if ($value['compensation']) {
                                    //$total_realwork+=self::timeofDay($i,$calendars_id);
                                    $total_realworkfor += self::timeofDay($i, $leavecalendar);
                                    $total_standard += self::standardTime($leavecalendar);

                                    $html .= "<td>" . HTML::timestampToString(self::timeofDay($i, $leavecalendar), false, false) . "</td>";
                                    $html .= "<td style='color:green;'>" . HTML::timestampToString(self::timeofDay($i, $leavecalendar), false, false) . "</td>";
                                    $html .= "<td>" . HTML::timestampToString(self::standardTime($leavecalendar), false, false) . "</td>";
                                    $html .= "<td style='color:green;'>" . HTML::timestampToString(self::standardTime($leavecalendar), false, false) . "</td>";
                                } else {
                                    $html .= "<td>-------</td>";
                                    $html .= "<td>-------</td>";
                                    $html .= "<td>-------</td>";
                                    $html .= "<td>-------</td>";
                                }
                                $html .= "<td>" . $value['name'] . "</td></tr>";
                            }
                        }
                    } else {
                        $flagstart = $start;
                        $flag = 0;
                        $flagtime = 0;
                        $flagstandard = 0;
                        for ($i = $start; $i <= $end; $i = date('Y-m-d', strtotime($i . ' + 1 days'))) {
                            if (self::isWorkingDay($i, $leavecalendar)) {
                                if ($value['compensation']) {
                                    $flagtime += self::timeofDay($i, $leavecalendar);
                                    $flagstandard += self::standardTime($leavecalendar);
                                }
                                $leaves[$value['type']]['total'] += 1;
                                $total_leves += 1;
                                if ($flag == 1) {
                                    $flagstart = $i;
                                    $flag = 0;
                                }
                            } elseif ($flag == 0) {
                                $date = date('Y-m-d', strtotime($i . ' - 1 days'));
                                if ($flagstart == $date) {
                                    $html .= "<tr class='leave'><td>" . $flagstart . "</td>";
                                    $html .= "<td>-------</td>";
                                    $html .= "<td>-------</td>";
                                    $html .= "<td>-------</td>";
                                    if ($value['compensation']) {
                                        //$total_realwork+=$flagtime;
                                        $total_realworkfor += $flagtime;
                                        $total_standard += $flagstandard;

                                        $html .= "<td>" . HTML::timestampToString($flagtime, false, false) . "</td>";
                                        $html .= "<td style='color:green;'>" . HTML::timestampToString($flagtime, false, false) . "</td>";
                                        $html .= "<td>" . HTML::timestampToString($flagstandard, false, false) . "</td>";
                                        $html .= "<td style='color:green;'>" . HTML::timestampToString($flagstandard, false, false) . "</td>";
                                    } else {
                                        $html .= "<td>-------</td>";
                                        $html .= "<td>-------</td>";
                                        $html .= "<td>-------</td>";
                                        $html .= "<td>-------</td>";
                                    }
                                    $html .= "<td>" . $value['name'] . "</td></tr>";
                                } else {
                                    $html .= "<tr class='leave'><td>" . $flagstart . " <--> " . $date . "</td>";
                                    $html .= "<td>-------</td>";
                                    $html .= "<td>-------</td>";
                                    $html .= "<td>-------</td>";
                                    if ($value['compensation']) {
                                        $total_realworkfor += $flagtime;
                                        $total_standard += $flagstandard;

                                        $html .= "<td>" . HTML::timestampToString($flagtime, false, false) . "</td>";
                                        $html .= "<td style='color:green;'>" . HTML::timestampToString($flagtime, false, false) . "</td>";
                                        $html .= "<td>" . HTML::timestampToString($flagstandard, false, false) . "</td>";
                                        $html .= "<td style='color:green;'>" . HTML::timestampToString($flagstandard, false, false) . "</td>";
                                    } else {
                                        $html .= "<td>-------</td>";
                                        $html .= "<td>-------</td>";
                                        $html .= "<td>-------</td>";
                                        $html .= "<td>-------</td>";
                                    }
                                    $html .= "<td>" . $value['name'] . "</td></tr>";
                                }
                                $flag = 1;
                                $flagtime = 0;
                                $flagstandard = 0;
                            }
                        }
                        if ($flag == 0) {
                            if ($flagstart == $end) {
                                $html .= "<tr class='leave'><td>" . $flagstart . "</td>";
                                $html .= "<td>-------</td>";
                                $html .= "<td>-------</td>";
                                $html .= "<td>-------</td>";
                                if ($value['compensation']) {
                                    $total_realworkfor += $flagtime;
                                    $total_standard += $flagstandard;

                                    $html .= "<td>" . HTML::timestampToString($flagtime, false, false) . "</td>";
                                    $html .= "<td style='color:green;'>" . HTML::timestampToString($flagtime, false, false) . "</td>";
                                    $html .= "<td>" . HTML::timestampToString($flagstandard, false, false) . "</td>";
                                    $html .= "<td style='color:green;'>" . HTML::timestampToString($flagstandard, false, false) . "</td>";
                                } else {
                                    $html .= "<td>-------</td>";
                                    $html .= "<td>-------</td>";
                                    $html .= "<td>-------</td>";
                                    $html .= "<td>-------</td>";
                                }
                                $html .= "<td>" . $value['name'] . "</td></tr>";
                            } else {
                                $html .= "<tr class='leave'><td>" . $flagstart . " <--> " . $end . "</td>";
                                $html .= "<td>-------</td>";
                                $html .= "<td>-------</td>";
                                $html .= "<td>-------</td>";
                                if ($value['compensation']) {
                                    //$total_realwork+=$flagtime;
                                    $total_realworkfor += $flagtime;
                                    $total_standard += $flagstandard;

                                    $html .= "<td>" . HTML::timestampToString($flagtime, false, false) . "</td>";
                                    $html .= "<td style='color:green;'>" . HTML::timestampToString($flagtime, false, false) . "</td>";
                                    $html .= "<td>" . HTML::timestampToString($flagstandard, false, false) . "</td>";
                                    $html .= "<td style='color:green;'>" . HTML::timestampToString($flagstandard, false, false) . "</td>";
                                } else {
                                    $html .= "<td>-------</td>";
                                    $html .= "<td>-------</td>";
                                    $html .= "<td>-------</td>";
                                    $html .= "<td>-------</td>";
                                }
                                $html .= "<td>" . $value['name'] . "</td></tr>";
                            }
                        }
                    }
                }
            } elseif (array_key_exists('close_time', $value)) {
                $start = date("Y-m-d", strtotime($value['start']));
                $end = date("Y-m-d", strtotime($value['end']));
                if ($start < date("Y-m-d", strtotime($options['start']))) {
                    $start = date("Y-m-d", strtotime($options['start']));
                }
                if ($end > date("Y-m-d", strtotime($options['end']))) {
                    $end = date("Y-m-d", strtotime($options['end']));
                }

                if ($start == $end) {
                    if (self::timeofDay($start, $calendars_id) > 0) {
                        $html .= "<tr class='close_time'><td>" . $start . "</td>";
                        $html .= "<td>-------</td>";
                        $html .= "<td>-------</td>";
                        $html .= "<td>-------</td>";
                        $html .= "<td>-------</td>";
                        $html .= "<td>-------</td>";
                        $html .= "<td>-------</td>";
                        $html .= "<td>-------</td>";
                        $html .= "<td>" . $value['name'] . "</td></tr>";
                        $total_close_time += 1;
                        $close_time[] = [
                            'name' => $value['name'],
                            'total' => 1
                        ];
                    }
                } elseif ($unstacked) {
                    $flagtotal = 0;
                    for ($i = $start; $i <= $end; $i = date('Y-m-d', strtotime($i . ' + 1 days'))) {
                        if (self::timeofDay($i, $calendars_id) > 0) {
                            $html .= "<tr class='close_time'><td>" . $i . "</td>";
                            $html .= "<td>-------</td>";
                            $html .= "<td>-------</td>";
                            $html .= "<td>-------</td>";
                            $html .= "<td>-------</td>";
                            $html .= "<td>-------</td>";
                            $html .= "<td>-------</td>";
                            $html .= "<td>-------</td>";
                            $html .= "<td>" . $value['name'] . "</td></tr>";
                            $flagtotal++;
                            $total_close_time += 1;
                        }
                    }
                    $close_time[] = [
                        'name' => $value['name'],
                        'total' => $flagtotal
                    ];
                } else {
                    $flagstart = $start;
                    $flag = 0;
                    $flagtotal = 0;
                    for ($i = $start; $i <= $end; $i = date('Y-m-d', strtotime($i . ' + 1 days'))) {
                        if (self::timeofDay($i, $calendars_id) > 0) {
                            $flagtotal += 1;
                            $total_close_time += 1;
                            if ($flag == 1) {
                                $flagstart = $i;
                                $flag = 0;
                            }
                        } elseif ($flag == 0) {
                            $date = date('Y-m-d', strtotime($i . ' - 1 days'));
                            if ($flagstart == $date) {
                                $html .= "<tr class='close_time'><td>" . $flagstart . "</td>";
                                $html .= "<td>-------</td>";
                                $html .= "<td>-------</td>";
                                $html .= "<td>-------</td>";
                                $html .= "<td>-------</td>";
                                $html .= "<td>-------</td>";
                                $html .= "<td>-------</td>";
                                $html .= "<td>-------</td>";
                                $html .= "<td>" . $value['name'] . "</td></tr>";
                            } else {
                                $html .= "<tr class='close_time'><td>" . $flagstart . " <--> " . $date . "</td>";
                                $html .= "<td>-------</td>";
                                $html .= "<td>-------</td>";
                                $html .= "<td>-------</td>";
                                $html .= "<td>-------</td>";
                                $html .= "<td>-------</td>";
                                $html .= "<td>-------</td>";
                                $html .= "<td>-------</td>";
                                $html .= "<td>" . $value['name'] . "</td></tr>";
                            }
                            $flag = 1;
                        }
                    }
                    if ($flag == 0) {
                        if ($flagstart == $end) {
                            $html .= "<tr class='close_time'><td>" . $flagstart . "</td>";
                            $html .= "<td>-------</td>";
                            $html .= "<td>-------</td>";
                            $html .= "<td>-------</td>";
                            $html .= "<td>-------</td>";
                            $html .= "<td>-------</td>";
                            $html .= "<td>-------</td>";
                            $html .= "<td>-------</td>";
                            $html .= "<td>" . $value['name'] . "</td></tr>";
                        } else {
                            $html .= "<tr class='close_time'><td>" . $flagstart . " <--> " . $end . "</td>";
                            $html .= "<td>-------</td>";
                            $html .= "<td>-------</td>";
                            $html .= "<td>-------</td>";
                            $html .= "<td>-------</td>";
                            $html .= "<td>-------</td>";
                            $html .= "<td>-------</td>";
                            $html .= "<td>-------</td>";
                            $html .= "<td>" . $value['name'] . "</td></tr>";
                        }
                    }
                    if ($flagtotal > 0) {
                        $close_time[] = [
                            'name' => $value['name'],
                            'total' => $flagtotal
                        ];
                    }
                }
            } elseif (array_key_exists('shift', $value)) {
                $total_shift += $value['active_time'];
                $html .= "<tr><td>" . date("Y-m-d", strtotime($key)) . "</td>";
                $html .= "<td>-------</td>";
                $html .= "<td>-------</td>";
                $html .= "<td>-------</td>";
                $html .= "<td>-------</td>";
                $html .= "<td>-------</td>";
                $html .= "<td>-------</td>";
                $html .= "<td>-------</td>";
                $html .= "<td>" . $value['name'] . "</td></tr>";
            } else {
                // * 1.5.0 Blockedlog
                global $DB;
                $blockedlog = '';
                $blockedlog_table = PluginTamBlockedlog::getTable();
                $like_date = date("Y-m-d", strtotime($key));
                $iterator = $DB->request([
                    'FROM' => $blockedlog_table,
                    'WHERE' => [
                        "$blockedlog_table.users_id" => $options['id'],
                        "$blockedlog_table.date_creation" => ['LIKE', "$like_date%"]
                    ]
                ]);

                foreach ($iterator as $tam_log) {
                    $bool = PluginTamBlockedlog::checkSignature($tam_log);
                    if (!$bool) {
                        $blockedlog = 'blockedlog';
                        PluginTamBlockedlog::updateValidation($tam_log['id'], true);
                        break;
                    }
                }

                $html .= "<tr class='".$blockedlog."'><td>" . date("Y-m-d", strtotime($key)) . "</td>";
                $html .= "<td>";
                $segments = '';
                $count = 0;
                foreach ($value['segment'] as $index => $segment) {
                    if ($count == 0) {
                        $segments = $segment;
                        $count++;
                    } else {
                        $segments .= " | " . $segment;
                    }
                }
                $html .= $segments . "</td>";
                $html .= "<td>" . HTML::timestampToString($value['total_time'], false, false) . "</td>";
                if ($value['active_time'] == 0) {
                    $break = 0;
                } else {
                    $break = $value['break'];
                }
                $html .= "<td>" . HTML::timestampToString($break, false) . "</td>";
                $html .= "<td>" . HTML::timestampToString($value['active_time'], false, false) . "</td>";
                $diff = ($value['total_time'] + $break) - $value['active_time'];
                if ($value['active_time'] == 0) {
                    $color = "style='color:magenta;'";
                } elseif ($diff > 0) {
                    $color = "style='color:red;'";
                } else {
                    $color = "style='color:green;'";
                }
                $html .= "<td " . $color . ">" . HTML::timestampToString($diff, false, false) . "</td>";
                $html .= "<td>" . HTML::timestampToString($value['standard_hour'], false, false) . "</td>";
                $diff_standard = ($value['total_time'] + $break) - $value['standard_hour'];
                if ($value['standard_hour'] == 0) {
                    $color = "style='color:magenta;'";
                } elseif ($diff_standard > 0) {
                    $color = "style='color:red;'";
                } else {
                    $color = "style='color:green;'";
                }
                $html .= "<td " . $color . ">" . HTML::timestampToString($diff_standard, false, false) . "</td>";

                $comment = "";
                if (array_key_exists('forgotten', $value)) {
                    $comment = ($value['comment'] === '') ? __("Forgotten timer", 'tam') : $value['comment'];
                    $num_forgotten++;
                }
                $html .= "<td>" . $comment . "</td></tr>";
                $total_breaks += $break;
                $total_standard += $value['standard_hour'];
                $total_workforgotten += $value['total_time'] + $break;
                $total_realworkfor += $value['active_time'];
                $num_days++;
            }
        }
        $html .= "</tbody></table>";

        //Totales
        $html .= "<table id='sum'><thead>";
        $html .= "<tr><th>" . __("Working days", 'tam') . "</th><th>" . __("Total Hours", 'tam') . "</th><th>" . __("Total Shift Hours", 'tam') . "</th><th>" . __("Total Breaks", 'tam') . "</th><th>" . __("Total Working day", 'tam') . "</th><th>" . __("Total Diff", 'tam') . "</th><th>" . __("Total Standard", 'tam') . "</th><th>" . __("Total Standard Diff", 'tam') . "</th></tr>";
        $html .= "</thead><tbody>";
        $html .= "<tr>";
        $html .= "<td>" . ($num_days + $total_leves) . "</td>";
        $html .= "<td>" . HTML::timestampToString($total_workforgotten, false, false) . "</td>";
        $html .= "<td>" . HTML::timestampToString($total_shift, false, false) . "</td>";
        $html .= "<td>" . HTML::timestampToString($total_breaks, false, false) . "</td>";
        $html .= "<td>" . HTML::timestampToString($total_realworkfor, false, false) . "</td>";
        $tfdiff = $total_workforgotten - $total_realworkfor;
        if ($tfdiff > 0) {
            $color = "style='color:red;'";
        } else {
            $color = "style='color:green;'";
        }
        $html .= "<td " . $color . ">" . HTML::timestampToString($tfdiff, false, false) . "</td>";
        $html .= "<td>" . HTML::timestampToString($total_standard, false, false) . "</td>";
        $sdiff = $total_workforgotten - $total_standard;
        if ($sdiff > 0) {
            $color = "style='color:red;'";
        } else {
            $color = "style='color:green;'";
        }
        $html .= "<td " . $color . ">" . HTML::timestampToString($sdiff, false, false) . "</td>";
        $html .= "</tr>";
        $html .= "</tbody></table>";

        $html .= "<table id='total'>";
        $html .= "<tbody>";
        $html .= "<tr>";
        $html .= "<td rowspan=2>";

        $html .= "<table id='leave'><thead>";
        $html .= "<tr><th>" . __('Total leaves', 'tam') . "</th><th>" . $total_leves . "</th></tr>";
        $html .= "</thead>";
        $html .= "<tbody>";
        foreach ($leaves as $key => $value) {
            $html .= "<tr>";
            $html .= "<td>" . $value['name'] . "</td>";
            $html .= "<td>" . $value['total'] . "</td>";
            $html .= "</tr>";
        }
        $html .= "</tbody>";
        $html .= "</table>";

        $html .= "</td>";

        $html .= "<td rowspan=2>";

        $html .= "<table id='close_time'><thead>";
        $html .= "<tr><th>" . __('Close times', 'tam') . "</th><th>" . $total_close_time . "</th></tr>";
        $html .= "</thead>";
        $html .= "<tbody>";
        foreach ($close_time as $key => $value) {
            $html .= "<tr>";
            $html .= "<td>" . $value['name'] . "</td>";
            $html .= "<td>" . $value['total'] . "</td>";
            $html .= "</tr>";
        }
        $html .= "</tbody>";
        $html .= "</table>";

        $html .= "</td>";

        $html .= "<td class='total'><div>" . sprintf(__("Forgotten Timers %s", 'tam'), $num_forgotten) . "</div></td>";

        $html .= "<td class='total'><div>" . sprintf(__("Effective working days %s", 'tam'), $num_days) . "</div></td>";

        $html .= "</tr>";
        $html .= "</tbody>";
        $html .= "</table>";

        $totals = PluginTamTam::anualPDFEvents($options);

        //Totales anual
        $html .= "<div class='anual'>";
        $html .= "<span>" . __("Summary") . "</span>";
        $html .= "<table id='sum'><thead>";
        $html .= "<tr><th>" . __("Working days", 'tam') . "</th><th>" . __("Total Hours", 'tam') . "</th><th>" . __("Total Shift", 'tam') . "</th><th>" . __("Total Breaks", 'tam') . "</th><th>" . __("Total Working day", 'tam') . "</th><th>" . __("Total Diff", 'tam') . "</th><th>" . __("Total Standard", 'tam') . "</th><th>" . __("Total Standard Diff", 'tam') . "</th></tr>";
        $html .= "</thead><tbody>";
        $html .= "<tr>";
        $html .= "<td>" . ($totals['total_day'] + $totals['total_leave']) . "</td>";
        $html .= "<td>" . HTML::timestampToString(($totals['total_work'] + $totals['total_break']), false, false) . "</td>";
        $html .= "<td>" . HTML::timestampToString($totals['total_shift'], false, false) . "</td>";
        $html .= "<td>" . HTML::timestampToString($totals['total_break'], false, false) . "</td>";
        $html .= "<td>" . HTML::timestampToString($totals['total_realwork'], false, false) . "</td>";
        $totaldiff = $totals['total_work'] + $totals['total_break'] - $totals['total_realwork'];
        if ($totaldiff > 0) {
            $color = "style='color:red;'";
        } else {
            $color = "style='color:green;'";
        }
        $html .= "<td " . $color . ">" . HTML::timestampToString($totaldiff, false, false) . "</td>";
        $html .= "<td>" . HTML::timestampToString($totals['total_standard'], false, false) . "</td>";
        $totalsdiff = $totals['total_work'] + $totals['total_break'] - $totals['total_standard'];
        if ($totalsdiff > 0) {
            $color = "style='color:red;'";
        } else {
            $color = "style='color:green;'";
        }
        $html .= "<td " . $color . ">" . HTML::timestampToString($totalsdiff, false, false) . "</td>";
        $html .= "</tr>";
        $html .= "</tbody></table>";

        $html .= "<table id='total' class='anual'>";
        $html .= "<tbody>";
        $html .= "<tr>";
        $html .= "<td>";

        $html .= "<table id='leave'><thead>";
        $html .= "<tr><th>" . __('Total leaves', 'tam') . " " . $totals['total_leave'] . "</th></tr>";
        $html .= "</thead>";
        $html .= "</table>";

        $html .= "</td>";

        $html .= "<td>";

        $html .= "<table id='close_time'><thead>";
        $html .= "<tr><th>" . __('Total holidays', 'tam') . " " . $totals['total_holiday'] . " / " . $maxholiday . "</th></tr>";
        $html .= "</thead>";
        $html .= "</table>";

        $html .= "</td>";

        $html .= "<td>";

        $html .= "<table id='close_time'><thead>";
        $html .= "<tr><th>" . __('Close times', 'tam') . " " . $totals['total_close'] . "</th></tr>";
        $html .= "</thead>";
        $html .= "</table>";

        $html .= "</td>";

        $html .= "<td>";

        $html .= "<table id='close_time'><thead>";
        $html .= "<tr><th>" . sprintf(__("Forgotten Timers %s", 'tam'), $totals['total_forgot']) . "</th></tr>";
        $html .= "</thead>";
        $html .= "</table>";

        $html .= "</td>";

        $html .= "<td>";

        $html .= "<table id='close_time'><thead>";
        $html .= "<tr><th>" . sprintf(__("Effective working days %s", 'tam'), $totals['total_day']) . "</th></tr>";
        $html .= "</thead>";
        $html .= "</table>";

        $html .= "</td>";

        $html .= "</tr>";
        $html .= "</tbody>";
        $html .= "</table>";
        $html .= "</div>";

        //Firmas
        $html .= "<table id='signature'>";
        $html .= "<thead>";
        $html .= "<tr>";
        $html .= "<th>" . __('By the company', 'tam') . "</th>";
        $html .= "<th>" . __('By the worker', 'tam') . "</th>";
        $html .= "</tr>";
        $html .= "<tr>";
        $html .= "<td>" . $entity->fields['admin_email_name'] . "</td>";
        $html .= "<td>" . $completename . "</td>";
        $html .= "</tr>";
        $html .= "</thead>";
        $html .= "</table>";

        echo $html;

        $script = <<<JAVASCRIPT

		window.onload=function() {
			document.title ="{$title}";
			window.print();
		};
JAVASCRIPT;
        echo Html::scriptBlock($script);
    }

    public static function isWorkingDay($date, $calendars_id)
    {
        $calendar = new Calendar();
        $calendar->getFromDB($calendars_id);
        $dayweak = $calendar->getDayNumberInWeek(strtotime($date));
        $timeoftheday = CalendarSegment::getActiveTimeBetween($calendars_id, $dayweak, '00:00:00', '23:59:59');
        if ($timeoftheday > 0 && !$calendar->isHoliday($date)) {
            return true;
        } else {
            return false;
        }
    }

    public static function timeofDay($date, $calendars_id)
    {
        $calendar = new Calendar();
        $calendar->getFromDB($calendars_id);
        $dayweak = $calendar->getDayNumberInWeek(strtotime($date));
        $timeoftheday = CalendarSegment::getActiveTimeBetween($calendars_id, $dayweak, '00:00:00', '23:59:59');
        return $timeoftheday;
    }

    public static function standardTime($calendar_id)
    {
        global $DB;

        $query = [
            'FROM' => 'glpi_plugin_tam_calendars',
            'WHERE' => [
                'calendars_id' => $calendar_id
            ],
        ];
        $req = $DB->request($query);
        if ($row = $req->current()) {
            return $row['standard_hour'];
        } else {
            return 0;
        }
    }
}
