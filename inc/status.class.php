<?php
/*
 -------------------------------------------------------------------------
 TAM plugin for GLPI
 Copyright (C) 2021-2023 by the TICgal Team.
 https://www.tic.gal
 -------------------------------------------------------------------------
 LICENSE
 This file is part of the TAM plugin.
 TAM plugin is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.
 TAM plugin is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with TAM. If not, see <http://www.gnu.org/licenses/>.
 --------------------------------------------------------------------------
 @package   TAM
 @author    the TICgal team
 @copyright Copyright (c) 2021-2023 TICgal team
 @license   AGPL License 3.0 or (at your option) any later version
            http://www.gnu.org/licenses/agpl-3.0-standalone.html
 @link      https://www.tic.gal
 @since     2021
 ----------------------------------------------------------------------
*/

if (!defined('GLPI_ROOT')) {
    die("Sorry. You can't access directly to this file");
}

class PluginTamStatus
{
    public const CORRECT = 0;
    public const FORGOTTEN = 1;
    public const FIXED = 2;


    public static function getStatusName($n, $print = false)
    {
        $status = '';
        switch ($n) {
            case 0:
                $status = __('Ok', 'tam');
                break;
            case 1:
                $status = __('Forgotten', 'tam');
                break;
            case 2:
                $status = __('Fixed', 'tam');
                break;
            default:
                break;
        }

        if ($print) {
            echo $status;
        }

        return $status;
    }

    public static function getStatusIcon($n, $print = false)
    {
        $class = '';
        switch ($n) {
            case 0:
                $class = 'fa fa-circle-check';
                break;
            case 1:
                $class = 'fa fa-circle-question';
                break;
            case 2:
                $class = 'fa fa-circle-dot';
                break;
            default:
                break;
        }

        $icon = '<i class="mx-2 '.$class.'"></i>';

        if ($print) {
            echo $icon;
        }

        return $icon;
    }
}
