<?php
/*
 -------------------------------------------------------------------------
 TAM plugin for GLPI
 Copyright (C) 2021-2023 by the TICgal Team.
 https://www.tic.gal
 -------------------------------------------------------------------------
 LICENSE
 This file is part of the TAM plugin.
 TAM plugin is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.
 TAM plugin is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with TAM. If not, see <http://www.gnu.org/licenses/>.
 --------------------------------------------------------------------------
 @package   TAM
 @author    the TICgal team
 @copyright Copyright (c) 2021-2023 TICgal team
 @license   AGPL License 3.0 or (at your option) any later version
            http://www.gnu.org/licenses/agpl-3.0-standalone.html
 @link      https://www.tic.gal
 @since     2021
 ----------------------------------------------------------------------
*/

class PluginTamAdmin extends CommonDBTM
{
    public static $rightname = 'plugin_tam_all_timers';

    public static function getTypeName($nb = 0)
    {
        return __("TAM", "tam");
    }

    public function defineTabs($options = [])
    {
        $ong = [];

        $this->addDefaultFormTab($ong);

        return $ong;
    }

    public function canSee()
    {
        return Session::haveRight('plugin_tam_all_timers', 1);
    }

    public static function show($options = [])
    {
        $uid = 0;
        if (isset($options['users_id'])) {
            $uid = $options['users_id'];
        }
        $date = date("m/01/y");
        if (isset($options['date_start']) && $options['date_start'] != "") {
            $date = $options['date_start'];
        }

        echo "<div class='col search-container'>";
        echo "<form method='post' class='search-form-container' action='" . PluginTamAdmin::getFormURL() . "'>";
        echo "<div class='search-form card card-sm mb-4'>";
        echo "<table class='tab_cadre_fixe'>";
        echo "<tr class='tab_bg_1'>";
        echo "<td class='tab_bg_2 right'>" . __('User') . "</td>";
        echo "<td>";
        User::dropdown([
            'value'        => $uid,
            'right'        => 'all'
        ]);
        echo "</td>";
        echo "<td class='tab_bg_2 right'>" . __('Date') . "</td>";
        echo "<td><div class='input-group flex-grow-1 flatpickr d-flex align-items-center' id='showdate'><input type='text' name='date_start' value='" . $date . "' data-input><a class='input-button' data-toggle><i class='input-group-text far fa-calendar-alt fa-lg pointer'></i></a><a data-clear  title='" . __s('Clear') . "'><i class='input-group-text fas fa-times-circle pointer'></i></a></div>";
        echo "</td>";

        $script = <<<JAVASCRIPT
	$(function(){
		$('#showdate').flatpickr({
			altInput: true,
			altFormat: 'F Y',
			enableTime: false,
			dateFormat: 'm/01/y',
			wrap: true,
		});
	});

JAVASCRIPT;
        echo Html::scriptBlock($script);
        //echo "</tr><tr>";
        echo "<td class='center' colspan='4'>";
        echo Html::submit(__('Search'), ['name' => 'search', 'class' => 'btn btn-primary my-1', 'icon' => 'ti ti-list-search']);
        echo "</td>";
        echo "</tr>";
        echo "</table>";
        echo "</div>";
        Html::closeForm();
        echo "</div>";

        $res = PluginTamAdmin::search($options);

        echo "<div class='card center'>";
        echo "<table class='table card-table table-hover align-middle'>";
        echo "<thead><tr>";
        echo "<th class='border'></th>";
        echo "<th class='center border' colspan='6'>" . __("Month") . "</th>";
        echo "<th class='center border' colspan='6'>" . __("Summary") . "</th>";
        echo "</tr><tr>";
        echo "<th>" . __('User') . "</a></th>";
        echo "<th>" . sprintf(__("Effective working days %s", 'tam'), '') . "</th>";
        echo "<th>" . __("Hours") . "</th>";
        echo "<th>" . __("Working day", 'tam') . "</th>";
        echo "<th>" . __('Diff', 'tam') . "</th>";
        echo "<th>" . __('Standard Working time', 'tam') . "</th>";
        echo "<th>" . __('Standard Working Diff', 'tam') . "</th>";

        echo "<th>" . sprintf(__("Effective working days %s", 'tam'), '') . "</th>";
        echo "<th>" . __("Total Hours", 'tam') . "</th>";
        echo "<th>" . __("Total Working day", 'tam') . "</th>";
        echo "<th>" . __('Total Diff', 'tam') . "</th>";
        echo "<th>" . __('Total Standard', 'tam') . "</th>";
        echo "<th>" . __('Total Standard Diff', 'tam') . "</th>";
        echo "</tr></thead>";
        echo "<tbody>";
        foreach ($res as $key => $value) {
            echo "<tr>";
            echo "<td><a href='" . PluginTamAdmin::getFormURL() . "?user=" . $key . "&date=" . $date . "'>" . getUserName($key) . "</td>";
            $month = $value['m'];
            if (!array_key_exists('total_work', $month)) {
                $month['total_work'] = 0;
            }
            echo "<td>" . $month['total_day'] . "</td>";
            echo "<td>" . HTML::timestampToString(($month['total_work'] + $month['total_break']), false, false) . "</td>";
            echo "<td>" . HTML::timestampToString($month['total_realwork'], false, false) . "</td>";
            echo "<td>" . HTML::timestampToString(($month['total_work'] + $month['total_break'] - $month['total_realwork']), false, false) . "</td>";
            echo "<td>" . HTML::timestampToString($month['total_standard'], false, false) . "</td>";
            echo "<td>" . HTML::timestampToString(($month['total_work'] + $month['total_break'] - $month['total_standard']), false, false) . "</td>";
            $annual = $value['a'];
            echo "<td>" . $annual['total_day'] . "</td>";
            echo "<td>" . HTML::timestampToString(($annual['total_work'] + $annual['total_break']), false, false) . "</td>";
            echo "<td>" . HTML::timestampToString($annual['total_realwork'], false, false) . "</td>";
            echo "<td>" . HTML::timestampToString(($annual['total_work'] + $annual['total_break'] - $annual['total_realwork']), false, false) . "</td>";
            echo "<td>" . HTML::timestampToString($annual['total_standard'], false, false) . "</td>";
            echo "<td>" . HTML::timestampToString(($annual['total_work'] + $annual['total_break'] - $annual['total_standard']), false, false) . "</td>";
            echo "</tr>";
        }
        echo "</tbody>";
        echo "</table>";
        echo "</div>";
    }

    public static function showPlanning($ID, $date)
    {
        global $CFG_GLPI;

        $rand = mt_rand();

        echo PluginTamTam::createModal();

        echo "<div class='card center mb-3'>";
        echo "<h1 class='m-1'>" . getUserName($ID) . "</h1>";
        echo "</div>";

        echo Html::css('public/lib/fullcalendar.css', ['media' => '']);
        echo Html::css('/lib/jqueryplugins/fullcalendar/fullcalendar.print.css', ['media' => 'print']);
        Html::requireJs('fullcalendar');
        foreach ($_SESSION['glpi_js_toload']['fullcalendar'] as $s) {
            echo Html::script($s);
        }

        $text_worked = __('Working time', 'tam');
        $help_text = __('Help');
        $print = __('Print', 'tam');
        $actual_date = date("Y-m-d H:i:s");
        echo "<div id='planning$rand' class='card p-4'></div>";

        $date_format = Toolbox::jsDateFormat();
        $pl_height = "function() {
			var _newheight = $(window).height() - 272;
			if ($('#debugajax').length > 0) {
				_newheight -= $('#debugajax').height();
			}
			//minimal size
			var _minheight = 300;
			if (_newheight < _minheight) {
				_newheight = _minheight;
			}
			return _newheight;
		}";
        if ($_SESSION['glpipage_layout'] == "vsplit") {
            $pl_height = "function() {
				var _newheight = $('.ui-tabs-panel').height() - 30;
				//minimal size
				var _minheight = 300;
				if (_newheight < _minheight) {
					_newheight = _minheight;
				}
				return _newheight;
			}";
        }

        $header = "{
			left:   'prev,next,today',
			center: 'title',
			right:  'dayGridMonth,timeGridWeek,timeGridDay,listYear'
		}";

        $url_dir = Plugin::getWebDir('tam');

        $script = <<<JAVASCRIPT

		$('#planning$rand').parent().append("<div id='div_print' class='center' style='margin-top:15px;'></div>");

		function formatDate(today){
			var dd = today.getDate();
			var mm = today.getMonth() + 1; //January is 0!

			var yyyy = today.getFullYear();
			if (dd < 10) {
			  dd = '0' + dd;
			} 
			if (mm < 10) {
			  mm = '0' + mm;
			} 
			return mm+'/'+dd+'/'+yyyy;
		}

		$(function() {
			var disable_qtip = false,
			disable_edit = false;
			$('.planning_on_central a').mousedown(function() {
				disable_qtip = true;
				$('.qtip').hide();
			}).mouseup(function() {
				disable_qtip = false;
			});

			var window_focused = true;
			var loaded = false;
			var lastView;
			var lastDate;
			var lastDateDirty = false;
			window.onblur = function() { window_focused = false; }
			window.onfocus = function() { window_focused = true; }

			var initFCDatePicker = function(currentDate) {
				$('#planning_datepicker').flatpickr({
					defaultDate: currentDate,
					onChange: function(selected_date) {
						// convert to UTC to avoid timezone issues
						var date = new Date(
							Date.UTC(
								selected_date[0].getFullYear(),
								selected_date[0].getMonth(),
								selected_date[0].getDate()
							)
						);
						calendar.gotoDate(date);
					}
				});
			}

			var calendar=new FullCalendar.Calendar(document.getElementById('planning$rand'),{
				plugins: ['dayGrid', 'interaction', 'list', 'timeGrid', 'resourceTimeline', 'rrule'],
				height: {$pl_height},
				timeZone: 'UTC',
				theme: true,
				weekNumbers: false,
				defaultView: 'dayGridMonth',
				timeFormat:  'H:mm',
				eventLimit:  true,
				minTime:     '{$CFG_GLPI["planning_begin"]}',
				maxTime:     '{$CFG_GLPI["planning_end"]}',
				nowIndicator: true,
				now: "{$actual_date}",
				listDayAltFormat: false,
				agendaEventMinHeight: 13,
				header: {$header},
				views: {
					dayGridMonth: {
						titleFormat: {
							year:'numeric',
							month:'short',
							day:'numeric'
						},
						eventLimit:3,
						allDaySlot: false
					},
					timeGridWeek: {
						titleFormat: {
							year:'numeric',
							month:'short',
							day:'numeric'
						},
					},
					timeGridDay: {
						titleFormat: {
							year:'numeric',
							month:'short',
							day:'numeric'
						},
					},
					listYear:{
						titleFormat: {
							year:'numeric',
							month:'short',
							day:'numeric'
						},
					},
				},
				datesRender: function(info) {
					var view = info.view;
					// force refetch events from ajax on view change (don't refetch on firt load)
					if (loaded) {
						calendar.refetchEvents();
						calendar.rerenderEvents();
					}else{
						loaded=true;
					}
					$('#planning$rand .fc-toolbar .fc-center h2').after($('<i style=\"cursor: help;\" title=\"{$help_text}\" class=\"fa fa-question\" id=\"help_tam_{$rand}\"></i>')).after($('<i id=\"refresh_planning\" class=\"fa fa-sync pointer\"></i>')).after($('<div id=\"planning_datepicker\"><a data-toggle><i class=\"far fa-calendar-alt fa-lg pointer\"></i></a>'));
					// reinit datepicker
					$('#planning_datepicker').show();
					initFCDatePicker(new Date(view.currentStart));

					// show controls buttons
					$('#planning .fc-left .fc-button-group').show();

					var start=formatDate(new Date(view.currentStart));
					var end =formatDate(new Date(view.currentEnd));
					var button="<a href='{$url_dir}/front/pdf.form.php?id={$ID}&start="+start+"&end="+end+"' target='_blank' type='button' class='vsubmit'><i class=\'me-2 fas fa-file-pdf\'></i> {$print}</a>";
					$('#div_print').html(button);
				},
				eventRender: function(info) {
					var event=info.event;
					var extProps = event.extendedProps;
					var element=$(info.el);
					var view =info.view;

					var eventtype_marker = '<span class=\"event_type\" style=\"background-color: '+extProps.typeColor+'\"></span>';
					element.find('.fc-content').after(eventtype_marker);
					element.find('.fc-list-item-title > a').prepend(eventtype_marker);

					var content = extProps.content;
					var tooltip = extProps.tooltip;
					if(view.type !== 'dayGridMonth' && view.type.indexOf('list') < 0 && !event.allDay){
							element.append('<div class=\"content\">'+content+'</div>');
					}

					// add classes to current event
					added_classes = '';
					if (typeof event.end !== 'undefined' && event.end !== null) {
						var now = new Date();
						var end = event.end;
						added_classes = end.getTime() < now.getTime() ? ' event_past'   : '';
						added_classes+= end.getTime() > now.getTime() ? ' event_future' : '';
						added_classes+= end.toDateString() === now.toDateString() ? ' event_today' : '';
					}
					if (extProps.state != '') {
						added_classes+= extProps.state == 0 ? ' event_info'
							: extProps.state == 1 ? ' event_todo'
							: extProps.state == 2 ? ' event_done'
							: '';
					}
					if (added_classes != '') {
						element.addClass(added_classes);
					}

					// add tooltip to event
					if (!disable_qtip) {
						var qtip_position = {
							viewport: 'auto'
						};
						if (view.type.indexOf('list') >= 0) {
							qtip_position.target= element.find('a');
						}
						element.qtip({
							position: qtip_position,
							content: tooltip,
							style: {
								classes: 'qtip-shadow qtip-bootstrap'
							},
							show: {
								solo: true,
								delay: 400
							},
							hide: {
								fixed: true,
								delay: 100
							},
							events: {
								show: function(event, api) {
									if(!window_focused) {
										event.preventDefault();
									}
								}
							}
						});
					}

					if (event.allDay && !event.extendedProps.diff) {
						var div=element.find('.fc-list-item-title > a').parent();
						var content="<div>Working day : "+event.extendedProps.active_time+"</div>";
						var divdialog="<div id='realtime'>"+content+"</div>";
						$(divdialog).val(content);

						div.append(divdialog);
					}
				},
				viewSkeletonRender: function(info) {
					var view=info.view;
					var start=formatDate(new Date(view.currentStart));
					var end =formatDate(new Date(view.currentEnd));
					var button="<a href='{$url_dir}/front/pdf.form.php?id={$ID}&start="+start+"&end="+end+"' target='_blank' type='button' class='vsubmit'><i class=\'fas fa-file-pdf\'></i> {$print}</a>";
					$('#div_print').html(button);

					// set a var to force refetch events (see viewRender callback)
					loaded = true;

					// scroll div to first element needed to be viewed
					var scrolltoevent = $('#planning$rand .event_past.event_todo').first();
					if (scrolltoevent.length == 0) {
						scrolltoevent = $('#planning$rand .event_today').first();
					}
					if (scrolltoevent.length == 0) {
						scrolltoevent = $('#planning$rand .event_future').first();
					}
					if (scrolltoevent.length == 0) {
						scrolltoevent = $('#planning$rand .event_past').last();
					}
					if (scrolltoevent.length) {
						$('#planning$rand .fc-scroller').scrollTop(scrolltoevent.prop('offsetTop')-25);
					}
				},
				events:{
					url:  '{$url_dir}/ajax/planning.php',
					type: 'POST',
					extraParams: function() {
						var view_name = calendar ? calendar.state.viewType: 'dayGridMonth';
						var display_done_events = 1;
						if (view_name.indexOf('list') >= 0) {
							display_done_events = 0;
						}
						return {
							'action': 'get_events',
							'display_done_events': display_done_events,
							'view_name': view_name,
							'user':'{$ID}'
						};
					},
					success: function(data) {
						
					},
					failure: function(error) {
						console.log('there was an error while fetching events!',error);
					}
				}
			});

			var loadedLocales = Object.keys(FullCalendarLocales);
			if (loadedLocales.length === 1) {
				calendar.setOption('locale', loadedLocales[0]);
			}
			calendar.render();

			$('#refresh_planning').click(function() {
				calendar.refetchEvents();
				calendar.rerenderEvents();
			});

			initFCDatePicker();

			$("#help_tam_{$rand}").click(function(event) {
				$('#modal_help').remove().modal("show");
			});
		});
JAVASCRIPT;

        echo Html::scriptBlock($script);
    }

    public static function search($options)
    {
        global $DB;

        $datas = [];
        $date_start = date("Y-m-01 00:00:00");
        if (isset($options['date_start']) && $options['date_start'] != "") {
            $date_start = date("Y-m-01 00:00:00", strtotime($options['date_start']));
        }
        $date_end = date("Y-m-t 23:59:59", strtotime($date_start));

        $sqltotal_work = [
            'SELECT' => [
                'users_id',
                'SUM' => [
                    'working_time AS total_work'
                ]
            ],
            'FROM' => 'glpi_plugin_tam_tams',
            'INNER JOIN' => [
                'glpi_plugin_tam_days' => [
                    'FKEY' => [
                        'glpi_plugin_tam_tams' => 'plugin_tam_days_id',
                        'glpi_plugin_tam_days' => 'id'
                    ]
                ]
            ],
            'WHERE' => [
                'date_start' => ['>=', $date_start],
                'date_end' => ['<=', $date_end],
            ],
        ];
        if (isset($options['users_id']) && $options['users_id'] > 0) {
            $sqltotal_work['WHERE'][] = ["users_id" => $options['users_id']];
        } else {
            $sqltotal_work['GROUPBY'] = 'users_id';
        }

        foreach ($DB->request($sqltotal_work) as $id => $row) {
            $datas[$row['users_id']]['m']['total_work'] = $row['total_work'];

            $sqlstandard = [
                'SELECT' => [
                    'SUM' => [
                        'active_time AS total_realwork',
                        'break AS total_break',
                        'standard_hour AS total_standard',
                    ],
                    'COUNT' => [
                        'date AS total_day',
                    ],
                ],
                'FROM' => 'glpi_plugin_tam_days',
                'LEFT JOIN' => [
                    'glpi_plugin_tam_calendars' => [
                        'FKEY' => [
                            'glpi_plugin_tam_days' => 'plugin_tam_calendar_id',
                            'glpi_plugin_tam_calendars' => 'id'
                        ]
                    ],
                ],
                'WHERE' => [
                    'date' => ['>=', $date_start],
                    'AND' => [
                        'date' => ['<=', $date_end],
                    ],
                    'users_id' => $row['users_id']
                ]
            ];
            $req = $DB->request($sqlstandard);
            if ($stan = $req->current()) {
                $datas[$row['users_id']]['m']['total_standard'] = $stan['total_standard'];
                $datas[$row['users_id']]['m']['total_realwork'] = $stan['total_realwork'];
                $datas[$row['users_id']]['m']['total_break'] = $stan['total_break'];
                $datas[$row['users_id']]['m']['total_day'] = $stan['total_day'];
            } else {
                $datas[$row['users_id']]['m']['total_standard'] = 0;
                $datas[$row['users_id']]['m']['total_realwork'] = 0;
                $datas[$row['users_id']]['m']['total_break'] = 0;
                $datas[$row['users_id']]['m']['total_day'] = 0;
            }
        }

        //Anual
        $users = [];
        foreach (array_keys($datas) as $key => $value) {
            $users[] = $value;
            $datas[$value]['a']['total_work'] = 0;
            $datas[$value]['a']['total_standard'] = 0;
            $datas[$value]['a']['total_realwork'] = 0;
            $datas[$value]['a']['total_break'] = 0;
            $datas[$value]['a']['total_day'] = 0;
        }

        if (count($users)) {
            $begin = date("Y-01-01 00:00:00");
            $end = date("Y-m-t 23:59:59");
            if (isset($options['date_start']) && $options['date_start'] != "") {
                $begin = date("Y-01-01 00:00:00", strtotime($options['date_start']));
                $end = date("Y-m-t 23:59:59", strtotime($options['date_start']));
            }
            $sqltotal_work = [
                'SELECT' => [
                    'users_id',
                    'SUM' => [
                        'working_time AS total_work'
                    ]
                ],
                'FROM' => 'glpi_plugin_tam_tams',
                'INNER JOIN' => [
                    'glpi_plugin_tam_days' => [
                        'FKEY' => [
                            'glpi_plugin_tam_tams' => 'plugin_tam_days_id',
                            'glpi_plugin_tam_days' => 'id'
                        ]
                    ]
                ],
                'WHERE' => [
                    'date_start' => ['>=', $begin],
                    'date_end' => ['<=', $end],
                    'users_id' => $users
                ],
                'GROUPBY' => 'users_id'
            ];
            foreach ($DB->request($sqltotal_work) as $id => $row) {
                $datas[$row['users_id']]['a']['total_work'] = $row['total_work'];
            }

            $sqlstandard = [
                'SELECT' => [
                    'users_id',
                    'SUM' => [
                        'active_time AS total_realwork',
                        'break AS total_break',
                        'standard_hour AS total_standard',
                    ],
                    'COUNT' => [
                        'date AS total_day',
                    ],
                ],
                'FROM' => 'glpi_plugin_tam_days',
                'LEFT JOIN' => [
                    'glpi_plugin_tam_calendars' => [
                        'FKEY' => [
                            'glpi_plugin_tam_days' => 'plugin_tam_calendar_id',
                            'glpi_plugin_tam_calendars' => 'id'
                        ]
                    ],
                ],
                'WHERE' => [
                    'date' => ['>=', $begin],
                    'AND' => [
                        'date' => ['<=', $end],
                    ],
                    'users_id' => $users
                ],
                'GROUPBY' => 'users_id'
            ];
            foreach ($DB->request($sqlstandard) as $id => $row) {
                $datas[$row['users_id']]['a']['total_standard'] = $row['total_standard'];
                $datas[$row['users_id']]['a']['total_realwork'] = $row['total_realwork'];
                $datas[$row['users_id']]['a']['total_break'] = $row['total_break'];
                $datas[$row['users_id']]['a']['total_day'] = $row['total_day'];
            }
        }

        return $datas;
    }
}
