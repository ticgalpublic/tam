<?php
/*
 -------------------------------------------------------------------------
 TAM plugin for GLPI
 Copyright (C) 2021-2023 by the TICgal Team.
 https://www.tic.gal
 -------------------------------------------------------------------------
 LICENSE
 This file is part of the TAM plugin.
 TAM plugin is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.
 TAM plugin is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with TAM. If not, see <http://www.gnu.org/licenses/>.
 --------------------------------------------------------------------------
 @package   TAM
 @author    the TICgal team
 @copyright Copyright (c) 2021-2023 TICgal team
 @license   AGPL License 3.0 or (at your option) any later version
            http://www.gnu.org/licenses/agpl-3.0-standalone.html
 @link      https://www.tic.gal
 @since     2021
 ----------------------------------------------------------------------
*/
use Glpi\Toolbox\Sanitizer;

class PluginTamForgot extends CommonGLPI
{
    public static $rightname = 'plugin_tam_all_timers';

    public static function getTypeName($nb = 0)
    {
        return __("TAM", "tam");
    }

    public function defineTabs($options = [])
    {
        $ong = [];
        $this->addStandardTab(__CLASS__, $ong, $options);
        $ong['no_all_tab'] = true;

        return $ong;
    }

    public function getTabNameForItem(CommonGLPI $item, $withtemplate = 0)
    {
        if ($item->getType() == __CLASS__) {
            $tabs[1] = __('Forgotten timer', 'tam');
            $tabs[2] = __("Log");
            return $tabs;
        }
        return "";
    }

    public static function displayTabContentForItem(CommonGLPI $item, $tabnum = 1, $withtemplate = 0)
    {
        if ($item->getType() == __CLASS__) {
            switch ($tabnum) {
                case 1:
                    $item->show();
                    break;
                case 2:
                    $item->showLog();
                    break;
            }
        }
        return true;
    }

    public function showLog()
    {
        global $DB;

        if (isset($_GET["start"])) {
            $start = intval($_GET["start"]);
        } else {
            $start = 0;
        }

        $total_number    = countElementsInTable("glpi_plugin_tam_forgots", ['NOT' => ['last_mod' => null]]);
        Html::printPager($start, $total_number, '', '');

        // Output events
        echo "<div class='center'>";
        echo "<table class='table card-table border align-middle'>";

        $header = "<tr>";
        $header .= "<th>" . __('ID') . "</th>";
        $header .= "<th>" . __('User') . "</th>";
        $header .= "<th>" . __('Day') . "</th>";
        $header .= "<th>" . __('Date end', 'tam') . "</th>";
        $header .= "<th>" . __('Comment') . "</th>";
        $header .= "<th>" . __('Last update') . "</th>";
        $header .= "<th>" . __('By user') . "</th>";

        $header .= "</tr>";

        echo "<thead>";
        echo $header;
        echo "</thead>";

        //echo "<tfoot>$header</tfoot>";

        echo "<tbody>";

        foreach (self::getHistoryData($start, $_SESSION['glpilist_limit']) as $data) {
            // show line
            echo "<tr class='tab_bg_2'>";
            echo "<td>" . $data['id'] . "</td>";
            echo "<td>" . getUserName($data['users_id']) . "</td>";
            echo "<td class='tab_date'>" . $data['date'] . "</td>";
            echo "<td class='tab_date'>" . $data['date_end'] . "</td>";
            echo "<td>" . $data['comment'] . "</td>";
            echo "<td class='tab_date'>" . $data['date_mod'] . "</td>";
            echo "<td>" . getUserName($data['last_mod']) . "</td>";
            echo "</tr>";
        }
        echo "</tbody>";

        echo "</table>";
        echo "</div>";
        Html::printPager($start, $total_number, '', '');
    }

    public static function show()
    {
        global $CFG_GLPI;

        echo "<div class='center p-2'>";
        echo "<table class='table card-table table-hover border align-middle'>";

        $header = "<tr>";
        $header .= "<th>" . __('ID') . "</th>";
        $header .= "<th>" . __('User') . "</th>";
        $header .= "<th>" . __('Date start', 'tam') . "</th>";
        $header .= "<th>" . __('IP start', 'tam') . "</th>";
        $header .= "<th>" . __('Origin start', 'tam') . "</th>";
        //$header .= "<th>" . __('Date end', 'tam') . "</th>";
        //$header .= "<th>" . __('IP end', 'tam') . "</th>";
        //$header .= "<th>" . __('Origin end', 'tam') . "</th>";
        $header .= "<th></th>";

        $header .= "</tr>";

        echo "<thead>";
        echo $header;
        echo "</thead>";
        echo "<tbody>";
        $origin = PluginTamTam::getOrigin();

        $id = '';
        if (isset($_GET['id']) && $_GET['id'] > 0) {
            $id = $_GET['id'];
        }

        foreach (PluginTamForgot::getForgottenTimer($id) as $data) {
            echo "<tr class='tab_bg_2 clickable-row' data-href=\"" . Plugin::getWebDir('tam') . "/front/forgot.form.php?id=" . $data['forgot_id'] . "\">";
            echo "<td><a href=\"" . Plugin::getWebDir('tam') . "/front/forgot.form.php?id=" . $data['forgot_id'] . "&tam=" . $data['id'] . "\">" . $data['id'] . "</a></td>";
            echo "<td>" . getUserName($data['user_name']) . "</td>";
            echo "<td class='tab_date'>" . $data['date_start'] . "</td>";
            echo "<td>" . $data['ip_start'] . "</td>";
            echo "<td>";
            Dropdown::showFromArray('origin_start', $origin, ['value' => $data['origin_start'], 'readonly' => true, 'class' => 'w-auto']);
            echo "</td>";
            //echo "<td class='tab_date'>" . $data['date_end'] . "</td>";
            //echo "<td>" . $data['ip_end'] . "</td>";
            //echo "<td>";
            //Dropdown::showFromArray('origin_end', $origin, ['value' => $data['origin_end'], 'readonly' => true]);
            //echo "</td>";

            $check = PluginTamManagement::checkForgottenTam($data['id']);
            if ($check['count'] > 0) {
                echo "<td><span>" . __('Fixed', 'tam') . "</span></td>";
            } else {
                echo "<td><a class='btn btn-warning' href=\"" . Plugin::getWebDir('tam') . "/front/forgot.form.php?id=" . $data['forgot_id'] . "&tam=" . $data['id'] . "\">" . __('Fix TAM', 'tam') . "</a></td>";
            }

            echo "</tr>";
        }
        echo "</tbody>";

        echo "</table>";
        echo "</div>";
    }

    public function showForm($ID, $options = [])
    {
        global $CFG_GLPI, $DB;

        $rand = mt_rand();
        $tam = new PluginTamTam();
        $tam->getFromDB($options['tam']);
        $origin = PluginTamTam::getOrigin();

        $date = date("Y-m-d", strtotime($tam->fields['date_start']));

        echo "<div class='card px-2 p-2'>";
        echo "<h2>".__('Forgotten TAM', 'tam')."</h2>";
        echo "<hr class='m-0 mb-3'>";
        echo "<div class='center'>";
        echo "<table class='table card-table border align-middle'>";
        echo "<thead>";
        echo "<tr>";
        echo "<th>" . __('ID') . "</th>";
        echo "<th>" . __('Date start', 'tam') . "</th>";
        echo "<th>" . __('IP start', 'tam') . "</th>";
        echo "<th>" . __('Origin start', 'tam') . "</th>";
        echo "<th>" . __('Date end', 'tam') . "</th>";
        echo "<th>" . __('IP end', 'tam') . "</th>";
        echo "<th>" . __('Origin end', 'tam') . "</th>";
        echo "</tr>";
        echo "</thead>";
        echo "<tbody>";
        echo "<tr class='tab_bg_2'>";
        echo "<td>" . $ID . "</td>";
        echo "<td class='tab_date'>" . $tam->fields['date_start'] . "</td>";
        echo "<td>" . $tam->fields['ip_start'] . "</td>";
        echo "<td>";
        Dropdown::showFromArray('origin_start', $origin, ['value' => $tam->fields['origin_start'], 'readonly' => true]);
        echo "</td>";
        echo "<td class='tab_date'>" . $tam->fields['date_end'] . "</td>";
        echo "<td>" . $tam->fields['ip_end'] . "</td>";
        echo "<td>";
        Dropdown::showFromArray('origin_end', $origin, ['value' => $tam->fields['origin_end'], 'readonly' => true]);
        echo "</td>";
        echo "</tr>";
        echo "</tbody>";
        echo "</table>";
        echo "</div>";

        echo "<form name='form' method='post' action='" . $this->getFormURL() . "' enctype=\"multipart/form-data\">";
        echo "<div class='spaced mt-3' id='tabsbody'>";
        echo "<table class='tab_cadre_fixe' id='mainformtable'>";

        echo "<tr class='tab_bg_2'>";
        echo "<input type='hidden' name='users_id' value='" . Session::getLoginUserID() . "'>";
        echo "<input type='hidden' name='id' value='" . $ID . "'>";
        echo "<input type='hidden' name='tam_id' value='" . $options['tam'] . "'>";
        echo "<td>" . __("New end of working day", 'tam') . "</td>";
        echo "<td>";

        $warning = __("The selected date is different from the start date", "tam");
        $script = <<<JAVASCRIPT
$(document).ready(function() {
	check=function(selectedDates,dateStr,instance){
			var picker=new Date(dateStr);
			var b =new Date("{$date}");
			if (picker.getFullYear()!=b.getFullYear() || picker.getMonth()!=b.getMonth() || picker.getDate()!=b.getDate()) {
				$("#warning{$rand}").html("{$warning}");
			}else{
				$("#warning{$rand}").html("");
			}
	};
});

JAVASCRIPT;

        echo Html::scriptBlock($script);

        Html::showDateTimeField("date", [
            'value' 		=> $tam->fields['date_start'],
            'mindate' 		=> $tam->fields['date_start'],
            'maxdate' 		=> strtotime("+1 day", strtotime($tam->fields['date_start'])),
            'timestep'   	=> 1,
            'maybeempty' 	=> false,
            'rand' 			=> $rand,
            'required' 		=> true,
            'on_change' 	=> 'check(selectedDates,dateStr,instance)'
        ]);
        echo "</td>";
        echo "</tr>";

        echo "<tr><td colspan=2><div id='warning$rand' style='color:red'></div></td></tr>";

        echo "<tr class='tab_bg_2'>";
        echo "<td><label>" . __("Reason", 'tam') . "</label></td>";
        echo "<td>";
        PluginTamReason::dropdown(['name' => 'reason_id']);
        echo "</td>";
        echo "</tr>";

        echo "<tr class='tab_bg_2'>";
        echo "<td><label>" . __("Motive", 'tam') . "</label></td>";
        echo "<td>";
        Html::textarea([
            'name'              => 'comment',
            'rows'				=> 5,
            'enable_richtext'   => false,
            'enable_fileupload' => false,
            'rand'              => $rand,
            'editor_id'         => 'comment' . $rand,
            'required'          => true
        ]);
        echo "</td>";
        echo "</tr>";

        echo "<td class='center' colspan='2'>\n";
        echo Html::submit(_x('button', 'Save'), ['name' => 'update']);

        echo "</table></div>";
        Html::closeForm();
        echo "</div>";

        return true;
    }

    public static function getForgottenTimer($id = '')
    {
        global $DB;

        $where = [
            'last_mod' => null
        ];

        if ($id && $id > 0) {
            $where = [
                'last_mod' => null,
                'glpi_plugin_tam_tams.id' => $id
            ];
        }

        $query = [
            'SELECT' => [
                'glpi_plugin_tam_tams.*',
                'glpi_plugin_tam_forgots.id as forgot_id',
                'users_id',
            ],
            'FROM' => 'glpi_plugin_tam_tams',
            'INNER JOIN' => [
                'glpi_plugin_tam_forgots' => [
                    'FKEY' => [
                        'glpi_plugin_tam_tams' => 'id',
                        'glpi_plugin_tam_forgots' => 'plugin_tam_tams_id',
                    ]
                ],
                'glpi_plugin_tam_days' => [
                    'FKEY' => [
                        'glpi_plugin_tam_tams' => 'plugin_tam_days_id',
                        'glpi_plugin_tam_days' => 'id'
                    ]
                ]
            ],
            'WHERE' => $where
        ];

        $iterator = $DB->request($query);

        $values = [];
        foreach ($iterator as $data) {
            $tmp = [];
            $tmp['forgot_id'] = $data['forgot_id'];
            $tmp['id'] = $data['id'];
            $tmp['user_name'] = $data['users_id'];
            $tmp['date_start'] = $data['date_start'];
            $tmp['ip_start'] = $data['ip_start'];
            $tmp['origin_start'] = $data['origin_start'];
            $tmp['date_end'] = $data['date_end'];
            $tmp['ip_end'] = $data['ip_end'];
            $tmp['origin_end'] = $data['origin_end'];
            $values[] = $tmp;
        }
        return $values;
    }

    public static function updateForgot(array $input)
    {
        global $DB;

        $tam = new PluginTamTam();
        $tam->getFromDB($input['tam_id']);

        $actiontime = strtotime($input['date']) - strtotime($tam->fields['date_start']);
        $ip = getenv("HTTP_X_FORWARDED_FOR") ? Sanitizer::encodeHtmlSpecialChars(getenv("HTTP_X_FORWARDED_FOR")) : getenv("REMOTE_ADDR");
        $reason = '';
        $owt = 0;
        if (isset($input['reason_id'])) {
            $reason_item = new PluginTamReason();
            $reason_item->getFromDB($input['reason_id']);
            if (isset($reason_item->fields['code']) && isset($reason_item->fields['name'])) {
                $reason = $reason_item->fields['code'].'-'.$reason_item->fields['name'];
            }
            if (isset($reason_item->fields['on_working_time'])) {
                $owt = $reason_item->fields['on_working_time'];
            }
        }

        $table = PluginTamTam::getTable();
        $DB->insert(
            $table,
            [
                "$table.plugin_tam_days_id" => $tam->fields['plugin_tam_days_id'],
                "$table.date_start" => $tam->fields['date_start'],
                "$table.ip_start" => $tam->fields['ip_start'],
                "$table.origin_start" => $tam->fields['origin_start'],
                "$table.date_end" => $input['date'],
                "$table.ip_end" => $ip,
                "$table.origin_end" => PluginTamTam::WEB,
                "$table.working_time" => $actiontime,
                "$table.status" => PluginTamStatus::FIXED,
                "$table.reason" => $reason,
                "$table.on_working_time" => $owt,
            ]
        );
        $timer_id = $DB->insertId();
        PluginTamBlockedlog::createLog($timer_id, $input['users_id']);

        $DB->update(
            'glpi_plugin_tam_forgots',
            [
                'last_mod' => $input['users_id'],
                'comment' => $input['comment'],
                'date_mod' => $_SESSION["glpi_currenttime"]
            ],
            [
                'id' => $input['id'],
                'plugin_tam_tams_id' => $input['tam_id']
            ]
        );
    }

    public static function getHistoryData($start = 0, $limit = 0)
    {
        global $DB;

        $query = [
            'SELECT' => [
                'glpi_plugin_tam_forgots.*',
                'glpi_plugin_tam_days.users_id',
                'glpi_plugin_tam_days.date',
                'glpi_plugin_tam_tams.date_end'
            ],
            'FROM'   => 'glpi_plugin_tam_forgots',
            'INNER JOIN' => [
                'glpi_plugin_tam_tams' => [
                    'FKEY' => [
                        'glpi_plugin_tam_tams' => 'id',
                        'glpi_plugin_tam_forgots' => 'plugin_tam_tams_id',
                    ]
                ],
                'glpi_plugin_tam_days' => [
                    'FKEY' => [
                        'glpi_plugin_tam_tams' => 'plugin_tam_days_id',
                        'glpi_plugin_tam_days' => 'id'
                    ]
                ]
            ],
            'WHERE'  => [
                'NOT' => [
                    'last_mod' => null
                ]
            ],
            'ORDER'  => 'id DESC'
        ];

        if ($limit) {
            $query['START'] = (int)$start;
            $query['LIMIT'] = (int)$limit;
        }

        $iterator = $DB->request($query);

        $values = [];
        foreach ($iterator as $data) {
            $tmp = [];
            $tmp['id'] = $data['id'];
            $tmp['users_id'] = $data['users_id'];
            $tmp['date'] = $data['date'];
            $tmp['date_end'] = $data['date_end'];
            $tmp['last_mod'] = $data['last_mod'];
            $tmp['date_mod'] = $data['date_mod'];
            $tmp['comment'] = $data['comment'];
            $values[] = $tmp;
        }
        return $values;
    }

    public static function install(Migration $migration)
    {
        global $DB;

        $default_charset = DBConnection::getDefaultCharset();
        $default_collation = DBConnection::getDefaultCollation();
        $default_key_sign = DBConnection::getDefaultPrimaryKeySignOption();

        $table = 'glpi_plugin_tam_forgots';

        if (!$DB->tableExists($table)) {
            $migration->displayMessage("Installing $table");
            $query = "CREATE TABLE IF NOT EXISTS $table (
				`id` INT {$default_key_sign} NOT NULL auto_increment,
				`plugin_tam_tams_id` INT {$default_key_sign} NOT NULL,
				`name` VARCHAR(255) NOT NULL,
				`last_mod` INT {$default_key_sign},
				`date_mod` timestamp NULL DEFAULT NULL,
				`date_creation` timestamp NULL DEFAULT NULL,
				`comment` text,
				PRIMARY KEY (`id`)
			) ENGINE=InnoDB DEFAULT CHARSET={$default_charset} COLLATE={$default_collation} ROW_FORMAT=DYNAMIC;";
            $DB->query($query) or die($DB->error());
        }
    }
}
