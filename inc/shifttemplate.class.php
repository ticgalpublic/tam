<?php
/*
 -------------------------------------------------------------------------
 TAM plugin for GLPI
 Copyright (C) 2021-2023 by the TICgal Team.
 https://www.tic.gal
 -------------------------------------------------------------------------
 LICENSE
 This file is part of the TAM plugin.
 TAM plugin is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.
 TAM plugin is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with TAM. If not, see <http://www.gnu.org/licenses/>.
 --------------------------------------------------------------------------
 @package   TAM
 @author    the TICgal team
 @copyright Copyright (c) 2021-2023 TICgal team
 @license   AGPL License 3.0 or (at your option) any later version
            http://www.gnu.org/licenses/agpl-3.0-standalone.html
 @link      https://www.tic.gal
 @since     2021
 ----------------------------------------------------------------------
*/

if (!defined('GLPI_ROOT')) {
    die("Sorry. You can't access this file directly");
}

class PluginTamShiftTemplate extends CommonDropdown
{
    use Glpi\Features\PlanningEvent {
        prepareInputForAdd    as protected prepareInputForAddTrait;
        prepareInputForUpdate as protected prepareInputForUpdateTrait;
        rawSearchOptions      as protected trait_rawSearchOptions;
    }

    public $dohistory          = true;
    public $can_be_translated  = true;

    public static $rightname = 'plugin_tam_shift';

    public static function getTypeName($nb = 0)
    {
        return __('Shift template', 'tam');
    }

    public function getAdditionalFields()
    {
        return [
            [
                'name'  => 'state',
                'label' => __('Status'),
                'type'  => 'planningstate',
            ], [
                'name'  => 'plan',
                'label' => __('Calendar'),
                'type'  => 'plan',
            ], [
                'name'  => 'rrule',
                'label' => __('Repeat'),
                'type'  => 'rrule',
            ], [
                'name'  => 'text',
                'label' => __('Description'),
                'type'  => 'tinymce',
            ]
        ];
    }

    public function displaySpecificTypeField($ID, $field = [], array $options = [])
    {

        switch ($field['type']) {
            case 'planningstate':
                Planning::dropdownState("state", $this->fields["state"]);
                break;

            case 'plan':
                Planning::showAddEventClassicForm([
                    'duration'       => $this->fields['duration'],
                    'itemtype'       => self::getType(),
                    'items_id'       => $this->fields['id'],
                    '_display_dates' => false,
                ]);
                break;

            case 'rrule':
                echo self::showRepetitionForm($this->fields['rrule'] ?? '');
                break;
        }
    }

    public static function getSpecificValueToDisplay($field, $values, array $options = [])
    {
        if (!is_array($values)) {
            $values = [$field => $values];
        }

        switch ($field) {
            case 'state':
                return Planning::getState($values[$field]);
        }

        return parent::getSpecificValueToDisplay($field, $values, $options);
    }

    public static function getSpecificValueToSelect($field, $name = '', $values = '', array $options = [])
    {
        if (!is_array($values)) {
            $values = [$field => $values];
        }
        $options['display'] = false;

        switch ($field) {
            case 'state':
                return Planning::dropdownState($name, $values[$field], $options);
        }

        return parent::getSpecificValueToSelect($field, $name, $values, $options);
    }

    public function prepareInputForAdd($input)
    {
        $saved_input = $input;
        $input = $this->prepareInputForAddTrait($input);

        return $this->parseExtraInput($saved_input, $input);
    }

    public function prepareInputForupdate($input)
    {
        $saved_input = $input;
        $input = $this->prepareInputForupdateTrait($input);

        return $this->parseExtraInput($saved_input, $input);
    }

    public function parseExtraInput(array $orig_input = [], array $input = [])
    {
        if (isset($orig_input['plan']) && array_key_exists('_duration', $orig_input['plan'])) {
            $input['duration'] = $orig_input['plan']['_duration'];
        }

        if (isset($orig_input['_planningrecall']) && array_key_exists('before_time', $orig_input['_planningrecall'])) {
            $input['before_time'] = $orig_input['_planningrecall']['before_time'];
        }

        return $input;
    }


    public function rawSearchOptions()
    {
        return $this->trait_rawSearchOptions();
    }

    public static function install(Migration $migration)
    {
        global $DB;

        $default_charset = DBConnection::getDefaultCharset();
        $default_collation = DBConnection::getDefaultCollation();
        $default_key_sign = DBConnection::getDefaultPrimaryKeySignOption();

        $table = self::getTable();

        if (!$DB->tableExists($table)) {
            $migration->displayMessage("Installing $table");
            $query = "CREATE TABLE IF NOT EXISTS $table (
				`id` int {$default_key_sign} NOT NULL AUTO_INCREMENT,
				`entities_id` int {$default_key_sign} NOT NULL DEFAULT '0',
				`name` varchar(255) DEFAULT NULL,
				`text` text,
				`comment` text,
				`duration` int {$default_key_sign} NOT NULL DEFAULT '0',
				`before_time` int {$default_key_sign} NOT NULL DEFAULT '0',
				`rrule` text,
				`state` int {$default_key_sign} NOT NULL DEFAULT '0',
				`date_mod` timestamp NULL DEFAULT NULL,
				`date_creation` timestamp NULL DEFAULT NULL,
				PRIMARY KEY (`id`),
				KEY `entities_id` (`entities_id`),
				KEY `state` (`state`),
				KEY `date_mod` (`date_mod`),
				KEY `date_creation` (`date_creation`)
			) ENGINE=InnoDB DEFAULT CHARSET={$default_charset} COLLATE={$default_collation} ROW_FORMAT=DYNAMIC;";
            $DB->query($query) or die($DB->error());
        }
    }
}
