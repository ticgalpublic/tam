<?php
/*
 -------------------------------------------------------------------------
 TAM plugin for GLPI
 Copyright (C) 2021-2023 by the TICgal Team.
 https://www.tic.gal
 -------------------------------------------------------------------------
 LICENSE
 This file is part of the TAM plugin.
 TAM plugin is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.
 TAM plugin is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with TAM. If not, see <http://www.gnu.org/licenses/>.
 --------------------------------------------------------------------------
 @package   TAM
 @author    the TICgal team
 @copyright Copyright (c) 2021-2023 TICgal team
 @license   AGPL License 3.0 or (at your option) any later version
            http://www.gnu.org/licenses/agpl-3.0-standalone.html
 @link      https://www.tic.gal
 @since     2021
 ----------------------------------------------------------------------
*/

if (!defined('GLPI_ROOT')) {
    die("Sorry. You can't access this file directly");
}

use Glpi\CalDAV\Contracts\CalDAVCompatibleItemInterface;
use Glpi\CalDAV\Traits\VobjectConverterTrait;
use Sabre\VObject\Component\VCalendar;
use Sabre\VObject\Component\VTodo;
use RRule\RRule;

class PluginTamShift extends CommonDBTM implements CalDAVCompatibleItemInterface
{
    use Glpi\Features\PlanningEvent {
        rawSearchOptions as protected trait_rawSearchOptions;
        prepareInputForUpdate as protected trait_prepareInputForUpdate;
        prepareInputForAdd as protected trait_prepareInputForAdd;
    }
    use VobjectConverterTrait;

    public static $rightname = 'plugin_tam_shift';

    public static function getTypeName($nb = 0)
    {
        return __('Shift', 'tam');
    }

    public function canUpdateItem()
    {

        if (!Session::haveRight(self::$rightname, UPDATE)) {
            return false;
        }

        return parent::canUpdateItem();
    }

    public function canPurgeItem()
    {

        if (!Session::haveRight(self::$rightname, PURGE)) {
            return false;
        }

        return parent::canPurgeItem();
    }

    public function defineTabs($options = [])
    {
        $ong = [];
        $this->addDefaultFormTab($ong);
        $this->addStandardTab('Document_Item', $ong, $options);
        $this->addStandardTab('Log', $ong, $options);

        return $ong;
    }

    public function showForm($ID, $options = [])
    {
        global $CFG_GLPI;

        $canedit    = $this->can($ID, UPDATE);
        $rand       = mt_rand();
        $rand_plan  = mt_rand();
        $rand_rrule = mt_rand();

        $this->initForm($ID, $options);
        $this->showFormHeader($options);

        $is_ajax  = isset($options['from_planning_edit_ajax']) && $options['from_planning_edit_ajax'];
        $is_rrule = strlen($this->fields['rrule']) > 0;

        // set event for another user
        if (isset($options['res_itemtype']) && isset($options['res_items_id']) && strtolower($options['res_itemtype']) == "user") {
            $this->fields['users_id'] =  $options['res_items_id'];
        }

        if ($canedit) {
            $tpl_class = 'PluginTamShiftTemplate';
            echo "<tr class='tab_bg_1' style='vertical-align: top'>";
            echo "<td colspan='2'>" . $tpl_class::getTypeName() . "</td>";
            echo "<td colspan='2'>";
            $tpl_class::dropdown([
                'value'     => $this->fields['plugin_tam_shifttemplates_id'],
                'entity'    => $this->getEntityID(),
                'rand'      => $rand,
                'on_change' => "template_update$rand(this.value)"
            ]);

            $ajax_url = Plugin::getWebDir('tam') . "/ajax/planning.php";
            $JS = <<<JAVASCRIPT
				function template_update{$rand}(value) {
					$.ajax({
						url: '{$ajax_url}',
						type: "POST",
						data: {
							action: 'get_shift_template',
							plugin_tam_shifttemplates_id: value
						}
					}).done(function(data) {
						// set common fields
						if (data.name.length > 0) {
							$("#textfield_name{$rand}").val(data.name);
						}
						$("#dropdown_state{$rand}").trigger("setValue", data.state);
						if (data.planningeventcategories_id > 0) {
							$("#dropdown_planningeventcategories_id{$rand}").trigger("setValue", data.planningeventcategories_id);
						}
						$("#dropdown_background{$rand}").trigger("setValue", data.background);
						if (data.text.length > 0) {
							if (contenttinymce = tinymce.get("text{$rand}")) {
								contenttinymce.setContent(data.text);
							}
						}

						// set planification fields
						if (data.duration > 0) {
							$("#dropdown_plan__duration_{$rand_plan}").trigger("setValue", data.duration);
						}
						$("#dropdown__planningrecall_before_time_{$rand_plan}").trigger("setValue", data.before_time);

						// set rrule fields
						if (data.rrule != null && data.rrule.freq != null ) {
							$("#dropdown_rrule_freq_{$rand_rrule}").trigger("setValue", data.rrule.freq);
							$("#dropdown_rrule_interval_{$rand_rrule}").trigger("setValue", data.rrule.interval);
							$("#showdate{$rand_rrule}").val(data.rrule.until);
							$("#dropdown_rrule_byday_{$rand_rrule}").val(data.rrule.byday).trigger('change');
							$("#dropdown_rrule_bymonth_{$rand_rrule}").val(data.rrule.bymonth).trigger('change');
						}
					});
				}
		JAVASCRIPT;
            echo Html::scriptBlock($JS);
            echo "</tr>";
        }

        echo "<tr class='tab_bg_2'><td colspan='2'>" . __('Title') . "</td>";
        echo "<td colspan='2'>";
        if (isset($options['start'])) {
            echo Html::hidden('day', ['value' => $options['start']]);
        }
        if ($canedit) {
            echo Html::input(
                'name',
                [
                    'value' => $this->fields['name'],
                    'id'    => "textfield_name$rand",
                ]
            );
        } else {
            echo $this->fields['name'];
        }
        if (isset($options['from_planning_edit_ajax']) && $options['from_planning_edit_ajax']) {
            echo Html::hidden('from_planning_edit_ajax');
        }
        echo "</td>";
        echo "</tr>";

        echo "<tr class='tab_bg_2'><td colspan='2'>" . __('User') . "</td>";
        echo "<td colspan='2'>";
        User::dropdown([
            'name'          => 'users_id',
            'right'         => 'all',
            'value'         => $this->fields['users_id']
        ]);
        echo "</td>";
        echo "</tr>";

        echo "<tr class='tab_bg_2'>";
        echo "<td colspan='2'>" . __('Status') . "</td>";
        echo "<td colspan='2'>";
        if ($canedit) {
            Planning::dropdownState("state", $this->fields["state"], true, [
                'rand' => $rand,
            ]);
        } else {
            echo Planning::getState($this->fields["state"]);
        }
        echo "</td>";
        echo "</tr>";

        echo "<tr class='tab_bg_2'><td  colspan='2'>" . __('Calendar') . "</td>";
        echo "<td>";
        Planning::showAddEventClassicForm([
            'items_id'  => $this->fields['id'],
            'itemtype'  => $this->getType(),
            'begin'     => $this->fields['begin'],
            'end'       => $this->fields['end'],
            'rand_user' => $this->fields['users_id'],
            'rand'      => $rand_plan,
        ]);
        echo "</td></tr>";

        echo "<tr class='tab_bg_2'><td  colspan='2'>" . __('Repeat') . "</td>";
        echo "<td>";
        echo self::showRepetitionForm($this->fields['rrule'] ?? '', ['rand' => $rand_rrule]);
        echo "</td></tr>";

        echo "<tr class='tab_bg_2'><td>" . __('Description') . "</td>" . "<td colspan='3'>";

        if ($canedit) {
            Html::textarea([
                'name'              => 'text',
                'value'             => $this->fields["text"],
                'enable_richtext'   => true,
                'enable_fileupload' => true,
                'rand'              => $rand,
                'editor_id'         => 'text' . $rand,
            ]);
        } else {
            echo "<div>";
            echo Toolbox::unclean_html_cross_side_scripting_deep($this->fields["text"]);
            echo "</div>";
        }

        echo "</td></tr>";

        if ($is_ajax && $is_rrule) {
            $options['candel'] = false;
            $options['addbuttons'] = [
                'purge'          => "<i class='fas fa-trash-alt'></i>&nbsp;" . __("Delete serie"),
                'purge_instance' => "<i class='far fa-trash-alt'></i>&nbsp;" . __("Delete instance"),
            ];
        }

        $this->showFormButtons($options);

        return true;
    }

    public static function getGroupItemsAsVCalendars($groups_id)
    {
        return self::getItemsAsVCalendars([]);
    }

    public static function getUserItemsAsVCalendars($users_id)
    {

        return self::getItemsAsVCalendars([
            'OR' => [
                self::getTableField('users_id') => $users_id,
            ]
        ]);
    }

    private static function getItemsAsVCalendars(array $criteria)
    {

        global $DB;

        $query = [
            'FROM'  => self::getTable(),
            'WHERE' => $criteria,
        ];

        $event_iterator = $DB->request($query);

        $vcalendars = [];
        foreach ($event_iterator as $event) {
            $item = new self();
            $item->getFromResultSet($event);
            $vcalendar = $item->getAsVCalendar();
            if (null !== $vcalendar) {
                $vcalendars[] = $vcalendar;
            }
        }

        return $vcalendars;
    }

    public function getAsVCalendar()
    {

        if (!$this->canViewItem()) {
            return null;
        }

        // Transform HTML text to plain text
        $this->fields['text'] = Html::clean(
            Toolbox::unclean_cross_side_scripting_deep(
                $this->fields['text']
            )
        );

        $is_task = in_array($this->fields['state'], [Planning::DONE, Planning::TODO]);
        $is_planned = !empty($this->fields['begin']) && !empty($this->fields['end']);
        $target_component = $this->getTargetCaldavComponent($is_planned, $is_task);
        if (null === $target_component) {
            return null;
        }

        $vcalendar = $this->getVCalendarForItem($this, $target_component);

        return $vcalendar;
    }

    public function getInputFromVCalendar(VCalendar $vcalendar)
    {

        $vcomp = $vcalendar->getBaseComponent();

        $input = $this->getCommonInputFromVcomponent($vcomp);

        $input['text'] = $input['content'];
        unset($input['content']);

        if ($vcomp instanceof VTodo && !array_key_exists('state', $input)) {
            // Force default state to TO DO or event will be considered as VEVENT
            $input['state'] = \Planning::TODO;
        }

        return $input;
    }

    public function getDaysFromRrule($rules, $start, $end)
    {
        $utc_tz = new \DateTimeZone('UTC');
        $rrule_specs = json_decode($rules, true);
        $exceptions = [];
        if (array_key_exists('byweekday', $rrule_specs)) {
            $rrule_specs['byday'] = $rrule_specs['byweekday'];
            unset($rrule_specs['byweekday']);
        }
        if (array_key_exists('until', $rrule_specs) && empty($rrule_specs['until'])) {
            $rrule_specs['until'] = (new \DateTime($end))->setTimeZone($utc_tz);
        }
        if (array_key_exists('exceptions', $rrule_specs)) {
            foreach ($rrule_specs['exceptions'] as $exdate) {
                $exceptions[] = $exdate;
            }
            unset($rrule_specs['exceptions']);
        }
        $rrule_specs['dtstart'] = (new \DateTime($start))->setTimeZone($utc_tz);
        $rrule = new RRule($rrule_specs);
        $days = [];
        foreach ($rrule as $occurrence) {
            if (!in_array($occurrence->format("Y-m-d"), $exceptions)) {
                $days[] = $occurrence->format("Y-m-d");
            }
        }
        return $days;
    }

    public function prepareInputForAdd($input)
    {
        if (isset($input['rrule'])) {
            $user_id = (!isset($input['users_id']) || empty($input['users_id'])) ? Session::getLoginUserID() : $input['users_id'];
            $begin = $_SESSION["glpi_currenttime"];
            if (isset($input['plan'])) {
                if (!empty($input['plan']["begin"])) {
                    $begin = $input['plan']["begin"];
                }
            }
            $exception = (isset($input['rrule']) && isset($input['rrule']['exceptions']) && !empty($input['rrule']['exceptions'])) ? explode(",", $input['rrule']['exceptions']) : [];
            $input['rrule']['exceptions'] = $this->addException($user_id, $input['entities_id'], $begin, $exception);
        }

        return $this->trait_prepareInputForAdd($input);
    }

    public function prepareInputForUpdate($input)
    {

        if (isset($input['rrule'])) {
            if (isset($input['users_id'])) {
                $user_id = $input['users_id'];
            } else {
                $user_id = 0;
            }
            if (isset($input['entities_id'])) {
                $entities_id = $input['entities_id'];
            } else {
                $entities_id = 0;
            }
            if (isset($input['plan']['begin'])) {
                $begin = $input['plan']['begin'];
            } else {
                $begin = 0;
            }
            if (is_array($input['rrule']['exceptions'])) {
                $rrule = $input['rrule']['exceptions'];
            } else {
                $rrule = explode(",", $input['rrule']['exceptions']);
            }
            $input['rrule']['exceptions'] = $this->addException($user_id, $entities_id, $begin, $rrule);
        }

        return $this->trait_prepareInputForUpdate($input);
    }

    public function addException($user_id, $entities_id, $begin, $exceptions = [])
    {
        global $DB;

        if (!$calendars_id = PluginTamUserCalendar::getCalendar($user_id)) {
            $calendars_id = Entity::getUsedConfig('calendars_strategy', $entities_id, 'calendars_id', 0);
        }

        $begin = date("Y-m-d", strtotime($begin));
        if ($calendars_id > 0) {
            $query = [
                'SELECT' => 'glpi_holidays.*',
                'FROM' => 'glpi_calendars_holidays',
                'INNER JOIN' => [
                    'glpi_holidays' => [
                        'FKEY' => [
                            'glpi_calendars_holidays' => 'holidays_id',
                            'glpi_holidays' => 'id'
                        ]
                    ]
                ],
                'WHERE' => [
                    'calendars_id' => $calendars_id,
                    'is_perpetual' => 0,
                    'begin_date' => ['>=', $begin]
                ]
            ];
            foreach ($DB->request($query) as $id => $row) {
                $start = date("Y-m-d", strtotime($row['begin_date']));
                $end = date("Y-m-d", strtotime($row['end_date']));
                if ($start == $end) {
                    if (!in_array($start, $exceptions)) {
                        $exceptions[] = $start;
                    }
                } else {
                    for ($i = $start; $i <= $end; $i = date('Y-m-d', strtotime($i . ' + 1 days'))) {
                        if (!in_array($i, $exceptions)) {
                            $exceptions[] = $i;
                        }
                    }
                }
            }
            $query = [
                'SELECT' => 'glpi_holidays.*',
                'FROM' => 'glpi_calendars_holidays',
                'INNER JOIN' => [
                    'glpi_holidays' => [
                        'FKEY' => [
                            'glpi_calendars_holidays' => 'holidays_id',
                            'glpi_holidays' => 'id'
                        ]
                    ]
                ],
                'WHERE' => [
                    'calendars_id' => $calendars_id,
                    'is_perpetual' => 1,
                ]
            ];
            foreach ($DB->request($query) as $id => $row) {
                $year = date("Y");
                $start = date($year . "-m-d", strtotime($row['begin_date']));
                $end = date($year . "-m-d", strtotime($row['end_date']));
                if ($start == $end) {
                    if (!in_array($start, $exceptions)) {
                        $exceptions[] = $start;
                    }
                    $nextyear = date('Y-m-d', strtotime($start . ' + 1 year'));
                    if (!in_array($nextyear, $exceptions)) {
                        $exceptions[] = $nextyear;
                    }
                } else {
                    for ($i = $start; $i <= $end; $i = date('Y-m-d', strtotime($i . ' + 1 days'))) {
                        if (!in_array($i, $exceptions)) {
                            $exceptions[] = $i;
                        }
                        $nextyear = date('Y-m-d', strtotime($i . ' + 1 year'));
                        if (!in_array($nextyear, $exceptions)) {
                            $exceptions[] = $nextyear;
                        }
                    }
                }
            }
        }
        $query = [
            'FROM' => PluginTamLeave::getTable(),
            'WHERE' => [
                'users_id' => $user_id,
                'date_start' => ['>=', $begin],
                'is_deleted' => 0
            ],
            'ORDER' => [
                'date_start ASC'
            ]
        ];
        foreach ($DB->request($query) as $id => $row) {
            $start = date("Y-m-d", strtotime($row['date_start']));
            $finish = date("Y-m-d", strtotime($row['date_end']));
            if ($start == $finish) {
                if (!in_array($start, $exceptions)) {
                    $exceptions[] = $start;
                }
            } else {
                for ($i = $start; $i <= $finish; $i = date('Y-m-d', strtotime($i . ' + 1 days'))) {
                    if (!in_array($i, $exceptions)) {
                        $exceptions[] = $i;
                    }
                }
            }
        }

        return $exceptions;
    }

    public static function install(Migration $migration)
    {
        global $DB;

        $default_charset = DBConnection::getDefaultCharset();
        $default_collation = DBConnection::getDefaultCollation();
        $default_key_sign = DBConnection::getDefaultPrimaryKeySignOption();

        $table = self::getTable();

        if (!$DB->tableExists($table)) {
            $migration->displayMessage("Installing $table");
            $query = "CREATE TABLE IF NOT EXISTS $table (
				`id` int {$default_key_sign} NOT NULL AUTO_INCREMENT,
				`plugin_tam_shifttemplates_id` int {$default_key_sign} NOT NULL DEFAULT '0',
				`entities_id` int {$default_key_sign} NOT NULL DEFAULT '0',
				`is_recursive` TINYINT NOT NULL DEFAULT '1',
				`name` varchar(255) DEFAULT NULL,
				`text` text,
				`users_id` int {$default_key_sign} NOT NULL DEFAULT '0',
				`state` int {$default_key_sign} NOT NULL DEFAULT '0',
				`begin` timestamp NULL DEFAULT NULL,
				`end` timestamp NULL DEFAULT NULL,
				`rrule` text,
				`date_mod` timestamp NULL DEFAULT NULL,
				`date_creation` timestamp NULL DEFAULT NULL,
				PRIMARY KEY (`id`),
				KEY `entities_id` (`entities_id`),
				KEY `is_recursive` (`is_recursive`),
				KEY `date_mod` (`date_mod`),
				KEY `date_creation` (`date_creation`)
			) ENGINE=InnoDB DEFAULT CHARSET={$default_charset} COLLATE={$default_collation} ROW_FORMAT=DYNAMIC;";
            $DB->query($query) or die($DB->error());
        }
    }
}
