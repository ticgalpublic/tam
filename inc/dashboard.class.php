<?php
/*
 -------------------------------------------------------------------------
 TAM plugin for GLPI
 Copyright (C) 2021-2023 by the TICgal Team.
 https://www.tic.gal
 -------------------------------------------------------------------------
 LICENSE
 This file is part of the TAM plugin.
 TAM plugin is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.
 TAM plugin is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with TAM. If not, see <http://www.gnu.org/licenses/>.
 --------------------------------------------------------------------------
 @package   TAM
 @author    the TICgal team
 @copyright Copyright (c) 2021-2023 TICgal team
 @license   AGPL License 3.0 or (at your option) any later version
            http://www.gnu.org/licenses/agpl-3.0-standalone.html
 @link      https://www.tic.gal
 @since     2021
 ----------------------------------------------------------------------
*/

class PluginTamDashboard extends CommonDBTM
{
    public static function dashboardTypes()
    {
        return [
            'holidays' => [
                'label' => __("Holidays"),
            ]
        ];
    }

    public static function dashboardCards($cards)
    {
        $cards['plugin_tam_holiday'] = [
            'widgettype' => ["bigNumber"],
            'itemtype' => self::getType(),
            'label' => __('Holiday', 'tam'),
            'group' => __("TAM", "tam"),
            'provider' => "PluginTamDashboard::bigNumberHolidays"
        ];
        $cards['plugin_tam_leaves'] = [
            'widgettype' => ['pie', 'donut', 'halfpie', 'halfdonut', 'multipleNumber', 'bar', 'hbar'],
            'itemtype' => PluginTamLeave::getType(),
            'label' => __("Number of leaves by user", 'tam'),
            'group' => __("TAM", "tam"),
            'provider' => "PluginTamDashboard::leavesByUser"
        ];
        $cards['plugin_tam_leavestype'] = [
            'widgettype' => ['bars', 'stackedbars'],
            'itemtype' => PluginTamLeave::getType(),
            'label' => __("Leaves type by user", 'tam'),
            'group' => __("TAM", "tam"),
            'provider' => "PluginTamDashboard::leavestypeByUser"
        ];
        return $cards;
    }

    public static function bigNumberHolidays($params = [])
    {
        $default_params = [
            'label' => null,
            'icon' => null
        ];

        $params = array_merge($default_params, $params);
        $holidays = PluginTamHoliday::getHolidays(Session::getLoginUserID(), $_SESSION["glpiactive_entity"]);
        $total_holidays = 0;
        foreach ($holidays as $key => $value) {
            $start = new DateTime(date("Y-m-d 00:00:00", strtotime($value['begin'])));
            $end = new DateTime(date("Y-m-d 23:59:59", strtotime($value['end'])));
            $interval = $start->diff($end);
            $total_holidays += ($interval->format('%d') + 1);
        }
        return [
            'number' => $total_holidays,
            'label' => __('Holiday', 'tam'),
            'icon' => 'fas fa-store-slash'
        ];
    }

    public static function leavesByUser($params = [])
    {
        global $DB;

        $yearstart = date('Y-m-d', strtotime('01/01'));
        $yearend = date('Y-m-d', strtotime('12/31'));
        $query = [
            'FROM' => PluginTamLeave::getTable(),
            'WHERE' => [
                'date_start' => ['<=', $yearend],
                'date_end' => ['>=', $yearstart],
                'is_deleted' => 0
            ]
        ];
        $userleave = [];
        foreach ($DB->request($query) as $id => $row) {
            $start = new DateTime(date("Y-m-d", strtotime($row['date_start'])));
            $end = new DateTime(date("Y-m-d", strtotime($row['date_end'])));
            $interval = $start->diff($end);
            if (!array_key_exists($row['users_id'], $userleave)) {
                $userleave[$row['users_id']] = ($interval->format('%d') + 1);
            } else {
                $userleave[$row['users_id']] += ($interval->format('%d') + 1);
            }
        }

        $data = [];
        foreach ($userleave as $key => $value) {
            $data[] = [
                'number' => $value,
                'label' => getUserName($key)
            ];
        }

        return [
            'data' => $data,
            'label' => $params['label'],
            'icon' => 'far fa-calendar-times'
        ];
    }

    public static function leavestypeByUser($params = [])
    {
        global $DB;

        $yearstart = date('Y-m-d', strtotime('01/01'));
        $yearend = date('Y-m-d', strtotime('12/31'));
        $tableleave = PluginTamLeave::getTable();

        $usersquery = [
            'SELECT' => [
                $tableleave . ".users_id",
            ],
            'DISTINCT' => true,
            'FROM' => $tableleave,
            'INNER JOIN' => [
                'glpi_plugin_tam_leavetypes' => [
                    'FKEY' => [
                        $tableleave => 'plugin_tam_leavetypes_id',
                        'glpi_plugin_tam_leavetypes' => 'id'
                    ]
                ]
            ],
            'WHERE' => [
                'date_start' => ['<=', $yearend],
                'date_end' => ['>=', $yearstart],
                $tableleave . '.is_deleted' => 0
            ],
            'ORDER' => [
                'users_id ASC'
            ]
        ];
        $users = [];
        foreach ($DB->request($usersquery) as $key => $row) {
            $users[] = getUserName($row['users_id']);
        }


        $query = [
            'SELECT' => [
                $tableleave . ".users_id",
                $tableleave . ".date_start",
                $tableleave . ".date_end",
                $tableleave . ".calendars_id",
                'completename'
            ],
            'FROM' => $tableleave,
            'INNER JOIN' => [
                'glpi_plugin_tam_leavetypes' => [
                    'FKEY' => [
                        $tableleave => 'plugin_tam_leavetypes_id',
                        'glpi_plugin_tam_leavetypes' => 'id'
                    ]
                ]
            ],
            'WHERE' => [
                'date_start' => ['<=', $yearend],
                'date_end' => ['>=', $yearstart],
                $tableleave . '.is_deleted' => 0
            ],
            'ORDER' => [
                'users_id ASC'
            ]
        ];
        $userleave = [];
        foreach ($DB->request($query) as $id => $row) {
            $start = new DateTime(date("Y-m-d", strtotime($row['date_start'])));
            $end = new DateTime(date("Y-m-d", strtotime($row['date_end'])));

            $checkdate = date("Y-m-d", strtotime($row['date_start']));
            $checkdate_end = date("Y-m-d", strtotime($row['date_end']));
            $no_working_days = 0;
            while($checkdate != $checkdate_end){
                if(!PluginTamPdf::isWorkingDay($checkdate, $row['calendars_id'])){
                    $no_working_days++;
                }
                $checkdate = date("Y-m-d", strtotime($checkdate . ' +1 day'));
            }

            // +1 because it's inclusive
            $interval = ($start->diff($end))->days - $no_working_days + 1;

            if (!array_key_exists($row['completename'], $userleave)) {
                $userleave[$row['completename']][getUserName($row['users_id'])] = $interval;
            } else {
                if (!array_key_exists(getUserName($row['users_id']), $userleave[$row['completename']])) {
                    $userleave[$row['completename']][getUserName($row['users_id'])] = $interval;
                } else {
                    $userleave[$row['completename']][getUserName($row['users_id'])] += $interval;
                }
            }
        }

        $series = [];
        foreach ($userleave as $key => $value) {
            $data = [];
            foreach ($users as $id => $user) {
                if (!array_key_exists($user, $value)) {
                    $days = 0;
                } else {
                    $days = $value[$user];
                }
                $data[] = ['value' => $days];
            }
            $series[] = [
                'name' => $key,
                'data' => $data
            ];
        }

        $data = [
            'labels' => $users,
            'series' => $series
        ];
        return [
            'data' => $data,
            'label' => $params['label'],
            'icon' => 'far fa-calendar-times'
        ];
    }
}
