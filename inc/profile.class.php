<?php
/*
 -------------------------------------------------------------------------
 TAM plugin for GLPI
 Copyright (C) 2021-2023 by the TICgal Team.
 https://www.tic.gal
 -------------------------------------------------------------------------
 LICENSE
 This file is part of the TAM plugin.
 TAM plugin is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.
 TAM plugin is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with TAM. If not, see <http://www.gnu.org/licenses/>.
 --------------------------------------------------------------------------
 @package   TAM
 @author    the TICgal team
 @copyright Copyright (c) 2021-2023 TICgal team
 @license   AGPL License 3.0 or (at your option) any later version
            http://www.gnu.org/licenses/agpl-3.0-standalone.html
 @link      https://www.tic.gal
 @since     2021
 ----------------------------------------------------------------------
*/

if (!defined('GLPI_ROOT')) {
    die("Sorry. You can't access directly to this file");
}

class PluginTamProfile extends Profile
{
    public static $rightname = 'profile';

    public function getTabNameForItem(CommonGLPI $item, $withtemplate = 0)
    {
        if ($item->getType() == 'Profile') {
            return self::createTabEntry(PluginTamTam::getTypeName(2));
        }
        return '';
    }

    public static function displayTabContentForItem(CommonGLPI $item, $tabnum = 1, $withtemplate = 0)
    {
        if ($item->getType() == 'Profile') {
            $ID = $item->getID();
            $prof = new self();

            self::addDefaultProfileInfos($ID, ['plugin_tam_active_timer' => 1, 'plugin_tam_all_timers' => 0, 'plugin_tam_leave' => 0]);
            $prof->showForm($ID);
        }
        return true;
    }

    public static function addDefaultProfileInfos($profiles_id, $rights, $drop_existing = false)
    {
        global $DB;

        $profileRight = new ProfileRight();
        foreach ($rights as $right => $value) {
            if (countElementsInTable('glpi_profilerights', ['profiles_id' => $profiles_id, 'name' => $right]) && $drop_existing) {
                $profileRight->deleteByCriteria([
                    'profiles_id' => $profiles_id,
                    'name'        => $right
                ]);
            }
            if (!countElementsInTable('glpi_profilerights', ['profiles_id' => $profiles_id, 'name' => $right])) {
                $profileRight->add([
                    'profiles_id' => $profiles_id,
                    'name'        => $right,
                    'rights'      => $value,
                ]);
                //Add right to the current session
                $_SESSION['glpiactiveprofile'][$right] = $value;
            }
        }
    }

    public static function getAllRights($all = false)
    {

        $rights = [
            [
                'itemtype' => 'PluginTamLeave',
                'label' => __('Leave', 'tam'),
                'field' => 'plugin_tam_leave'
            ]
        ];

        $rights[] = [
            'itemtype' => 'PluginTamShift',
            'label' => __('Shift', 'tam'),
            'field' => 'plugin_tam_shift'
        ];

        if ($all) {
            $rights[] = [
                'itemtype' => 'PluginTamTam',
                'label'    => __('Show timer', 'tam'),
                'field'    => 'plugin_tam_active_timer'
            ];
            $rights[] = [
                'itemtype' => 'PluginTamTam',
                'label' => __('View all timers', 'tam'),
                'field' => 'plugin_tam_all_timers'
            ];
        }

        return $rights;
    }

    public function showForm($ID = 0, $options = [])
    {

        echo "<div class='firstbloc'>";

        if (!Session::haveRight("profile", READ)) {
            return false;
        }

        $canedit = Session::haveRight("profile", UPDATE);

        $profile = new Profile();
        $profile->getFromDB($ID);

        echo "<form action='" . Profile::getFormUrl() . "' method='post'>";

        if ($profile->getField('interface') == 'central') {
            $rights = $this->getAllRights();
            $profile->displayRightsChoiceMatrix($rights, [
                'canedit'       => $canedit,
                'default_class' => 'tab_bg_2',
                'title'         => __('General'),
            ]);
        }

        echo "<table class='tab_cadre_fixehov'>";
        echo "<tr class='tab_bg_1'><th colspan='4'>" . __('Helpdesk') . "</th></tr>\n";
        $effective_rights = ProfileRight::getProfileRights($ID, ['plugin_tam_active_timer', 'plugin_tam_all_timers']);
        echo "<tr class='tab_bg_2'>";
        echo "<td width='20%'>" . __('Show timer', 'tam') . "</td>";
        echo "<td colspan='5'>";
        Html::showCheckbox(['name'    => '_plugin_tam_active_timer', 'checked' => $effective_rights['plugin_tam_active_timer']]);
        echo "</td></tr>\n";
        echo "<tr class='tab_bg_2'>";
        echo "<td width='20%'>" . __('View all timers', 'tam') . "</td>";
        echo "<td colspan='5'>";
        Html::showCheckbox(['name'    => '_plugin_tam_all_timers', 'checked' => $effective_rights['plugin_tam_all_timers']]);
        echo "</td></tr>\n";
        echo "</table>";

        if ($canedit) {
            echo "<div class='center'>";
            echo Html::hidden('id', ['value' => $ID]);
            echo Html::submit(_sx('button', 'Save'), ['name' => 'update', 'class' => 'btn btn-primary']);
            echo "</div>";
            Html::closeForm();
        }

        echo "</div>";
    }

    public static function initProfile()
    {
        global $DB;
        $profile = new self();
        //Add new rights in glpi_profilerights table
        foreach ($profile->getAllRights(true) as $data) {
            if (!countElementsInTable("glpi_profilerights", ['name' => $data['field']])) {
                ProfileRight::addProfileRights([$data['field']]);
            }
        }
        foreach ($DB->request("SELECT *
										FROM `glpi_profilerights`
										WHERE `profiles_id`='" . $_SESSION['glpiactiveprofile']['id'] . "'
										AND `name` LIKE '%plugin_tam%'") as $prof) {
            $_SESSION['glpiactiveprofile'][$prof['name']] = $prof['rights'];
        }
    }

    public static function install(Migration $migration)
    {
        global $DB;

        self::initProfile();
    }

    public static function uninstall()
    {
        global $DB;
        $DB->query("DELETE FROM glpi_profilerights WHERE name LIKE 'plugin_tam_%'");
        self::removeRightsFromSession();
    }

    public static function removeRightsFromSession()
    {
        foreach (self::getAllRights(true) as $right) {
            if (isset($_SESSION['glpiactiveprofile'][$right['field']])) {
                unset($_SESSION['glpiactiveprofile'][$right['field']]);
            }
        }
    }
}
