<?php
/*
 -------------------------------------------------------------------------
 TAM plugin for GLPI
 Copyright (C) 2021-2023 by the TICgal Team.
 https://www.tic.gal
 -------------------------------------------------------------------------
 LICENSE
 This file is part of the TAM plugin.
 TAM plugin is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.
 TAM plugin is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with TAM. If not, see <http://www.gnu.org/licenses/>.
 --------------------------------------------------------------------------
 @package   TAM
 @author    the TICgal team
 @copyright Copyright (c) 2021-2023 TICgal team
 @license   AGPL License 3.0 or (at your option) any later version
            http://www.gnu.org/licenses/agpl-3.0-standalone.html
 @link      https://www.tic.gal
 @since     2021
 ----------------------------------------------------------------------
*/

if (!defined('GLPI_ROOT')) {
    die("Sorry. You can't access directly to this file");
}

class PluginTamUserCalendar extends CommonDBRelation
{
    public static function showField(User $user)
    {
        echo "<table class='tab_cadre_fixe'><tbody>";
        echo "<tr class='tab_bg_1'><th colspan='4'>" . __('TAM', 'tam') . "</th></tr>";
        if ($user->fields['id'] > 0) {
            echo "<tr class='tab_bg_1'>";
            echo "<td>" . __('Calendar') . "</td>";
            echo "<td>";
            Calendar::dropdown(['value'  => self::getCalendar($user->fields['id'])]);
            echo "</td>";
            echo "<td>" . __('Workplace', 'tam') . "</td>";
            echo "<td>";
            Location::dropdown(['name' => 'workplace', 'value' => self::getWorkplace($user->fields['id'])]);
            echo "</td>";
            echo "</tr>";
        } else {
            echo "<tr class='tab_bg_1'>";
            echo "<td>" . __('Calendar') . "</td>";
            echo "<td>";
            Calendar::dropdown();
            echo "</td>";
            echo "<td>" . __('Workplace', 'tam') . "</td>";
            echo "<td>";
            Location::dropdown(['name' => 'workplace']);
            echo "</td>";
            echo "</tr>";
        }
        echo "</tbody></table>";
    }

    public static function getCalendar($user_id)
    {
        global $DB;

        $query = [
            'FROM' => self::getTable(),
            'WHERE' => [
                'users_id' => $user_id,
            ]
        ];
        if ($row = $DB->request($query)->current()) {
            return $row['calendars_id'];
        } else {
            return 0;
        }
    }

    public static function getWorkplace($user_id)
    {
        global $DB;

        $query = [
            'FROM' => self::getTable(),
            'WHERE' => [
                'users_id' => $user_id,
            ]
        ];
        if ($row = $DB->request($query)->current()) {
            return $row['locations_id'];
        } else {
            return 0;
        }
    }

    public static function addCalendar(User $user)
    {
        global $DB;

        if (isset($user->input['calendars_id'])) {
            $input = [
                'users_id' => $user->fields['id'],
                'calendars_id' => $user->input['calendars_id']
            ];

            if (isset($user->input['workplace'])) {
                $input['locations_id'] = $user->input['workplace'];
            }
            $DB->insert(self::getTable(), $input);
            self::addUserLog($user->fields['id'], 0, $user->input['calendars_id']);
        }
    }

    public static function updateCalendar(User $user)
    {
        global $DB;

        if (isset($user->input['calendars_id']) && $user->input['calendars_id'] != self::getCalendar($user->fields['id'])) {
            $old = self::getCalendar($user->fields['id']);
            $new = $user->input['calendars_id'];
            $DB->updateOrInsert(self::getTable(), ['calendars_id' => $user->input['calendars_id']], ['users_id' => $user->fields['id']]);
            self::addUserLog($user->fields['id'], $old, $new);
        }

        if (isset($user->input['workplace']) && $user->input['workplace'] != self::getWorkplace($user->fields['id'])) {
            $DB->updateOrInsert(self::getTable(), ['locations_id' => $user->input['workplace']], ['users_id' => $user->fields['id']]);
        }
    }

    public static function addUserLog($id, $old, $new)
    {
        global $DB;

        if ($uid = Session::getLoginUserID(false)) {
            if (is_numeric($uid)) {
                $username = sprintf(__('%1$s (%2$s)'), getUserName($uid), $uid);
            } else {
                $username = $uid;
            }
        } else {
            $username = "";
        }

        $params = [
            'items_id'          => $id,
            'itemtype'          => User::getType(),
            'linked_action'     => 0,
            'user_name'         => addslashes($username),
            'date_mod'          => $_SESSION["glpi_currenttime"],
            'id_search_option'  => 1000,
            'old_value'         => $old,
            'new_value'         => $new
        ];
        $result = $DB->insert(Log::getTable(), $params);

        if ($result && $DB->affectedRows($result) > 0) {
            return $_SESSION['glpi_maxhistory'] = $DB->insertId();
        }
    }

    public static function install(Migration $migration)
    {
        global $DB;

        $default_charset = DBConnection::getDefaultCharset();
        $default_collation = DBConnection::getDefaultCollation();
        $default_key_sign = DBConnection::getDefaultPrimaryKeySignOption();

        $table = self::getTable();

        if (!$DB->tableExists($table)) {
            $migration->displayMessage("Installing $table");
            $query = "CREATE TABLE IF NOT EXISTS $table (
				`id` INT {$default_key_sign} NOT NULL auto_increment,
				`users_id` INT {$default_key_sign} NOT NULL,
				`calendars_id` INT {$default_key_sign} NOT NULL DEFAULT '0',
				`locations_id` INT {$default_key_sign} NOT NULL DEFAULT '0',
				PRIMARY KEY (`id`)
			) ENGINE=InnoDB DEFAULT CHARSET={$default_charset} COLLATE={$default_collation} ROW_FORMAT=DYNAMIC;";
            $DB->query($query) or die($DB->error());
        } else {
            $migration->addField($table, 'locations_id', 'integer');

            $migration->migrationOneTable($table);
        }
    }
}
