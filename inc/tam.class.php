<?php
/*
 -------------------------------------------------------------------------
 TAM plugin for GLPI
 Copyright (C) 2021-2023 by the TICgal Team.
 https://www.tic.gal
 -------------------------------------------------------------------------
 LICENSE
 This file is part of the TAM plugin.
 TAM plugin is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.
 TAM plugin is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with TAM. If not, see <http://www.gnu.org/licenses/>.
 --------------------------------------------------------------------------
 @package   TAM
 @author    the TICgal team
 @copyright Copyright (c) 2021-2023 TICgal team
 @license   AGPL License 3.0 or (at your option) any later version
            http://www.gnu.org/licenses/agpl-3.0-standalone.html
 @link      https://www.tic.gal
 @since     2021
 ----------------------------------------------------------------------
*/
class PluginTamTam extends CommonDBTM
{
    //public static $rightname='plugin_tam_active_timer';
    public const AUTO = 1;
    public const WEB = 2;
    public const ANDROID = 3;

    public static function getTypeName($nb = 0)
    {
        return __('TAM', 'tam');
    }

    public static function cronInfo($name)
    {
        switch ($name) {
            case 'stopClock':
                return ['description' => __('Stop forgotten timer', 'tam')];
        }
        return [];
    }

    public static function cronStopClock($task)
    {
        global $DB, $CFG_GLPI;

        $tam_table = self::getTable();
        $td_table = PluginTamDay::getTable();

        $query = [
            'SELECT' => [
                "$tam_table.id",
                'date_start',
                'users_id',
            ],
            'FROM' => $tam_table,
            'INNER JOIN' => [
                $td_table => [
                    'FKEY' => [
                        $tam_table => 'plugin_tam_days_id',
                        $td_table => 'id'
                    ]
                ]
            ],
            'WHERE' => [
                "$tam_table.date_start" => ['<', date("Y-m-d")],
                "$tam_table.date_end" => null,
            ]
        ];

        foreach ($DB->request($query) as $id => $row) {
            $plugin = new Plugin();
            if ($plugin->isActivated('actualtime')) {
                if (!PluginActualtimeTask::checkUserFree($row["users_id"])) {
                    $task_id = PluginActualtimeTask::getTask($row["users_id"]);
                    $actual_begin = PluginActualtimeTask::getActualBegin($task_id);
                    $seconds = (strtotime(date("Y-m-d H:i:s")) - strtotime($actual_begin));
                    $DB->update(
                        'glpi_plugin_actualtime_tasks',
                        [
                            'actual_end'        => date("Y-m-d H:i:s"),
                            'actual_actiontime' => $seconds,
                            'origin_end' => PluginActualtimetask::AUTO,
                            /*'latitude_end'=>$params['latitude'],
                            'longitude_end'=>$params['longitude'],*/
                        ],
                        [
                            'tasks_id' => $task_id,
                            [
                                'NOT' => ['actual_begin' => null],
                            ],
                            'actual_end' => null,
                        ]
                    );
                }
            }
            $date = date("Y-m-d", strtotime($row['date_start']));
            $closedate = date("Y-m-d H:i:s", strtotime($date . " 23:59:59"));
            $DB->update(
                $tam_table,
                [
                    'date_end' => $closedate,
                    'origin_end' => 1,
                    'status' => PluginTamStatus::FORGOTTEN
                ],
                [
                    'id' => $row["id"]
                ]
            );

            // * 1.5.0 Blockedlog
            PluginTamBlockedlog::createLog($row['id'], $row['users_id']);

            $DB->insert(
                'glpi_plugin_tam_forgots',
                [
                    'plugin_tam_tams_id' => $row["id"],
                    'name' => User::getFormURLWithID($row["users_id"]) . "-" . $date,
                ]
            );

            // * Create Ticket for correct TAM
            $tam_entity = new PluginTamEntity();
            $tam_entity->getFromDBByCrit(['entities_id' => $_SESSION['glpiactive_entity']]);
            if (isset($tam_entity->fields['create_ticket']) && $tam_entity->fields['create_ticket'] > 0) {
                PluginTamManagement::createTicket($row['id'], $row['users_id'], $tam_entity->fields['create_task'], $tam_entity->fields['tickettemplates_id']);
            }

            $task->addVolume(1);
            $task->log("<a href='" . User::getFormURLWithID($row["users_id"], false) . "'>" . sprintf(__("User %s forgot to stop the timer", "tam"), getUserName($row["users_id"])) . "</a>");
        }
        return true;
    }

    public function getTabNameForItem(CommonGLPI $item, $withtemplate = 0)
    {
        $profiles_id = $_SESSION['glpiactiveprofile']['id'];
        $effective_rights = ProfileRight::getProfileRights($profiles_id, ['plugin_tam_active_timer']);
        if (get_class($item) == 'Preference' && $effective_rights['plugin_tam_active_timer']) {
            return __("TAM", "tam");
        }
        return '';
    }

    public static function displayTabContentForItem(CommonGLPI $item, $tabnum = 1, $withtemplate = 0)
    {
        if (get_class($item) == 'Preference') {
            $pref = new self();
            $pref->showForm(Session::getLoginUserID());
        }
        return true;
    }

    public function showForm($ID, $options = [])
    {
        global $CFG_GLPI;

        $rand = mt_rand();

        $plugin = new Plugin();
        $actualtime = ($plugin->isActivated('actualtime')) ? "true" : "false";

        echo "<div class='center' style='margin-bottom: 20px;'>";

        if (!PluginTamLeave::checkLeave($ID)) {
            if (!$plugin->isActivated('gappextended')) {
                $timer_id = self::checkWorking($ID);
                if ($timer_id == 0) {
                    $button = "<input class='btn' type='button' id='tam_{$rand}' action='start' value='" . __('Start') . "' timer_id='0' class='x-button x-button-main' style='float:none;background-color:green;color:white'>";
                } else {
                    $button = "<input class='btn' type='button' id='tam_{$rand}' action='end' value='" . __('End') . "' timer_id='{$timer_id}' class='x-button x-button-main' style='float:none;background-color:red;color:white'>";
                }
                echo $button;
            } else {
                echo "<h1>" . __('You need to use the mobile application') . "</h1>";
            }
        }
        echo "</div>";
        $ajax_url = Plugin::getWebDir('tam') . '/ajax/timer.php';
        $actualtime_url = Plugin::getWebDir('tam') . '/ajax/actualtime.php';
        $url_dir = Plugin::getWebDir('tam');

        echo Html::css('public/lib/fullcalendar.css', ['media' => '']);
        echo Html::css('/lib/jqueryplugins/fullcalendar/fullcalendar.print.css', ['media' => 'print']);
        Html::requireJs('fullcalendar');
        foreach ($_SESSION['glpi_js_toload']['fullcalendar'] as $s) {
            echo Html::script($s);
        }
        self::showCalendar($rand, $ID);
    }

    public static function showCalendar($rand, $ID)
    {
        global $CFG_GLPI;

        $text_worked = __('Working time', 'tam');
        $help_text = __('Help');
        $print = __('Print', 'tam');
        $modal_help = self::createModal();
        echo $modal_help;
        $actual_date = date("Y-m-d H:i:s");
        echo "<div id='planning$rand'></div>";

        $pl_height = "function() {
			var _newheight = $(window).height() - 272;
			if ($('#debugajax').length > 0) {
				_newheight -= $('#debugajax').height();
			}
			//minimal size
			var _minheight = 300;
			if (_newheight < _minheight) {
				_newheight = _minheight;
			}
			return _newheight;
		}";
        if ($_SESSION['glpipage_layout'] == "vsplit") {
            $pl_height = "function() {
				var _newheight = $('.ui-tabs-panel').height() - 30;
				//minimal size
				var _minheight = 300;
				if (_newheight < _minheight) {
					_newheight = _minheight;
				}
				return _newheight;
			}";
        }

        $header = "{
			left:   'prev,next,today',
			center: 'title',
			right:  'dayGridMonth,timeGridWeek,timeGridDay,listYear'
		}";

        $url_dir = Plugin::getWebDir('tam');
        $ajax_url = Plugin::getWebDir('tam') . '/ajax/timer.php';
        $actualtime_url = Plugin::getWebDir('tam') . '/ajax/actualtime.php';
        $plugin = new Plugin();
        $actualtime = ($plugin->isActivated('actualtime')) ? "true" : "false";

        $script = <<<JAVASCRIPT

		$('#planning$rand').parent().append("<div id='div_print' class='center' style='margin-top:15px;'></div>");

	function formatDate(today){
		var dd = today.getDate();
		var mm = today.getMonth() + 1; //January is 0!

		var yyyy = today.getFullYear();
		if (dd < 10) {
		  dd = '0' + dd;
		} 
		if (mm < 10) {
		  mm = '0' + mm;
		} 
		return mm+'/'+dd+'/'+yyyy;
	}

	$(function() {
		$("#tam_{$rand}").click(function(event) {
			if({$actualtime}==true && $(this).attr('action')=="end"){
				checkActualtime($(this).attr('action'),$(this).attr('timer_id'));
			}else{
				tam_pressedButton($(this).attr('action'),$(this).attr('timer_id'));
			}
		});
		function tam_pressedButton(val,timer){
			jQuery.ajax({
				type: "POST",
				url: "{$ajax_url}",
				dataType: 'json',
				data: {action:val,timer_id:timer},
				success: function(result){
					$("#tam_{$rand}").attr('action',result['action']);
					$("#tam_{$rand}").attr('value',result['value']);
					$("#tam_{$rand}").css('background-color', result['color']);
					$("#tam_{$rand}").attr('timer_id',result['id']);
					calendar.refetchEvents();
					calendar.rerenderEvents();
				}
			});
		}

		function checkActualtime(val,timer){
			jQuery.ajax({
				type:"POST",
				url:"{$actualtime_url}",
				dataType:'json',
				data: {action:val,timer_id:timer},
				success:function(result){
					if (result['active']==false) {
						tam_pressedButton(val,timer);
					}else{
						displayAjaxMessageAfterRedirect();
					}
				}
			});
		}

		var disable_qtip = false,
		disable_edit = false;
		$('.planning_on_central a').mousedown(function() {
			disable_qtip = true;
			$('.qtip').hide();
		}).mouseup(function() {
			disable_qtip = false;
		});

		var window_focused = true;
		var loaded = false;
		var lastView;
		var lastDate;
		var lastDateDirty = false;
		window.onblur = function() { window_focused = false; }
		window.onfocus = function() { window_focused = true; }

		var initFCDatePicker = function(currentDate) {
			$('#planning_datepicker').flatpickr({
				defaultDate: currentDate,
				onChange: function(selected_date) {
					// convert to UTC to avoid timezone issues
					var date = new Date(
						Date.UTC(
							selected_date[0].getFullYear(),
							selected_date[0].getMonth(),
							selected_date[0].getDate()
						)
					);
					calendar.gotoDate(date);
				}
			});
		}
		var calendar=new FullCalendar.Calendar(document.getElementById('planning$rand'),{
			plugins: ['dayGrid', 'interaction', 'list', 'timeGrid', 'resourceTimeline', 'rrule'],
			height: {$pl_height},
			timeZone: 'UTC',
			theme: true,
			weekNumbers: false,
			defaultView: 'listYear',
			timeFormat:  'H:mm',
			eventLimit:  true,
			minTime:     '{$CFG_GLPI["planning_begin"]}',
			maxTime:     '{$CFG_GLPI["planning_end"]}',
			nowIndicator: true,
			now: "{$actual_date}",
			listDayAltFormat: false,
			agendaEventMinHeight: 13,
			header: {$header},
			views: {
				dayGridMonth: {
					titleFormat: {
						year:'numeric',
						month:'short',
						day:'numeric'
					},
					eventLimit:3,
					allDaySlot: false
				},
				timeGridWeek: {
					titleFormat: {
						year:'numeric',
						month:'short',
						day:'numeric'
					},
				},
				timeGridDay: {
					titleFormat: {
						year:'numeric',
						month:'short',
						day:'numeric'
					},
				},
				listYear:{
					titleFormat: {
						year:'numeric',
						month:'short',
						day:'numeric'
					},
				},
			},
			eventRender: function(info) {
				var event=info.event;
				var extProps = event.extendedProps;
				var element=$(info.el);
				var view =info.view;

				var eventtype_marker = '<span class=\"event_type\" style=\"background-color: '+extProps.typeColor+'\"></span>';
				element.find('.fc-content').after(eventtype_marker);
				element.find('.fc-list-item-title > a').prepend(eventtype_marker);

				var content = extProps.content;
				var tooltip = extProps.tooltip;
				if(view.type !== 'dayGridMonth' && view.type.indexOf('list') < 0 && !event.allDay){
						element.append('<div class=\"content\">'+content+'</div>');
				}

				// add classes to current event
				added_classes = '';
				if (typeof event.end !== 'undefined' && event.end !== null) {
					var now = new Date();
					var end = event.end;
					added_classes = end.getTime() < now.getTime() ? ' event_past'   : '';
					added_classes+= end.getTime() > now.getTime() ? ' event_future' : '';
					added_classes+= end.toDateString() === now.toDateString() ? ' event_today' : '';
				}
				if (extProps.state != '') {
					added_classes+= extProps.state == 0 ? ' event_info'
						: extProps.state == 1 ? ' event_todo'
						: extProps.state == 2 ? ' event_done'
						: '';
				}
				if (added_classes != '') {
					element.addClass(added_classes);
				}

				// add tooltip to event
				if (!disable_qtip) {
					var qtip_position = {
						viewport: 'auto'
					};
					if (view.type.indexOf('list') >= 0) {
						qtip_position.target= element.find('a');
					}
					element.qtip({
						position: qtip_position,
						content: tooltip,
						style: {
							classes: 'qtip-shadow qtip-bootstrap'
						},
						show: {
							solo: true,
							delay: 400
						},
						hide: {
							fixed: true,
							delay: 100
						},
						events: {
							show: function(event, api) {
								if(!window_focused) {
									event.preventDefault();
								}
							}
						}
					});
				}

				if (event.allDay && !event.extendedProps.diff) {
					var div=element.find('.fc-list-item-title > a').parent();
					var content="<div>Working day : "+event.extendedProps.active_time+"</div>";
					var divdialog="<div id='realtime'>"+content+"</div>";
					$(divdialog).val(content);

					div.append(divdialog);
				}
			},
			datesRender: function(info) {
				var view = info.view;
				// force refetch events from ajax on view change (don't refetch on firt load)
				if (loaded) {
					calendar.refetchEvents();
					calendar.rerenderEvents();
				}else{
					loaded=true;
				}
				$('#planning$rand .fc-toolbar .fc-center h2').after($('<i style=\"cursor: help;\" title=\"{$help_text}\" class=\"fa fa-question\" id=\"help_tam_{$rand}\"></i>')).after($('<i id=\"refresh_planning\" class=\"fa fa-sync pointer\"></i>')).after($('<div id=\"planning_datepicker\"><a data-toggle><i class=\"far fa-calendar-alt fa-lg pointer\"></i></a>'));
				// reinit datepicker
				$('#planning_datepicker').show();
				initFCDatePicker(new Date(view.currentStart));

				// show controls buttons
				$('#planning .fc-left .fc-button-group').show();

				var start=formatDate(new Date(view.currentStart));
				var end =formatDate(new Date(view.currentEnd));
				var button="<a href='{$url_dir}/front/pdf.form.php?id={$ID}&start="+start+"&end="+end+"' target='_blank' type='button' class='vsubmit'><i class=\'me-2 fas fa-file-pdf\'></i> {$print}</a>";
				$('#div_print').html(button);
			},
			viewSkeletonRender: function(info) {
				var view=info.view;
				var start=formatDate(new Date(view.currentStart));
				var end =formatDate(new Date(view.currentEnd));
				var button="<a href='{$url_dir}/front/pdf.form.php?id={$ID}&start="+start+"&end="+end+"' target='_blank' type='button' class='vsubmit'><i class=\'me-2 fas fa-file-pdf\'></i> {$print}</a>";
				$('#div_print').html(button);

				// set a var to force refetch events (see viewRender callback)
				loaded = true;

				// scroll div to first element needed to be viewed
				var scrolltoevent = $('#planning$rand .event_past.event_todo').first();
				if (scrolltoevent.length == 0) {
					scrolltoevent = $('#planning$rand .event_today').first();
				}
				if (scrolltoevent.length == 0) {
					scrolltoevent = $('#planning$rand .event_future').first();
				}
				if (scrolltoevent.length == 0) {
					scrolltoevent = $('#planning$rand .event_past').last();
				}
				if (scrolltoevent.length) {
					$('#planning$rand .fc-scroller').scrollTop(scrolltoevent.prop('offsetTop')-25);
				}
			},
			events:{
				url:  '{$url_dir}/ajax/planning.php',
				type: 'POST',
				extraParams: function() {
					var view_name = calendar ? calendar.state.viewType: 'listYear';
					var display_done_events = 1;
					if (view_name.indexOf('list') >= 0) {
						display_done_events = 0;
					}
					return {
						'action': 'get_events',
						'display_done_events': display_done_events,
						'view_name': view_name,
						'user':'{$ID}'
					};
				},
				success: function(data) {
					
				},
				failure: function(error) {
					console.log('there was an error while fetching events!',error);
				}
			},
		});
		var loadedLocales = Object.keys(FullCalendarLocales);
      if (loadedLocales.length === 1) {
         calendar.setOption('locale', loadedLocales[0]);
      }
		calendar.render();

		$('#refresh_planning').click(function() {
			calendar.refetchEvents();
			calendar.rerenderEvents();
		});
		
		initFCDatePicker();

		$("#help_tam_{$rand}").click(function(event) {
			$('#modal_help').remove().modal("show");
		});
	});
JAVASCRIPT;

        echo Html::scriptBlock($script);
    }

    public static function constructEventsArray($options = [])
    {
        global $DB;

        $begin = date("Y-m-d H:i:s", strtotime($options['start']));
        $end = date("Y-m-d H:i:s", strtotime($options['end']));
        $ID = $options['user'];

        $query = [
            'FROM' => self::getTable(),
            'INNER JOIN' => [
                'glpi_plugin_tam_days' => [
                    'FKEY' => [
                        self::getTable() => 'plugin_tam_days_id',
                        'glpi_plugin_tam_days' => 'id'
                    ]
                ]
            ],
            'LEFT JOIN' => [
                'glpi_plugin_tam_calendars' => [
                    'FKEY' => [
                        'glpi_plugin_tam_days' => 'plugin_tam_calendar_id',
                        'glpi_plugin_tam_calendars' => 'id'
                    ]
                ]
            ],
            'WHERE' => [
                'OR' => [
                    [
                        'AND' => [
                            'users_id' => $ID,
                            'date_start' => ['>=', $begin],
                            'date_end' => ['<=', $end]
                        ]
                    ],
                    [
                        'AND' => [
                            'users_id' => $ID,
                            'date_start' => ['>=', $begin],
                            'date_end' => null
                        ]
                    ],
                ],
                //'status' => [PluginTamStatus::CORRECT, PluginTamStatus::FIXED]
            ]
        ];

        $events = [];
        $total_time = [];
        $active_time = [];
        $break = [];
        foreach ($DB->request($query) as $id => $row) {
            if ($row['date_end'] == null) {
                $color = '#ffc966';
                $title = __('Working', 'tam');
                $seconds = strtotime(date("Y-m-d H:i:s")) - strtotime($row['date_start']);
            } else {
                $seconds = strtotime($row['date_end']) - strtotime($row['date_start']);
                if ($row['origin_end'] == self::AUTO) {
                    $color = '#ff6666';
                    $title = __('Forgotten timer', 'tam');
                } else {
                    $color = '#66ff66';
                    $title = __('Working time', 'tam');
                }
            }
            $content = HTML::timestampToString($seconds, false);
            $events[] = [
                'title'		  => $title,
                'content'     => $content,
                'start'       => $row['date_start'],
                'end'         => $row['date_end'],
                'editable'    => false,
                'color'       => $color,
                'borderColor' => '#0067ad',
                'textColor'   => 'black',
                'typeColor'   => '#ffffff'
            ];
            $key = date("Y-m-d", strtotime($row['date_start']));

            // Ignore forgotten timers for total time
            if($row['status'] != PluginTamStatus::FORGOTTEN) {
                if (array_key_exists($key, $total_time)) {
                    $total_time[$key] += $seconds;
                } else {
                    $total_time[$key] = $seconds;
                }
            } elseif (!array_key_exists($key, $total_time)) {
                $total_time[$key] = 0;
            }

            if (!array_key_exists($key, $active_time)) {
                $active_time[$key] = $row['active_time'];
            }
            if (!array_key_exists($key, $break)) {
                $break[$key] = $row['break'];
            }
        }

        foreach ($total_time as $key => $value) {
            if ($active_time[$key] > 0) {
                if ($break[$key] > 0) {
                    $title = HTML::timestampToString($value, false) . " (+" . HTML::timestampToString($break[$key], false) . ")";
                } else {
                    $title = HTML::timestampToString($value, false);
                }


                $diff = ($value + $break[$key]) - $active_time[$key];

                if ($diff < 0) {
                    $diff = abs($diff);
                    $color = 'green';
                } else {
                    $color = 'red';
                }

                $events[] = [
                    'title' => HTML::timestampToString($diff, false),
                    'allDay' => true,
                    'start' => $key,
                    'stick' => true,
                    'order' => 2,
                    'color' => $color,
                    'diff' => true
                ];
            } else {
                $title = HTML::timestampToString($value, false);

                $events[] = [
                    'title' => HTML::timestampToString($value, false),
                    'allDay' => true,
                    'start' => $key,
                    'stick' => true,
                    'order' => 2,
                    'color' => 'magenta',
                    'diff' => true
                ];
            }

            $events[] = [
                'title' => $title,
                'allDay' => true,
                'start' => $key,
                'stick' => true,
                'order' => 1,
                'active_time' => HTML::timestampToString($active_time[$key], false),
                'diff' => false
            ];
        }

        $query = [
            'FROM' => PluginTamLeave::getTable(),
            'WHERE' => [
                'users_id' => $ID,
                'date_start' => ['<=', $end],
                'date_end' => ['>=', $begin],
                'is_deleted' => 0
            ]
        ];
        foreach ($DB->request($query) as $id => $row) {
            $tmp = [
                'title'		  => $row['name'],
                'content'     => $row['name'],
                'start'       => date("Y-m-d 00:00:00", strtotime($row['date_start'])),
                'end'         => date("Y-m-d 23:59:59", strtotime($row['date_end'])),
                'editable'    => false,
                'color'       => 'silver',
                'borderColor' => '#0067ad',
                'textColor'   => 'black',
                'typeColor'   => '#ffffff'
            ];
            $admin = new PluginTamLeave();
            if ($admin->can($row['id'], UPDATE)) {
                $tmp['url'] = PluginTamLeave::getFormURLWithID($row['id']);
            }
            $events[] = $tmp;
        }

        $holidays = PluginTamHoliday::getHolidays($ID, $_SESSION["glpiactive_entity"]);
        foreach ($holidays as $key => $value) {
            if ($value['is_perpetual']) {
                for ($i = 0; $i <= 10; $i++) {
                    $year = (date("Y") - 5) + $i;
                    $hb = date('m-d', strtotime($value['begin']));
                    $he = date('m-d', strtotime($value['end']));
                    $beginhol = $year . '-' . $hb . ' 00:00:00';
                    $endhol = $year . '-' . $he . ' 23:59:59';

                    if (($begin < $beginhol && $beginhol < $end) || ($begin < $endhol && $endhol < $end)) {
                        $interv['editable'] = false;
                        $interv['color'] = 'SlateGray';
                        $interv['title'] = $value['name'];
                        $interv['content'] = $value['comment'];
                        $interv['start'] = date("Y-m-d 00:00:00", strtotime($beginhol));
                        $interv['end'] = date("Y-m-d 23:59:59", strtotime($endhol));
                        $events[] = $interv;
                    }
                }
            } else {
                $value['editable'] = false;
                $value['color'] = 'SlateGray';
                $value['title'] = $value['name'];
                $value['content'] = $value['comment'];
                $value['start'] = date("Y-m-d 00:00:00", strtotime($value['begin']));
                $value['end'] = date("Y-m-d 23:59:59", strtotime($value['end']));
                $events[] = $value;
            }
        }

        return $events;
    }

    public static function constructPDFEvents($options = [])
    {
        global $DB;

        $begin = date("Y-m-d H:i:s", strtotime($options['start']));
        $end = date("Y-m-d H:i:s", strtotime($options['end']));
        $ID = $options['id'];

        $query = [
            'FROM' => self::getTable(),
            'INNER JOIN' => [
                'glpi_plugin_tam_days' => [
                    'FKEY' => [
                        self::getTable() => 'plugin_tam_days_id',
                        'glpi_plugin_tam_days' => 'id'
                    ]
                ]
            ],
            'LEFT JOIN' => [
                'glpi_plugin_tam_calendars' => [
                    'FKEY' => [
                        'glpi_plugin_tam_days' => 'plugin_tam_calendar_id',
                        'glpi_plugin_tam_calendars' => 'id'
                    ]
                ],
                'glpi_plugin_tam_forgots' => [
                    'FKEY' => [
                        'glpi_plugin_tam_forgots' => 'plugin_tam_tams_id',
                        self::getTable() => 'id'
                    ]
                ]
            ],
            'WHERE' => [
                'users_id' => $ID,
                'date_start' => ['>=', $begin],
                'date_end' => ['<=', $end],
                'status' => [PluginTamStatus::CORRECT, PluginTamStatus::FIXED]
            ],
            'ORDER' => [
                'date_start ASC'
            ]
        ];

        $time = [];
        foreach ($DB->request($query) as $id => $row) {
            if ($row['date_end'] == null) {
                $seconds = strtotime(date("Y-m-d H:i:s")) - strtotime($row['date_start']);
            } else {
                $seconds = strtotime($row['date_end']) - strtotime($row['date_start']);
            }
            $key = date("Y-m-d", strtotime($row['date_start']));
            if (array_key_exists($key, $time)) {
                $time[$key]['total_time'] += $seconds;
            } else {
                $time[$key]['total_time'] = $seconds;
                $time[$key]['active_time'] = $row['active_time'];
                $time[$key]['break'] = $row['break'];
                $time[$key]['standard_hour'] = $row['standard_hour'];
            }
            if ($row['origin_end'] == self::AUTO) {
                $time[$key]['forgotten'] = true;
                if (isset($row['comment']) && $row['comment']) {
                    $time[$key]['comment'] = trim($row['comment']);
                }
            }
            $time[$key]['leave'] = false;
            $time[$key]['segment'][] = date("H:i", strtotime($row['date_start'])) . "-" . date("H:i", strtotime($row['date_end']));
        }

        $query = [
            'SELECT' => [
                'date_start',
                'date_end',
                'completename',
                'natural',
                'compensation',
                PluginTamLeave::getTable() . '.plugin_tam_leavetypes_id',
                'calendars_id'
            ],
            'FROM' => PluginTamLeave::getTable(),
            'INNER JOIN' => [
                'glpi_plugin_tam_leavetypes' => [
                    'FKEY' => [
                        PluginTamLeave::getTable() => 'plugin_tam_leavetypes_id',
                        'glpi_plugin_tam_leavetypes' => 'id'
                    ]
                ]
            ],
            'WHERE' => [
                'users_id' => $ID,
                'date_start' => ['<=', $end],
                'date_end' => ['>=', $begin],
                PluginTamLeave::getTable() . '.is_deleted' => 0
            ],
            'ORDER' => [
                'date_start ASC'
            ]
        ];

        foreach ($DB->request($query) as $id => $row) {
            $start = date("Y-m-d", strtotime($row['date_start']));
            $finish = date("Y-m-d", strtotime($row['date_end']));
            $key = $start . "<->" . $finish;
            $time[$key]['leave'] = true;
            $time[$key]['start'] = $start;
            $time[$key]['end'] = $finish;
            $time[$key]['name'] = $row['completename'];
            $time[$key]['natural'] = $row['natural'];
            $time[$key]['compensation'] = $row['compensation'];
            $time[$key]['type'] = $row['plugin_tam_leavetypes_id'];
            $time[$key]['calendar'] = $row['calendars_id'];
        }

        $user = new User();
        $user->getFromDB($ID);
        if (!$calendars_id = PluginTamUserCalendar::getCalendar($ID)) {
            $calendars_id = Entity::getUsedConfig('calendars_strategy', $user->fields["entities_id"], 'calendars_id', 0);
        }

        if ($calendars_id > 0) {
            $query = [
                'SELECT' => 'glpi_holidays.*',
                'FROM' => 'glpi_calendars_holidays',
                'INNER JOIN' => [
                    'glpi_holidays' => [
                        'FKEY' => [
                            'glpi_calendars_holidays' => 'holidays_id',
                            'glpi_holidays' => 'id'
                        ]
                    ]
                ],
                'WHERE' => [
                    'calendars_id' => $calendars_id,
                    'begin_date' => ['<=', $end],
                    'end_date' => ['>=', $begin],
                    'is_perpetual' => 0
                ]
            ];
            foreach ($DB->request($query) as $id => $row) {
                $start = date("Y-m-d", strtotime($row['begin_date']));
                $finish = date("Y-m-d", strtotime($row['end_date']));
                $key = $start . "<->" . $finish;
                $time[$key]['leave'] = false;
                $time[$key]['close_time'] = true;
                $time[$key]['start'] = $start;
                $time[$key]['end'] = $finish;
                $time[$key]['name'] = $row['name'];
                $time[$key]['natural'] = 1;
                $time[$key]['compensation'] = 0;
                $time[$key]['type'] = 0;
            }

            $query = [
                'SELECT' => 'glpi_holidays.*',
                'FROM' => 'glpi_calendars_holidays',
                'INNER JOIN' => [
                    'glpi_holidays' => [
                        'FKEY' => [
                            'glpi_calendars_holidays' => 'holidays_id',
                            'glpi_holidays' => 'id'
                        ]
                    ]
                ],
                'WHERE' => [
                    'calendars_id' => $calendars_id,
                    new QueryExpression("DATE_FORMAT(" . DBmysql::quoteName('begin_date') . ",'%m-%d') <= '" . date('m-d', strtotime($end . " -1day")) . "'"),
                    new QueryExpression("DATE_FORMAT(" . DBmysql::quoteName('end_date') . ",'%m-%d') >= '" . date('m-d', strtotime($begin)) . "'"),
                    'is_perpetual' => 1,
                ]
            ];
            foreach ($DB->request($query) as $id => $row) {
                $year = strftime("%Y");
                $start = date($year . "-m-d", strtotime($row['begin_date']));
                $finish = date($year . "-m-d", strtotime($row['end_date']));
                $key = $start . "<->" . $finish;
                $time[$key]['leave'] = false;
                $time[$key]['close_time'] = true;
                $time[$key]['start'] = $start;
                $time[$key]['end'] = $finish;
                $time[$key]['name'] = $row['name'];
                $time[$key]['natural'] = 1;
                $time[$key]['compensation'] = 0;
                $time[$key]['type'] = 0;
            }
        }

        $query = [
            'FROM' => PluginTamShift::getTable(),
            'WHERE' => [
                'users_id' => $ID,
                'NOT' => ['rrule' => '']
            ]
        ];
        foreach ($DB->request($query) as $id => $row) {
            $shift = new PluginTamShift();
            $ruledays = $shift->getDaysFromRrule($row['rrule'], $row['begin'], $end);
            foreach ($ruledays as $id => $value) {
                if ($value >= $begin) {
                    if ($value > $end) {
                        break;
                    }
                    $key = $value;
                    if (!isset($time[$key])) {
                        $time[$key]['leave'] = false;
                        $time[$key]['shift'] = true;
                        $time[$key]['name'] = trim($row['name']);
                        $time[$key]['active_time'] = strtotime($row['end']) - strtotime($row['begin']);
                    }
                }
            }
        }

        ksort($time);

        return $time;
    }

    public static function anualPDFEvents($options = [])
    {
        global $DB;

        $begin = date("Y-01-01 00:00:00", strtotime($options['start']));
        $end = date("Y-m-d H:i:s", strtotime($options['end']));
        $ID = $options['id'];

        $user = new User();
        $user->getFromDB($ID);
        if (!$calendars_id = PluginTamUserCalendar::getCalendar($ID)) {
            $calendars_id = Entity::getUsedConfig('calendars_strategy', $user->fields["entities_id"], 'calendars_id', 0);
        }

        $query = [
            'SELECT' => [
                'SUM' => [
                    'working_time AS total_work'
                ],
                'COUNT' => [
                    'glpi_plugin_tam_forgots.id AS total_forgot'
                ],
            ],
            'FROM' => self::getTable(),
            'INNER JOIN' => [
                'glpi_plugin_tam_days' => [
                    'FKEY' => [
                        self::getTable() => 'plugin_tam_days_id',
                        'glpi_plugin_tam_days' => 'id'
                    ]
                ]
            ],
            'LEFT JOIN' => [
                'glpi_plugin_tam_forgots' => [
                    'FKEY' => [
                        'glpi_plugin_tam_forgots' => 'plugin_tam_tams_id',
                        self::getTable() => 'id'
                    ]
                ]
            ],
            'WHERE' => [
                'users_id' => $ID,
                'date_start' => ['>=', $begin],
                'date_end' => ['<=', $end]
            ],
            'ORDER' => [
                'date_start ASC'
            ]
        ];

        $event = [];
        $req = $DB->request($query);
        if ($row = $req->current()) {
            $event = $row;
        }

        $query = [
            'SELECT' => [
                'SUM' => [
                    'active_time AS total_realwork',
                    'break AS total_break',
                    'standard_hour AS total_standard',
                ],
                'COUNT' => [
                    'date AS total_day',
                ],
            ],
            'FROM' => 'glpi_plugin_tam_days',
            'LEFT JOIN' => [
                'glpi_plugin_tam_calendars' => [
                    'FKEY' => [
                        'glpi_plugin_tam_days' => 'plugin_tam_calendar_id',
                        'glpi_plugin_tam_calendars' => 'id'
                    ]
                ],
            ],
            'WHERE' => [
                'users_id' => $ID,
                'date' => ['>=', $begin],
                'AND' => [
                    'date' => ['<=', $end],
                ],
            ],
        ];

        $req = $DB->request($query);
        if ($row = $req->current()) {
            $event['total_realwork'] = $row['total_realwork'];
            $event['total_break'] = $row['total_break'];
            $event['total_standard'] = $row['total_standard'];
            $event['total_day'] = $row['total_day'];
        }

        $event['total_leave'] = 0;
        $event['total_holiday'] = 0;
        $query = [
            'FROM' => PluginTamLeave::getTable(),
            'INNER JOIN' => [
                'glpi_plugin_tam_leavetypes' => [
                    'FKEY' => [
                        PluginTamLeave::getTable() => 'plugin_tam_leavetypes_id',
                        'glpi_plugin_tam_leavetypes' => 'id'
                    ]
                ]
            ],
            'WHERE' => [
                'users_id' => $ID,
                'date_start' => ['<=', $end],
                'date_end' => ['>=', $begin],
                PluginTamLeave::getTable() . '.is_deleted' => 0
            ],
            'ORDER' => [
                'date_start ASC'
            ]
        ];
        $req = $DB->request($query);
        if (count($req)) {
            foreach ($req as $row) {
                if (!$leavecalendar = $row['calendars_id']) {
                    $leavecalendar = $calendars_id;
                }
                $start = date("Y-m-d", strtotime($row['date_start']));
                $close = date("Y-m-d", strtotime($row['date_end']));
                if ($start < $begin) {
                    $start = $begin;
                }
                if ($close > date("Y-m-d", strtotime($options['end']))) {
                    $close = date("Y-m-d", strtotime($options['end']));
                }

                if ($row['natural']) {
                    $interval = date_diff(date_create($start), date_create($close));
                    if ($row['holiday']) {
                        $event['total_holiday'] += ($interval->format('%a') + 1);
                    }
                    $event['total_leave'] += ($interval->format('%a') + 1);
                    if ($row['compensation']) {
                        for ($i = $start; $i <= $close; $i = date('Y-m-d', strtotime($i . ' + 1 days'))) {
                            $event['total_realwork'] += PluginTamPdf::timeofDay($i, $leavecalendar);
                            if (PluginTamPdf::isWorkingDay($i, $leavecalendar)) {
                                $event['total_standard'] += PluginTamPdf::standardTime($leavecalendar);
                            }
                        }
                    }
                } else {
                    if ($start == $close) {
                        if (PluginTamPdf::isWorkingDay($start, $leavecalendar)) {
                            $event['total_leave'] += 1;
                            if ($row['holiday']) {
                                $event['total_holiday'] += 1;
                            }
                            if ($row['compensation']) {
                                $event['total_realwork'] += PluginTamPdf::timeofDay($start, $leavecalendar);
                                $event['total_standard'] += PluginTamPdf::standardTime($leavecalendar);
                            }
                        }
                    } else {
                        for ($i = $start; $i <= $close; $i = date('Y-m-d', strtotime($i . ' + 1 days'))) {
                            if (PluginTamPdf::isWorkingDay($i, $leavecalendar)) {
                                $event['total_leave'] += 1;
                                if ($row['holiday']) {
                                    $event['total_holiday'] += 1;
                                }
                                if ($row['compensation']) {
                                    $event['total_realwork'] += PluginTamPdf::timeofDay($i, $leavecalendar);
                                    $event['total_standard'] += PluginTamPdf::standardTime($leavecalendar);
                                }
                            }
                        }
                    }
                }
            }
        }

        if ($calendars_id > 0) {
            $query = [
                'SELECT' => 'glpi_holidays.*',
                'FROM' => 'glpi_calendars_holidays',
                'INNER JOIN' => [
                    'glpi_holidays' => [
                        'FKEY' => [
                            'glpi_calendars_holidays' => 'holidays_id',
                            'glpi_holidays' => 'id'
                        ]
                    ]
                ],
                'WHERE' => [
                    'calendars_id' => $calendars_id,
                    'begin_date' => ['<=', $end],
                    'end_date' => ['>=', $begin],
                    'is_perpetual' => 0
                ]
            ];
            $req = $DB->request($query);
            $event['total_close'] = count($req);

            $query = [
                'SELECT' => 'glpi_holidays.*',
                'FROM' => 'glpi_calendars_holidays',
                'INNER JOIN' => [
                    'glpi_holidays' => [
                        'FKEY' => [
                            'glpi_calendars_holidays' => 'holidays_id',
                            'glpi_holidays' => 'id'
                        ]
                    ]
                ],
                'WHERE' => [
                    'calendars_id' => $calendars_id,
                    new QueryExpression("DATE_FORMAT(" . DBmysql::quoteName('begin_date') . ",'%m-%d') <= '" . date('m-d', strtotime($end . " -1day")) . "'"),
                    new QueryExpression("DATE_FORMAT(" . DBmysql::quoteName('end_date') . ",'%m-%d') >= '" . date('m-d', strtotime($begin)) . "'"),
                    'is_perpetual' => 1,
                ]
            ];
            $req = $DB->request($query);
            $event['total_close'] += count($req);
        } else {
            $event['total_close'] = 0;
        }

        $event['total_shift'] = 0;
        $query = [
            'FROM' => PluginTamShift::getTable(),
            'WHERE' => [
                'users_id' => $ID,
                'NOT' => ['rrule' => '']
            ]
        ];
        foreach ($DB->request($query) as $id => $row) {
            $shift = new PluginTamShift();
            $ruledays = $shift->getDaysFromRrule($row['rrule'], $row['begin'], $end);
            foreach ($ruledays as $key => $value) {
                $event['total_shift'] += (strtotime($row['end']) - strtotime($row['begin']));
            }
        }

        return $event;
    }

    public static function checkWorking($id)
    {
        global $DB;

        $query = [
            'SELECT' => [
                self::getTable() . '.id',
            ],
            'FROM' => self::getTable(),
            'INNER JOIN' => [
                'glpi_plugin_tam_days' => [
                    'FKEY' => [
                        self::getTable() => 'plugin_tam_days_id',
                        'glpi_plugin_tam_days' => 'id'
                    ]
                ]
            ],
            'WHERE' => [
                'users_id' => $id,
                [
                    'NOT' => ['date_start' => null],
                ],
                'date_end' => null,
            ]
        ];

        $req = $DB->request($query);
        if ($row = $req->current()) {
            return $row['id'];
        } else {
            return 0;
        }
    }

    public static function createModal()
    {
        $modal = "<div class='modal fade show' id='modal_help'><div class='modal-dialog'><div class='modal-content'>";
        $modal .= "<div class='modal-header'>";
        $modal .= "<h4 class='modal-title'>".__('INFO')."</h4>";
        $modal .= "<button type='button' class='btn-close' data-bs-dismiss='modal' aria-label='".__('Close')."'></button>";
        $modal .= "</div>";
        $modal .= "<div class='modal-body'>";
        $modal .= "<table style='font-size:15px;'>";
        $modal .= "<tr>";
        $modal .= "<td>";
        $modal .= "<div style='background-color: red;width: 12px;height: 12px;'></div>";
        $modal .= "</td>";
        $modal .= "<td>" . __("Overworked hours", 'tam') . "</td>";
        $modal .= "</tr>";
        $modal .= "<tr>";
        $modal .= "<td>";
        $modal .= "<div style='background-color: green;width: 12px;height: 12px;'></div>";
        $modal .= "</td>";
        $modal .= "<td>" . __("Worked less hours", 'tam') . "</td>";
        $modal .= "</tr>";
        $modal .= "<tr>";
        $modal .= "<td>";
        $modal .= "<div style='background-color: magenta;width: 12px;height: 12px;'></div>";
        $modal .= "</td>";
        $modal .= "<td>" . __("After hours", 'tam') . "</td>";
        $modal .= "</tr>";
        $modal .= "<tr>";
        $modal .= "<td>";
        $modal .= "<div style='background-color: #ffc966;width: 12px;height: 12px;'></div>";
        $modal .= "</td>";
        $modal .= "<td>" . __("Timer running", 'tam') . "</td>";
        $modal .= "</tr>";
        $modal .= "<tr>";
        $modal .= "<td>";
        $modal .= "<div style='background-color: #ff6666;width: 12px;height: 12px;'></div>";
        $modal .= "</td>";
        $modal .= "<td>" . __("Forgotten timer", 'tam') . "</td>";
        $modal .= "</tr>";
        $modal .= "<tr>";
        $modal .= "<td>";
        $modal .= "<div style='background-color: #66ff66;width: 12px;height: 12px;'></div>";
        $modal .= "</td>";
        $modal .= "<td>" . __("Standard timer", 'tam') . "</td>";
        $modal .= "</tr>";
        $modal .= "<tr>";
        $modal .= "<td>";
        $modal .= "<div style='background-color: silver;width: 12px;height: 12px;'></div>";
        $modal .= "</td>";
        $modal .= "<td>" . __("Leaves", 'tam') . "</td>";
        $modal .= "</tr>";
        $modal .= "<tr>";
        $modal .= "<td>";
        $modal .= "<div style='background-color: SlateGray;width: 12px;height: 12px;'></div>";
        $modal .= "</td>";
        $modal .= "<td>" . __("Close times", 'tam') . "</td>";
        $modal .= "</tr>";
        $modal .= "</table></div>";
        $modal .= "</div></div></div>";

        return $modal;
    }

    /**
     * Get blacklist types
     *
     * @return array of types
     **/
    public static function getOrigin()
    {
        $options = [
            self::AUTO => __('Forgotten'),
            self::WEB => __('WEB'),
            self::ANDROID => __('ANDROID'),
        ];

        return $options;
    }

    public static function getTimeDate($date, $ID)
    {
        global $DB;

        $data = [];

        $query = [
            'SELECT' => [
                'SUM' => 'working_time AS total',
            ],
            'FROM' => self::getTable(),
            'INNER JOIN' => [
                'glpi_plugin_tam_days' => [
                    'FKEY' => [
                        self::getTable() => 'plugin_tam_days_id',
                        'glpi_plugin_tam_days' => 'id'
                    ]
                ]
            ],
            'WHERE' => [
                'users_id' => $ID,
                'date' => $date,
            ]
        ];

        if ($row = $DB->request($query)->current()) {
            if (is_null($row['total'])) {
                $data['total_worked'] = 0;
            } else {
                $data['total_worked'] = $row['total'];
            }
        } else {
            $data['total_worked'] = 0;
        }

        //Temporizadores activos
        $query = [
            'SELECT' => [
                'date_start',
            ],
            'FROM' => self::getTable(),
            'INNER JOIN' => [
                'glpi_plugin_tam_days' => [
                    'FKEY' => [
                        self::getTable() => 'plugin_tam_days_id',
                        'glpi_plugin_tam_days' => 'id'
                    ]
                ]
            ],
            'WHERE' => [
                'users_id' => $ID,
                'date' => $date,
                'date_end' => null
            ]
        ];
        if ($row = $DB->request($query)->current()) {
            $data['total_worked'] += (strtotime(date("Y-m-d H:i:s")) - strtotime($row['date_start']));
        }

        $query = [
            'SELECT' => [
                'break',
                'standard_hour',
                'active_time'
            ],
            'FROM' => 'glpi_plugin_tam_days',
            'LEFT JOIN' => [
                'glpi_plugin_tam_calendars' => [
                    'FKEY' => [
                        'glpi_plugin_tam_days' => 'plugin_tam_calendar_id',
                        'glpi_plugin_tam_calendars' => 'id'
                    ]
                ]
            ],
            'WHERE' => [
                'users_id' => $ID,
                'date' => $date,
            ]
        ];

        if ($row = $DB->request($query)->current()) {
            $data['break'] = $row['break'];
            $data['standard_hour'] = $row['standard_hour'];
            $data['active_time'] = $row['active_time'];
        } else {
            $data['break'] = 0;
            $data['standard_hour'] = 0;
            $data['active_time'] = 0;
        }

        return $data;
    }

    public static function install(Migration $migration)
    {
        global $DB;

        $default_charset = DBConnection::getDefaultCharset();
        $default_collation = DBConnection::getDefaultCollation();
        $default_key_sign = DBConnection::getDefaultPrimaryKeySignOption();

        $table = self::getTable();

        if (!$DB->tableExists($table)) {
            $migration->displayMessage("Installing $table");
            $query = "CREATE TABLE IF NOT EXISTS $table (
				`id` INT {$default_key_sign} NOT NULL auto_increment,
				`plugin_tam_days_id` INT {$default_key_sign} NOT NULL,
				`date_start` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
				`ip_start` varchar(255) DEFAULT NULL,
				`origin_start` INT {$default_key_sign} NOT NULL,
				`date_end` TIMESTAMP NULL DEFAULT NULL,
				`ip_end` varchar(255) DEFAULT NULL,
				`origin_end` INT {$default_key_sign} NOT NULL DEFAULT 1,
				`working_time` INT {$default_key_sign} NOT NULL DEFAULT 0,
				`status` INT {$default_key_sign} NOT NULL DEFAULT 0,
				`reason` VARCHAR(255) COLLATE {$default_collation} NULL DEFAULT NULL,
				`on_working_time` BOOLEAN DEFAULT 0,
				PRIMARY KEY (`id`)
			) ENGINE=InnoDB DEFAULT CHARSET={$default_charset} COLLATE={$default_collation} ROW_FORMAT=DYNAMIC;";
            $DB->query($query) or die($DB->error());
        } else {
            $migration->changeField($table, 'origin_end', 'origin_end', 'int', ['value' => 1]);
            $migration->changeField($table, 'working_time', 'working_time', 'int', ['value' => 0]);
            // * 1.5.0
            $migration->addField($table, 'status', 'int', ['value' => 0]);
            $migration->addField($table, 'reason', 'string');
            $migration->addField($table, 'on_working_time', 'boolean');
        }
    }
}
