<?php
/*
 -------------------------------------------------------------------------
 TAM plugin for GLPI
 Copyright (C) 2021-2023 by the TICgal Team.
 https://www.tic.gal
 -------------------------------------------------------------------------
 LICENSE
 This file is part of the TAM plugin.
 TAM plugin is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.
 TAM plugin is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with TAM. If not, see <http://www.gnu.org/licenses/>.
 --------------------------------------------------------------------------
 @package   TAM
 @author    the TICgal team
 @copyright Copyright (c) 2021-2023 TICgal team
 @license   AGPL License 3.0 or (at your option) any later version
            http://www.gnu.org/licenses/agpl-3.0-standalone.html
 @link      https://www.tic.gal
 @since     2021
 ----------------------------------------------------------------------
*/

use Glpi\Plugin\Hooks;
use Glpi\Application\View\TemplateRenderer;

class PluginTamLogin extends CommonGLPI
{
    public function showLogin()
    {
        global $DB, $CFG_GLPI, $PLUGIN_HOOKS;

        $redirect = '/plugins/tam/front/login.php';
        $errors = [];
        $default = [
            'card_bg_width'       => true,
            'lang'                => $CFG_GLPI["languages"][$_SESSION['glpilanguage']][3],
            'title'               => __('Authentication'),
            'noAuto'              => $_GET["noAUTO"] ?? 0,
            'index_page'          => $_SERVER['REQUEST_SCHEME'] .'://' . $_SERVER['HTTP_HOST'],
            'redirect'            => $redirect,
            'text_login'          => $CFG_GLPI['text_login'],
            'namfield'            => ($_SESSION['namfield'] = uniqid('fielda')),
            'pwdfield'            => ($_SESSION['pwdfield'] = uniqid('fieldb')),
            'rmbfield'            => ($_SESSION['rmbfield'] = uniqid('fieldc')),
            'show_lost_password'  => $CFG_GLPI["notifications_mailing"]
                                    && countElementsInTable('glpi_notifications', [
                                        'itemtype'  => 'User',
                                        'event'     => 'passwordforget',
                                        'is_active' => 1
                                    ]),
            'languages_dropdown'  => Dropdown::showLanguages('language', [
                'display'             => false,
                'display_emptychoice' => true,
                'emptylabel'          => __('Default (from user profile)'),
                'width'               => '100%'
            ]),
            'right_panel'         => strlen($CFG_GLPI['text_login']) > 0
                                    || count($PLUGIN_HOOKS[Hooks::DISPLAY_LOGIN] ?? []) > 0
                                    || $CFG_GLPI["use_public_faq"],
            'auth_dropdown_login' => Auth::dropdownLogin(false),
            'copyright_message'   => Html::getCopyrightMessage(false),
            'errors'              => $errors,
        ];

        $user_id = isset($_SESSION['glpiID']) ? $_SESSION['glpiID'] : -1;
        $username = '';
        $user_exist = false;

        if ($user_id != -1) {
            $user = new User();
            $user_exist = $user->getFromDB($user_id);
        }

        if (!$user_exist) {
            $template = '@tam/login.html.twig';
            TemplateRenderer::getInstance()->display($template, $default);
        } else {
            $rand = mt_rand();
            $tamAction = $this->checkTAM($user_id);
            $userName = $user->fields['name'];
            $reason = '';

            $reason_item = new PluginTamReason();
            $reason_item->getFromDBByCrit(['is_default' => 1]);
            if (isset($reason_item->fields['id'])) {
                $reason = $reason_item->fields['id'];
            }

            echo Html::css('public/lib/fullcalendar.css', ['media' => '']);
            echo Html::css('/lib/jqueryplugins/fullcalendar/fullcalendar.print.css', ['media' => 'print']);
            Html::requireJs('fullcalendar');
            foreach ($_SESSION['glpi_js_toload']['fullcalendar'] as $s) {
                echo Html::script($s);
            }

            $template = '@tam/tam.html.twig';
            $tamParams = $default;
            $tamParams['user'] = $userName;
            $tamParams['rand'] = $rand;
            $tamParams['tam_action'] = $tamAction['action'];
            $tamParams['tam_timer'] = $tamAction['timer_id'];
            $tamParams['default_reason'] = $reason;
            $tamParams['options'] = [];
            TemplateRenderer::getInstance()->display($template, $tamParams);
        }
    }

    public function checkTAM($ID)
    {
        $action = '';
        $timer_id = 0;

        if (!PluginTamLeave::checkLeave($ID)) {
            $timer_id = PluginTamTam::checkWorking($ID);
            if ($timer_id == 0) {
                $action = 'start';
            } else {
                $action = 'end';
            }
        }

        return ['action' => $action, 'timer_id' => $timer_id];
    }
    public function updateTAM($post_action, $post_timer_id)
    {
        global $DB;

        if (isset($post_action)) {
            switch ($post_action) {
                case 'start':
                    $id = PluginTamTam::checkWorking(Session::getLoginUserID());
                    if ($id > 0) {
                        $result = [
                            'action' => 'end',
                            'value' => __('End'),
                            'color' => 'red',
                            'id' => $id
                        ];
                    } else {
                        $query = [
                            'FROM' => 'glpi_plugin_tam_days',
                            'WHERE' => [
                                'date' => date("Y-m-d"),
                                'users_id' => Session::getLoginUserID()
                            ],
                        ];
                        $req = $DB->request($query);
                        if ($row = $req->current()) {
                            $day_id = $row['id'];
                        } else {
                            if (!$calendars_id = PluginTamUserCalendar::getCalendar(Session::getLoginUserID())) {
                                $calendars_id = Entity::getUsedConfig('calendars_strategy', $_SESSION['glpidefault_entity'], 'calendars_id', 0);
                            }
                            $break = new PluginTamCalendar();
                            $break->getFromDBByCalendar($calendars_id);
                            $calendar     = new Calendar();

                            $timeoftheday = 0;
                            if (($calendars_id > 0) && $calendar->getFromDB($calendars_id) && !$calendar->isHoliday(date("Y-m-d"))) {
                                $dayweak = $calendar->getDayNumberInWeek(strtotime(date("Y-m-d")));
                                $timeoftheday = CalendarSegment::getActiveTimeBetween($calendars_id, $dayweak, '00:00:00', '23:59:59');
                            }
                            $break_id = $break->getID();

                            $DB->insert(
                                'glpi_plugin_tam_days',
                                [
                                    'users_id' => Session::getLoginUserID(),
                                    'active_time' => $timeoftheday,
                                    'plugin_tam_calendar_id' => $break_id,
                                    'date' => date("Y-m-d"),
                                ]
                            );
                            $day_id = $DB->insertId();
                        }
                        $ip = getenv("HTTP_X_FORWARDED_FOR") ? Sanitizer::encodeHtmlSpecialChars(getenv("HTTP_X_FORWARDED_FOR")) : getenv("REMOTE_ADDR");

                        $DB->insert(
                            'glpi_plugin_tam_tams',
                            [
                                'plugin_tam_days_id' => $day_id,
                                'ip_start' => $ip,
                                'origin_start' => PluginTamTam::WEB,
                            ]
                        );

                        $id = $DB->insertId();

                        $result = [
                            'action' => 'end',
                            'value' => __('End'),
                            'color' => 'red',
                            'id' => $id
                        ];
                    }
                    echo json_encode($result);
                    break;
                case 'end':

                    $id = PluginTamTam::checkWorking(Session::getLoginUserID());
                    if ($id <= 0) {
                        $result = [
                            'action' => 'start',
                            'value' => __('Start'),
                            'color' => 'green',
                            'id' => 0
                        ];
                    } else {
                        $date = date("Y-m-d H:i:s");

                        $query = [
                            'SELECT' => 'date_start',
                            'FROM' => 'glpi_plugin_tam_tams',
                            'WHERE' => [
                                'id' => $post_timer_id
                            ]
                        ];

                        $req = $DB->request($query);
                        $row = $req->current();
                        $actiontime = strtotime($date) - strtotime($row['date_start']);

                        $ip = getenv("HTTP_X_FORWARDED_FOR") ? Sanitizer::encodeHtmlSpecialChars(getenv("HTTP_X_FORWARDED_FOR")) : getenv("REMOTE_ADDR");

                        // * 1.5.0 Reason for end tam
                        $reason = '';
                        $owt = 0;
                        if (isset($_POST['reason'])) {
                            $reason_item = new PluginTamReason();
                            $reason_item->getFromDB($_POST['reason']);
                            $reason = $reason_item->fields['code'].'-'.$reason_item->fields['name'];
                            $owt = $reason_item->fields['on_working_time'];
                        }

                        $DB->update(
                            'glpi_plugin_tam_tams',
                            [
                                'date_end' => date("Y-m-d H:i:s"),
                                'working_time' => $actiontime,
                                'ip_end' => $ip,
                                'origin_end' => PluginTamTam::WEB,
                                'reason' => $reason,
                                'on_working_time' => $owt
                            ],
                            [
                                'id' => $post_timer_id
                            ]
                        );

                        // * 1.5.0 Blockedlog
                        PluginTamBlockedlog::createLog($post_timer_id, Session::getLoginUserID());

                        $result = [
                            'action' => 'start',
                            'value' => __('Start'),
                            'color' => 'green',
                            'id' => 0
                        ];

                        // * If reason is checked as 'on_working_time' reopen tam
                        if ($owt) {
                            $this->updateTAM('start', $post_timer_id);
                        }
                    }
                    echo json_encode($result);
                    break;
            }
        }
    }
}
