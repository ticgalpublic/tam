<?php
/*
 -------------------------------------------------------------------------
 TAM plugin for GLPI
 Copyright (C) 2021-2023 by the TICgal Team.
 https://www.tic.gal
 -------------------------------------------------------------------------
 LICENSE
 This file is part of the TAM plugin.
 TAM plugin is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.
 TAM plugin is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with TAM. If not, see <http://www.gnu.org/licenses/>.
 --------------------------------------------------------------------------
 @package   TAM
 @author    the TICgal team
 @copyright Copyright (c) 2021-2023 TICgal team
 @license   AGPL License 3.0 or (at your option) any later version
            http://www.gnu.org/licenses/agpl-3.0-standalone.html
 @link      https://www.tic.gal
 @since     2021
 ----------------------------------------------------------------------
*/
class PluginTamDay extends CommonDBTM
{
    public const WORKPLACE = 1;
    public const HOME = 2;
    public const REMOTE = 3;

    public static function getWorktype()
    {

        $options = [
            self::WORKPLACE => __('Assigned Workplace', 'tam'),
            self::HOME => __('Work from home', 'tam'),
            self::REMOTE => __('Remote working', 'tam'),
        ];

        return $options;
    }

    public static function getWorktypeName($value)
    {

        switch ($value) {
            case self::WORKPLACE:
                return __('Assigned Workplace', 'tam');
                break;

            case self::HOME:
                return __('Work from home', 'tam');
                break;

            case self::REMOTE:
                return __('Remote working', 'tam');
                break;

            default:
                return $value;
                break;
        }
    }

    public static function dropdownWorktype($name, $options = [])
    {

        $params = [
            'value'     => 0,
            'toadd'     => [],
            'on_change' => '',
            'display'   => true,
        ];

        if (is_array($options) && count($options)) {
            foreach ($options as $key => $val) {
                $params[$key] = $val;
            }
        }

        $items = [];
        if (count($params['toadd']) > 0) {
            $items = $params['toadd'];
        }

        $items += self::getWorktype();

        return Dropdown::showFromArray($name, $items, $params);
    }

    public static function install(Migration $migration)
    {
        global $DB;

        $default_charset = DBConnection::getDefaultCharset();
        $default_collation = DBConnection::getDefaultCollation();
        $default_key_sign = DBConnection::getDefaultPrimaryKeySignOption();

        $table = self::getTable();

        if (!$DB->tableExists($table)) {
            $migration->displayMessage("Installing $table");
            $query = "CREATE TABLE IF NOT EXISTS $table (
				`id` INT {$default_key_sign} NOT NULL auto_increment,
				`users_id` INT {$default_key_sign} NOT NULL,
				`date` DATE NOT NULL,
				`active_time` INT {$default_key_sign} NOT NULL,
				`plugin_tam_calendar_id` INT {$default_key_sign} NOT NULL,
				`worktype` INT {$default_key_sign} NOT NULL DEFAULT '99',
				PRIMARY KEY (`id`)
			) ENGINE=InnoDB  DEFAULT CHARSET={$default_charset} COLLATE={$default_collation} ROW_FORMAT=DYNAMIC;";
            $DB->query($query) or die($DB->error());
        } else {
            $migration->addField($table, 'worktype', 'integer', ['value' => 99]);

            $migration->migrationOneTable($table);
        }
    }
}
