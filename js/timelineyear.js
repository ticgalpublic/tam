/*
 -------------------------------------------------------------------------
 TAM plugin for GLPI
 Copyright (C) 2021-2023 by the TICgal Team.
 https://www.tic.gal
 -------------------------------------------------------------------------
 LICENSE
 This file is part of the TAM plugin.
 TAM plugin is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.
 TAM plugin is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with TAM. If not, see <http://www.gnu.org/licenses/>.
 --------------------------------------------------------------------------
 @package   TAM
 @author    the TICgal team
 @copyright Copyright (c) 2021-2023 TICgal team
 @license   AGPL License 3.0 or (at your option) any later version
			http://www.gnu.org/licenses/agpl-3.0-standalone.html
 @link      https://www.tic.gal
 @since     2021
 ----------------------------------------------------------------------
*/
$(document).ready(function () {

	function waitforCalendar() {
		if (GLPIPlanning.calendar === null) {
			setTimeout(waitforCalendar, 250);
		} else {
			changeCalendar();
		}
	}

	function changeCalendar() {
		var header = GLPIPlanning.calendar.getOption('header');
		header.right += ', resourceYear';
		GLPIPlanning.calendar.setOption('header', header);
		var views = GLPIPlanning.calendar.getOption('views');
		views['resourceYear'] = {
			type: 'resourceTimeline',
			buttonText: 'Timeline Year',
			duration: { years: 1 },
			groupByDateAndResource: true
		};
		GLPIPlanning.calendar.setOption('views', views);
		var originsource = GLPIPlanning.calendar.getEventSources()[0];
		var source = function (fetchInfo, successCallback, failureCallback) {
			if (GLPIPlanning.calendar.state.viewType == 'resourceYear') {
				$.ajax({
					type: 'POST',
					url: CFG_GLPI.root_doc + "/ajax/planning.php",
					data: {
						action: 'get_events',
						display_done_events: 1,
						view_name: view_name,
						force_all_events: true,
						start: fetchInfo.startStr,
						end: fetchInfo.endStr,
						timeZone: fetchInfo.timeZone
					},
					success: function (data) {
						successCallback(data);
					},
					error: function (error) {
						failureCallback(error);
					}
				});
			} else {
				var view_name = GLPIPlanning.calendar.state.viewType;
				var display_done_events = 1;
				if (view_name.indexOf('list') >= 0) {
					display_done_events = 0;
				}
				$.ajax({
					type: 'POST',
					url: CFG_GLPI.root_doc + "/ajax/planning.php",
					data: {
						action: 'get_events',
						display_done_events: display_done_events,
						view_name: view_name,
						start: fetchInfo.startStr,
						end: fetchInfo.endStr,
						timeZone: fetchInfo.timeZone
					},
					success: function (data) {
						successCallback(data);
					},
					error: function (error) {
						failureCallback(error);
					}
				});
			}
		};
		GLPIPlanning.calendar.addEventSource(source);
		originsource.remove();
		GLPIPlanning.refresh();
	}
	waitforCalendar();
});