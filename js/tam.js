/*
 -------------------------------------------------------------------------
 TAM plugin for GLPI
 Copyright (C) 2021-2023 by the TICgal Team.
 https://www.tic.gal
 -------------------------------------------------------------------------
 LICENSE
 This file is part of the TAM plugin.
 TAM plugin is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.
 TAM plugin is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with TAM. If not, see <http://www.gnu.org/licenses/>.
 --------------------------------------------------------------------------
 @package   TAM
 @author    the TICgal team
 @copyright Copyright (c) 2021-2023 TICgal team
 @license   AGPL License 3.0 or (at your option) any later version
            http://www.gnu.org/licenses/agpl-3.0-standalone.html
 @link      https://www.tic.gal
 @since     2021
 ----------------------------------------------------------------------
*/
var ajax_url = CFG_GLPI.root_doc + "/" + GLPI_PLUGINS_PATH.tam + "/ajax/tam.php";
$(document).ready(function () {

   if ($("#tam_mess").length) {
      $("#tam_mess").remove();
   }

   jQuery.ajax({
      type: 'POST',
      url: ajax_url,
      dataType: 'json',
      data: { check: true },
      success: function (result) {
         if (result['class']) {

            var html = `<div class="toast-container top-0 end-0 p-3 messages_after_redirect" id="tam_mess">
            <div class="toast animate__animated animate__tada animate__delay-2s animate__slow" role="alert" aria-live="assertive" aria-atomic="true">
               <div class="toast-header `+ result['class'] + ` ">
                  <strong class="me-auto">`+ result['title'] + `</strong>
                  <button type="button" class="btn-close" data-bs-dismiss="toast" aria-label="Close"></button>
               </div>
               <div class="toast-body">
                  `+ result['mensage'] + `
               </div>
            </div>
            </div>`;

            $("body").append(html);
            var toast = new bootstrap.Toast(document.querySelector('#tam_mess .toast:not(.show)'), { autohide: false })
            toast.show()
         }
      }
   });
});