# TAM
Time & Attendance Management plugin for GLPI
## Features

* Check-in and Check-off work
* Leaves Management
* Shifts Management
* Gapp Pro & Enterprise integration through Gapp eXtended
* New Planning Events:
    * Close times (Bank Holidays) 
    * Leaves
    * Shifts
* Per user monthly report
* Actualtime