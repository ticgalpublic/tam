<?php
/*
 -------------------------------------------------------------------------
 TAM plugin for GLPI
 Copyright (C) 2021-2023 by the TICgal Team.
 https://www.tic.gal
 -------------------------------------------------------------------------
 LICENSE
 This file is part of the TAM plugin.
 TAM plugin is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.
 TAM plugin is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with TAM. If not, see <http://www.gnu.org/licenses/>.
 --------------------------------------------------------------------------
 @package   TAM
 @author    the TICgal team
 @copyright Copyright (c) 2021-2023 TICgal team
 @license   AGPL License 3.0 or (at your option) any later version
            http://www.gnu.org/licenses/agpl-3.0-standalone.html
 @link      https://www.tic.gal
 @since     2021
 ----------------------------------------------------------------------
*/
$servername = "";
$username = "";
$password = "";
$dbname = "";
$file_error="mover-item-error.log";
$file_result="mover-item-result.log";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

$dates=[];
$sql_dates="select CONVERT(date_start,DATE) as date,users_id,sum(active_time) as active_time,sum(plugin_tam_break_id) as break from glpi_plugin_tam_tams group by CONVERT(date_start,DATE),users_id";
$result_dates=$conn->query($sql_dates);

if ($result_dates->num_rows>0) {
    while ($row_data=$result_dates->fetch_assoc()) {
        $sql_ins_date="INSERT INTO glpi_plugin_tam_days (users_id,date,active_time,plugin_tam_break_id) VALUES ('".$row_data['users_id']."','".$row_data['date']."','".$row_data['active_time']."','".$row_data['break']."')";
        if($conn->query($sql_ins_date)===true) {
            $last_id=$conn->insertId();
            $sql_update="UPDATE glpi_plugin_tam_tams SET plugin_tam_days_id=".$last_id." WHERE CONVERT(date_start,DATE)='".$row_data['date']."' and users_id=".$row_data['users_id'];
            file_put_contents($file_result, "New date created successfully ".$last_id."\xA", FILE_APPEND);
            if ($conn->query($sql_update)===true) {
                file_put_contents($file_result, "ID dia ".$last_id." añadido al dia ".$row_data['date']." y usuario ".$row_data['users_id']." \xA", FILE_APPEND);
            } else {
                file_put_contents($file_error, "Error in: " . $sql_update . " => " . $conn->error."\xA", FILE_APPEND);
            }
        } else {
            file_put_contents($file_error, "Error in: " . $sql_ins_date . " => " . $conn->error."\xA", FILE_APPEND);
        }
    }
}
