<?php
/*
 -------------------------------------------------------------------------
 TAM plugin for GLPI
 Copyright (C) 2021-2023 by the TICgal Team.
 https://www.tic.gal
 -------------------------------------------------------------------------
 LICENSE
 This file is part of the TAM plugin.
 TAM plugin is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.
 TAM plugin is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with TAM. If not, see <http://www.gnu.org/licenses/>.
 --------------------------------------------------------------------------
 @package   TAM
 @author    the TICgal team
 @copyright Copyright (c) 2021-2023 TICgal team
 @license   AGPL License 3.0 or (at your option) any later version
            http://www.gnu.org/licenses/agpl-3.0-standalone.html
 @link      https://www.tic.gal
 @since     2021
 ----------------------------------------------------------------------
*/

use Glpi\Event;

require_once('../../../inc/includes.php');

$plugin = new Plugin();
if (!$plugin->isInstalled('tam') || !$plugin->isActivated('tam')) {
    Html::displayNotFoundError();
}

Session::checkRight("plugin_tam_shift", READ);

if (empty($_GET["id"])) {
    $_GET["id"] = "";
}

$shift = new PluginTamShift();

if (isset($_POST["add"])) {
    $shift->check(-1, CREATE, $_POST);

    if ($newID = $shift->add($_POST)) {
        if ($_SESSION['glpibackcreated']) {
            Html::redirect($shift->getLinkURL());
        }
    }
    Html::back();
} elseif (isset($_POST["delete"])) {
    $shift->check($_POST["id"], DELETE);
    $shift->delete($_POST);
    $shift->redirectToList();
} elseif (isset($_POST["restore"])) {
    $shift->check($_POST["id"], DELETE);
    $shift->restore($_POST);
    $shift->redirectToList();
} elseif (isset($_POST["purge"])) {
    $shift->check($_POST["id"], PURGE);
    $shift->delete($_POST, 1);
    $shift->redirectToList();
} elseif (isset($_POST["purge_instance"])) {
    $shift->check($_POST["id"], PURGE);
    $shift->deleteInstance((int) $_POST["id"], $_POST['day']);
    $shift->redirectToList();
} elseif (isset($_POST["update"])) {
    $shift->check($_POST["id"], UPDATE);
    $shift->update($_POST);
    Html::back();
} else {
    Html::header(
        PluginTamShift::getTypeName(Session::getPluralNumber()),
        $_SERVER['PHP_SELF'],
        "helpdesk",
        "planning",
        "plugintamshift"
    );
    $shift->display(['id' => $_GET["id"]]);
    Html::footer();
}
