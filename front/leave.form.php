<?php
/*
 -------------------------------------------------------------------------
 TAM plugin for GLPI
 Copyright (C) 2021-2023 by the TICgal Team.
 https://www.tic.gal
 -------------------------------------------------------------------------
 LICENSE
 This file is part of the TAM plugin.
 TAM plugin is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.
 TAM plugin is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with TAM. If not, see <http://www.gnu.org/licenses/>.
 --------------------------------------------------------------------------
 @package   TAM
 @author    the TICgal team
 @copyright Copyright (c) 2021-2023 TICgal team
 @license   AGPL License 3.0 or (at your option) any later version
            http://www.gnu.org/licenses/agpl-3.0-standalone.html
 @link      https://www.tic.gal
 @since     2021
 ----------------------------------------------------------------------
*/
include("../../../inc/includes.php");

$plugin = new Plugin();
if (!$plugin->isInstalled('tam') || !$plugin->isActivated('tam')) {
    Html::displayNotFoundError();
}

if (!isset($_GET["id"])) {
    $_GET["id"] = "";
}
if (!isset($_GET["withtemplate"])) {
    $_GET["withtemplate"] = "";
}

$tam = new PluginTamLeave();

if (isset($_POST['add'])) {

    //Check CREATE
    $tam->check(-1, CREATE, $_POST);
    //Do object creation
    $newID = $tam->add($_POST);
    //Redirect to newly created object form
    if ($_SESSION['glpibackcreated']) {
        Html::redirect($tam->getFormURL() . "?id=" . $newID);
    }
    Html::back();
} elseif (isset($_POST['delete'])) {

    //Check DELETE
    $tam->check($_POST['id'], DELETE);
    //Put object in dustbin
    $tam->delete($_POST);
    //Redirect to objects list
    $tam->redirectToList();
} elseif (isset($_POST["restore"])) {

    $tam->check($_POST['id'], PURGE);
    $tam->restore($_POST);
    $tam->redirectToList();
} elseif (isset($_POST['purge'])) {

    //Check PURGE ACL
    $tam->check($_POST['id'], PURGE);
    //Do object purge
    $tam->delete($_POST, 1);
    //Redirect to objects list
    $tam->redirectToList();
} elseif (isset($_POST['update'])) {

    //Check UPDATE
    $tam->check($_POST['id'], UPDATE);
    //Do object update
    $tam->update($_POST);
    //Redirect to object form
    Html::back();
} else {

    $tam->checkGlobal(READ);

    Html::header(PluginTamLeave::getTypeName(2), $_SERVER['PHP_SELF'], 'management', 'plugintammenu', 'leave');
    $tam->display($_GET);
    Html::footer();
}
