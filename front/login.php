<?php
/*
 -------------------------------------------------------------------------
 TAM plugin for GLPI
 Copyright (C) 2021-2023 by the TICgal Team.
 https://www.tic.gal
 -------------------------------------------------------------------------
 LICENSE
 This file is part of the TAM plugin.
 TAM plugin is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.
 TAM plugin is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with TAM. If not, see <http://www.gnu.org/licenses/>.
 --------------------------------------------------------------------------
 @package   TAM
 @author    the TICgal team
 @copyright Copyright (c) 2021-2023 TICgal team
 @license   AGPL License 3.0 or (at your option) any later version
            http://www.gnu.org/licenses/agpl-3.0-standalone.html
 @link      https://www.tic.gal
 @since     2021
 ----------------------------------------------------------------------
*/
use Glpi\Application\View\TemplateRenderer;
use Glpi\Toolbox\Sanitizer;

include("../../../inc/includes.php");

$plugin = new Plugin();
if (!$plugin->isInstalled('tam') || !$plugin->isActivated('tam')) {
    Html::displayNotFoundError();
}

$tam = new PluginTamLogin();
if (isset($_POST['action']) && isset($_POST['timer_id'])) {
    $tam->updateTAM($_POST['action'], $_POST['timer_id']);
}

if (isset($_POST['TamLogout'])) {
    // * Logout
    if (
        !isset($_SESSION["noAUTO"])
        && isset($_SESSION["glpiauthtype"])
        && $_SESSION["glpiauthtype"] == Auth::CAS
        && Toolbox::canUseCAS()
    ) {
        if (version_compare(phpCAS::getVersion(), '1.6.0', '<')) {
            // Prior to version 1.6.0, 5th argument was `$changeSessionID`.
            phpCAS::client(
                constant($CFG_GLPI["cas_version"]),
                $CFG_GLPI["cas_host"],
                intval($CFG_GLPI["cas_port"]),
                $CFG_GLPI["cas_uri"],
                false
            );
        } else {
            // Starting from version 1.6.0, 5th argument is `$service_base_url`.
            phpCAS::client(
                constant($CFG_GLPI["cas_version"]),
                $CFG_GLPI["cas_host"],
                intval($CFG_GLPI["cas_port"]),
                $CFG_GLPI["cas_uri"],
                $CFG_GLPI["url_base"],
                false
            );
        }
        phpCAS::setServerLogoutURL(strval($CFG_GLPI["cas_logout"]));
        phpCAS::logout();
    }

    Session::cleanOnLogout();

    $_SESSION["glpicookietest"] = 'testcookie';

    if (isset($_POST['action']) && $_POST['action'] == 'start') {
        Session::addMessageAfterRedirect(__('Starting TAM', 'tam'), false, INFO);
    } else {
        Session::addMessageAfterRedirect(__('Ending TAM', 'tam'), false, INFO);
    }

    Html::redirect($_SERVER['PHP_SELF']);
} elseif (isset($_POST['TamLogin'])) {
    // * Login
    if (!isset($_SESSION["glpicookietest"]) || ($_SESSION["glpicookietest"] != 'testcookie')) {
        if (!is_writable(GLPI_SESSION_DIR)) {
            Html::redirect($_SERVER['PHP_SELF'] . "?error=2");
        } else {
            Html::redirect($_SERVER['PHP_SELF'] . "?error=1");
        }
    }

    $_POST = array_map('stripslashes', $_POST);


    //Do login and checks
    //$user_present = 1;
    if (isset($_SESSION['namfield']) && isset($_POST[$_SESSION['namfield']])) {
        $login = $_POST[$_SESSION['namfield']];
    } else {
        $login = '';
    }
    if (isset($_SESSION['pwdfield']) && isset($_POST[$_SESSION['pwdfield']])) {
        $password = Sanitizer::unsanitize($_POST[$_SESSION['pwdfield']]);
    } else {
        $password = '';
    }
    // Manage the selection of the auth source (local, LDAP id, MAIL id)
    if (isset($_POST['auth'])) {
        $login_auth = $_POST['auth'];
    } else {
        $login_auth = '';
    }

    $remember = isset($_SESSION['rmbfield']) && isset($_POST[$_SESSION['rmbfield']]) && $CFG_GLPI["login_remember_time"];

    // Redirect management
    $REDIRECT = "";
    if (isset($_POST['redirect']) && (strlen($_POST['redirect']) > 0)) {
        $REDIRECT = "?redirect=" . rawurlencode($_POST['redirect']);
    } elseif (isset($_GET['redirect']) && strlen($_GET['redirect']) > 0) {
        $REDIRECT = "?redirect=" . rawurlencode($_GET['redirect']);
    }

    $auth = new Auth();

    // now we can continue with the process...
    if ($auth->login($login, $password, (isset($_REQUEST["noAUTO"]) ? $_REQUEST["noAUTO"] : false), $remember, $login_auth)) {
        Auth::redirectIfAuthenticated();
    } else {
        TemplateRenderer::getInstance()->display('@tam/login_error.html.twig', [
            'errors'    => $auth->getErrors(),
            'login_url' => $_SERVER['PHP_SELF'],
        ]);
        exit();
    }
} elseif (isset($_POST['TamFix'])) {
    $tam_id = $_POST['TamFix'];
    $new_date_end = $_POST['date_end'];

    PluginTamManagement::fixForgottenTam($tam_id, $new_date_end);

    Html::back();
}

$tam->showLogin();

Html::displayMessageAfterRedirect();
Html::footer();
