<?php
/*
 -------------------------------------------------------------------------
 TAM plugin for GLPI
 Copyright (C) 2021-2023 by the TICgal Team.
 https://www.tic.gal
 -------------------------------------------------------------------------
 LICENSE
 This file is part of the TAM plugin.
 TAM plugin is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.
 TAM plugin is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with TAM. If not, see <http://www.gnu.org/licenses/>.
 --------------------------------------------------------------------------
 @package   TAM
 @author    the TICgal team
 @copyright Copyright (c) 2021-2023 TICgal team
 @license   AGPL License 3.0 or (at your option) any later version
            http://www.gnu.org/licenses/agpl-3.0-standalone.html
 @link      https://www.tic.gal
 @since     2021
 ----------------------------------------------------------------------
*/
include("../../../inc/includes.php");

$plugin = new Plugin();
if (!$plugin->isInstalled('tam') || !$plugin->isActivated('tam')) {
    Html::displayNotFoundError();
}

$forgot = new PluginTamForgot();
if (isset($_POST['update'])) {
    PluginTamForgot::updateForgot($_POST);
    Html::redirect(Plugin::getWebDir('tam') . "/front/forgot.php");
}
if (!isset($_GET["id"])) {
    Html::displayNotFoundError();
}

Html::header(
    PluginTamForgot::getTypeName(2),
    $_SERVER['PHP_SELF'],
    'management',
    'plugintammenu'
);

if (Session::haveRight('plugin_tam_all_timers', 1)) {
    $forgot->showForm($_GET['id'], $_GET);
} else {
    Html::displayRightError();
}

Html::footer();
