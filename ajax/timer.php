<?php
/*
 -------------------------------------------------------------------------
 TAM plugin for GLPI
 Copyright (C) 2021-2023 by the TICgal Team.
 https://www.tic.gal
 -------------------------------------------------------------------------
 LICENSE
 This file is part of the TAM plugin.
 TAM plugin is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.
 TAM plugin is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with TAM. If not, see <http://www.gnu.org/licenses/>.
 --------------------------------------------------------------------------
 @package   TAM
 @author    the TICgal team
 @copyright Copyright (c) 2021-2023 TICgal team
 @license   AGPL License 3.0 or (at your option) any later version
            http://www.gnu.org/licenses/agpl-3.0-standalone.html
 @link      https://www.tic.gal
 @since     2021
 ----------------------------------------------------------------------
*/

include("../../../inc/includes.php");
header("Content-Type: text/html; charset=UTF-8");
Html::header_nocache();

Session::checkLoginUser();

use Glpi\Toolbox\Sanitizer;

if (isset($_POST["action"])) {
    switch ($_POST["action"]) {
        case 'start':

            $id = PluginTamTam::checkWorking(Session::getLoginUserID());
            if ($id > 0) {
                $result = [
                    'action' => 'end',
                    'value' => __('End'),
                    'color' => 'red',
                    'id' => $id
                ];
            } else {
                $query = [
                    'FROM' => 'glpi_plugin_tam_days',
                    'WHERE' => [
                        'date' => date("Y-m-d"),
                        'users_id' => Session::getLoginUserID()
                    ],
                ];
                $req = $DB->request($query);
                if ($row = $req->current()) {
                    $day_id = $row['id'];
                } else {
                    if (!$calendars_id = PluginTamUserCalendar::getCalendar(Session::getLoginUserID())) {
                        $calendars_id = Entity::getUsedConfig('calendars_strategy', $_SESSION['glpidefault_entity'], 'calendars_id', 0);
                    }
                    $break = new PluginTamCalendar();
                    $break->getFromDBByCalendar($calendars_id);
                    $calendar     = new Calendar();

                    $timeoftheday = 0;
                    if (($calendars_id > 0) && $calendar->getFromDB($calendars_id) && !$calendar->isHoliday(date("Y-m-d"))) {
                        $dayweak = $calendar->getDayNumberInWeek(strtotime(date("Y-m-d")));
                        $timeoftheday = CalendarSegment::getActiveTimeBetween($calendars_id, $dayweak, '00:00:00', '23:59:59');
                    }
                    $break_id = $break->getID();

                    $DB->insert(
                        'glpi_plugin_tam_days',
                        [
                            'users_id' => Session::getLoginUserID(),
                            'active_time' => $timeoftheday,
                            'plugin_tam_calendar_id' => $break_id,
                            'date' => date("Y-m-d"),
                        ]
                    );
                    $day_id = $DB->insertId();
                }
                $ip = getenv("HTTP_X_FORWARDED_FOR") ? Sanitizer::encodeHtmlSpecialChars(getenv("HTTP_X_FORWARDED_FOR")) : getenv("REMOTE_ADDR");

                $DB->insert(
                    'glpi_plugin_tam_tams',
                    [
                        'plugin_tam_days_id' => $day_id,
                        'ip_start' => $ip,
                        'origin_start' => PluginTamTam::WEB,
                    ]
                );

                $id = $DB->insertId();

                $result = [
                    'action' => 'end',
                    'value' => __('End'),
                    'color' => 'red',
                    'id' => $id
                ];
            }
            echo json_encode($result);
            break;
        case 'end':

            $id = PluginTamTam::checkWorking(Session::getLoginUserID());
            if ($id <= 0) {
                $result = [
                    'action' => 'start',
                    'value' => __('Start'),
                    'color' => 'green',
                    'id' => 0
                ];
            } else {
                $date = date("Y-m-d H:i:s");

                $query = [
                    'SELECT' => 'date_start',
                    'FROM' => 'glpi_plugin_tam_tams',
                    'WHERE' => [
                        'id' => $_POST["timer_id"]
                    ]
                ];

                $req = $DB->request($query);
                $row = $req->current();
                $actiontime = strtotime($date) - strtotime($row['date_start']);

                $ip = getenv("HTTP_X_FORWARDED_FOR") ? Sanitizer::encodeHtmlSpecialChars(getenv("HTTP_X_FORWARDED_FOR")) : getenv("REMOTE_ADDR");

                // * 1.5.0 Reason for end tam
                $reason = '';
                if (isset($_POST['reason'])) {
                    $reason_item = new PluginTamReason();
                    $reason_item->getFromDB($_POST['reason']);
                    $reason = $reason_item->fields['name'];
                }

                $DB->update(
                    'glpi_plugin_tam_tams',
                    [
                        'date_end' => date("Y-m-d H:i:s"),
                        'working_time' => $actiontime,
                        'ip_end' => $ip,
                        'origin_end' => PluginTamTam::WEB,
                        'reason' => $reason,
                    ],
                    [
                        'id' => $_POST["timer_id"]
                    ]
                );

                // * 1.5.0 Blockedlog
                PluginTamBlockedlog::createLog($_POST["timer_id"], Session::getLoginUserID());

                $result = [
                    'action' => 'start',
                    'value' => __('Start'),
                    'color' => 'green',
                    'id' => 0
                ];
            }
            echo json_encode($result);
            break;
    }
}
