<?php
/*
 -------------------------------------------------------------------------
 TAM plugin for GLPI
 Copyright (C) 2021-2023 by the TICgal Team.
 https://www.tic.gal
 -------------------------------------------------------------------------
 LICENSE
 This file is part of the TAM plugin.
 TAM plugin is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.
 TAM plugin is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with TAM. If not, see <http://www.gnu.org/licenses/>.
 --------------------------------------------------------------------------
 @package   TAM
 @author    the TICgal team
 @copyright Copyright (c) 2021-2023 TICgal team
 @license   AGPL License 3.0 or (at your option) any later version
            http://www.gnu.org/licenses/agpl-3.0-standalone.html
 @link      https://www.tic.gal
 @since     2021
 ----------------------------------------------------------------------
*/

include("../../../inc/includes.php");

Session::checkLoginUser();

if (!isset($_REQUEST["action"])) {
    exit;
}

if ($_REQUEST["action"] == "get_events") {
    header("Content-Type: application/json; charset=UTF-8");
    echo json_encode(PluginTamTam::constructEventsArray($_REQUEST));
    exit;
}

if ($_REQUEST["action"] == "get_shift_template") {
    $key = 'plugin_tam_shifttemplates_id';
    if (isset($_POST[$key]) && $_POST[$key] > 0) {
        $template = new PluginTamShiftTemplate();
        $template->getFromDB($_POST[$key]);

        $template->fields = array_map('html_entity_decode', $template->fields);
        $template->fields['rrule'] = json_decode($template->fields['rrule'], true);
        header("Content-Type: application/json; charset=UTF-8");
        echo json_encode($template->fields, JSON_NUMERIC_CHECK);
        exit;
    }
}
