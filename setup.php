<?php
/*
 -------------------------------------------------------------------------
 TAM plugin for GLPI
 Copyright (C) 2021-2023 by the TICgal Team.
 https://www.tic.gal
 -------------------------------------------------------------------------
 LICENSE
 This file is part of the TAM plugin.
 TAM plugin is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.
 TAM plugin is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with TAM. If not, see <http://www.gnu.org/licenses/>.
 --------------------------------------------------------------------------
 @package   TAM
 @author    the TICgal team
 @copyright Copyright (c) 2021-2023 TICgal team
 @license   AGPL License 3.0 or (at your option) any later version
            http://www.gnu.org/licenses/agpl-3.0-standalone.html
 @link      https://www.tic.gal
 @since     2021
 ----------------------------------------------------------------------
*/
use Glpi\Plugin\Hooks;

define("PLUGIN_TAM_VERSION", "1.5.2");
define("PLUGIN_TAM_MIN_GLPI", "10.0.0");
define("PLUGIN_TAM_MAX_GLPI", "10.1.0");

function plugin_version_tam()
{
    return [
        'name' => 'TAM',
        'version' => PLUGIN_TAM_VERSION,
        'author' => '<a href="https://tic.gal">TICgal</a>',
        'requirements' => [
            'glpi' => [
                'min' => PLUGIN_TAM_MIN_GLPI,
                'max' => PLUGIN_TAM_MAX_GLPI,
            ]
        ]
    ];
}

function plugin_init_tam()
{
    global $PLUGIN_HOOKS, $CFG_GLPI;

    $PLUGIN_HOOKS['csrf_compliant']['tam'] = true;

    $plugin = new Plugin();
    if ($plugin->isActivated('tam')) {
        // * Classes
        Plugin::registerClass('PluginTamAdmin');

        Plugin::registerClass('PluginTamDashboard');

        Plugin::registerClass('PluginTamApirest');

        Plugin::registerClass('PluginTamLog');

        Plugin::registerClass('PluginTamReason');

        Plugin::registerClass('PluginTamUserCalendar');

        Plugin::registerClass('PluginTamManagement');

        Plugin::registerClass('PluginTamForgot', ['addtabon' => ['PluginTamForgot']]);

        Plugin::registerClass('PluginTamTam', ['addtabon' => 'Preference']);

        Plugin::registerClass('PluginTamConfig', ['addtabon' => 'Config']);

        Plugin::registerClass('PluginTamProfile', ['addtabon' => ['Profile']]);

        Plugin::registerClass('PluginTamEntity', ['addtabon' => ['Entity']]);

        Plugin::registerClass('PluginTamCalendar', ['addtabon' => ['Calendar']]);

        Plugin::registerClass('PluginTamShift', ['planning_types' => true, 'planning_add_types' => true]);

        Plugin::registerClass('PluginTamLeave', [
            'linkgroup_tech_types'   => true,
            'linkuser_tech_types'    => true,
            'document_types'         => true,
            'contract_types'         => true,
            'ticket_types'           => true,
            'helpdesk_visible_types' => true,
            'link_types'             => true,
            'planning_types'         => true
        ]);

        Plugin::registerClass('PluginTamHoliday', [
            'planning_types' => true
        ]);


        // * Plugin Hooks
        $PLUGIN_HOOKS['assign_to_ticket']['tam'] = true;

        $PLUGIN_HOOKS['use_massive_action']['tam'] = 1;

        $PLUGIN_HOOKS['config_page']['tam'] = 'front/config.form.php';

        $PLUGIN_HOOKS[Hooks::POST_ITEM_FORM]['tam'] = 'plugin_tam_post_item_form';

        $PLUGIN_HOOKS[Hooks::REDEFINE_MENUS]['tam'] = 'plugin_tam_redefine_menus';

        $PLUGIN_HOOKS[Hooks::DISPLAY_LOGIN]['tam'] = 'plugin_tam_display_login';

        $PLUGIN_HOOKS[Hooks::CHANGE_PROFILE]['tam'] = ['PluginTamProfile', 'initProfile'];

        $PLUGIN_HOOKS[Hooks::DASHBOARD_CARDS]['tam'] = ['PluginTamDashboard', 'dashboardCards'];

        $PLUGIN_HOOKS[Hooks::PRE_ITEM_ADD]['tam'] = [
            'PluginTamLeave' => 'plugin_tam_checkRequired',
        ];

        $PLUGIN_HOOKS[Hooks::PRE_ITEM_UPDATE]['tam'] = [
            'PluginTamLeave' => 'plugin_tam_checkRequired',
            'User' => ['PluginTamUserCalendar', 'updateCalendar'],
        ];

        $PLUGIN_HOOKS[Hooks::ITEM_ADD]['tam'] = [
            'User' => ['PluginTamUserCalendar', 'addCalendar'],
        ];

        $PLUGIN_HOOKS['display_planning']['tam']  = [
            'PluginTamHoliday' => "displayPlanningItem"
        ];

        $PLUGIN_HOOKS['planning_populate']['tam'] = [
            'PluginTamLeave' => "populatePlanning",
            'PluginTamHoliday' => "populatePlanning"
        ];

        $admin = new PluginTamAdmin();
        if ($admin->canSee()) {
            $PLUGIN_HOOKS['menu_toadd']['tam'] = ['management' => 'PluginTamMenu'];
            //$PLUGIN_HOOKS['helpdesk_menu_entry']['tam']='front/menu.php';
        }

        $leave = new PluginTamLeave();
        if ($leave->canCreate()) {
            array_push($CFG_GLPI['planning_add_types'], 'PluginTamLeave');
        }

        if (Session::getLoginUserID()) {
            $PLUGIN_HOOKS[Hooks::ADD_JAVASCRIPT]['tam'] = ['js/tam.js'];
        }

        if ($plugin->isActivated('advancedplanning') && isset($_SERVER['REQUEST_URI']) && strpos($_SERVER['REQUEST_URI'], "front/planning.php") !== false) {
            if (isset($_SESSION['glpi_plannings']['lastview']) && $_SESSION['glpi_plannings']['lastview'] == 'resourceYear') {
                $_SESSION['glpi_plannings']['lastview'] = 'timeGridWeek';
            }

            $PLUGIN_HOOKS[Hooks::ADD_JAVASCRIPT]['tam'][] = 'js/timelineyear.js';
        }

        // * Crontasks
        Crontask::Register("PluginTamTam", 'stopClock', DAY_TIMESTAMP, [
            'state'     => 1,
            'mode'      => CronTask::MODE_EXTERNAL,
            'hourmin'   => 0,
            'hourmax'   => 1
        ]);

        Crontask::Register("PluginTamEntityCalendar", 'changeCalendar', DAY_TIMESTAMP, [
            'state'     => 1,
            'mode'      => CronTask::MODE_EXTERNAL,
            'hourmin'   => 1,
            'hourmax'   => 2
        ]);

        Crontask::Register("PluginTamLeave", 'markLeave', DAY_TIMESTAMP, [
            'state'     => 1,
            'mode'      => CronTask::MODE_EXTERNAL,
            'hourmin'   => 2,
            'hourmax'   => 3
        ]);
    }
}
